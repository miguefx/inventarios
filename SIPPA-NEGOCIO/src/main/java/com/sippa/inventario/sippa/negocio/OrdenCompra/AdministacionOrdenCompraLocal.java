/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventario.sippa.negocio.OrdenCompra;

import VO.T_OrdenDeCompra;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author matc_
 */
@Local
public interface AdministacionOrdenCompraLocal {

    Long crearOrdenCompra(T_OrdenDeCompra objVoOrdenCompra);

    T_OrdenDeCompra findById(Long idOrdenCompra);

     List<T_OrdenDeCompra> obtenerOrdenesXEstado(Boolean TRUE);

     void actualizarOrden(T_OrdenDeCompra orden);

     T_OrdenDeCompra buscarOrdenById(Long idOrdenCompra);



}
