/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventario.sippa.negocio.inventarios;

import VO.GraficaStockMinimo;
import VO.T_Bodegas;
import VO.T_Inventarios;
import VO.T_Productos;
import com.sippa.inventarios.sippa.ejb.inventarios.T_InventariosFacadeLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author matc_
 */
@Stateless
public class AdministracionInventarios implements AdministracionInventariosLocal {

    @EJB
    T_InventariosFacadeLocal inventariosFacade;

    @Override
    public T_Inventarios findByProductoXBodega(T_Productos producto, T_Bodegas bodega) {
        return inventariosFacade.findByProductoXBodega(producto, bodega);
    }

    @Override
    public void crearNuevoInventario(T_Inventarios nuevoInventario) {
        inventariosFacade.create(nuevoInventario);
    }

    @Override
    public List<T_Inventarios> findAll() {
        return inventariosFacade.findAll();
    }

    @Override
    public void actualizarInventario(T_Inventarios inventario) {
        inventariosFacade.edit(inventario);
    }

    @Override
    public List<GraficaStockMinimo> consultarByProductoStockMinimo() {
        return inventariosFacade.findAllByStockMinimoSuperado();
    }

    @Override
    public List<T_Inventarios> findByBodega(T_Bodegas bodega) {
        return inventariosFacade.findByBodega(bodega);
    }

    @Override
    public void eliminarEntidad(T_Inventarios inventarioAEliminar) {
        inventariosFacade.remove(inventarioAEliminar);
    }

}
