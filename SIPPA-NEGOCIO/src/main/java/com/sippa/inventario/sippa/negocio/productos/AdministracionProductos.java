/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventario.sippa.negocio.productos;

import VO.T_Productos;
import com.sippa.inventarios.sippa.ejb.productos.T_ProductosFacadeLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author matc_
 */
@Stateless
public class AdministracionProductos implements AdministracionProductosLocal {

    @EJB
    T_ProductosFacadeLocal fachadaProductos;

    @Override
    public void crearProducto(T_Productos nuevoProducto) {
        fachadaProductos.create(nuevoProducto);
    }

    @Override
    public List<T_Productos> consultarProductosXEstado(Boolean estado) {
        return fachadaProductos.findByEstado(estado);
    }

    @Override
    public void actualizarProducto(T_Productos producto) {
       fachadaProductos.edit(producto);
    }

    @Override
    public void eliminarProducto(T_Productos producto) {
        fachadaProductos.remove(producto);
    }

    @Override
    public T_Productos consultarProductoPorId(Long idProducto) {
        return fachadaProductos.consultarProductoXId(idProducto);
    }

    @Override
    public void eliminarDetalle(Long idProducto) {
        fachadaProductos.eliminarDetalleProductosXProveedores(idProducto);
    }

    @Override
    public List<T_Productos> consultarPorStockMinimo() {
        return fachadaProductos.findAllStockMinimoTrue();
    }
}
