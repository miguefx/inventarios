/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventario.sippa.negocio.parametros;

import VO.T_Parametros;
import com.sippa.inventarios.sippa.ejb.parametros.T_ParametrosFacadeLocal;
import java.util.Objects;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author matc_
 */
@Stateless
public class AdministracionParametros implements AdministracionParametrosLocal {

    @EJB
    T_ParametrosFacadeLocal parametrosFacade;

    @Override
    public String buscarValorParametroGenerico(String stockMinimoProducto) {
        T_Parametros parametro = parametrosFacade.findByParametroGenerico(stockMinimoProducto);
        return Objects.nonNull(parametro) ? parametro.getValor(): null;
    }

}
