/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventario.sippa.negocio.InventarioSeriales;

import VO.T_InventariosSeriales;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author matc_
 */
@Local
public interface AdministracionInventarioSerialesLocal {

    void eliminarEntidad(T_InventariosSeriales paramT_InventariosSeriales);

    List<T_InventariosSeriales> findByIdInventario(Long idInventario);
}
