/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventario.sippa.negocio.usuarios;

import VO.T_Usuarios;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author matc_
 */
@Local
public interface AdministracionUsuariosLocal {

    T_Usuarios login(String usuario, String password);

    void crearUsuario(T_Usuarios objVoUsuarios);

    List<T_Usuarios> buscarUsuariosXEstado(Boolean estado);

     T_Usuarios buscarUsuarioPorId(Long idUsuario);

     void actualizarUsuario(T_Usuarios usuarioAModificar);

}
