/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventario.sippa.negocio.Proveedores;

import VO.T_Proveedores;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author matc_
 */
@Local
public interface AdministracionProveedoresLocal {

     void crearProveedor(T_Proveedores proveedores);

     List<T_Proveedores> consultarProveedoresXEstado(Boolean estado);

     void actualizarProveedores(T_Proveedores proveedor);

     T_Proveedores buscarProveedorPorId(Long idProveedor);

     void eliminarDetalle(Long idProveedor);
    
}
