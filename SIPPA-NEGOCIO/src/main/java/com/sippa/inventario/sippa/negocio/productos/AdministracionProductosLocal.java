/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventario.sippa.negocio.productos;

import VO.T_Productos;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author matc_
 */
@Local
public interface AdministracionProductosLocal {

     void crearProducto(T_Productos nuevoProducto);

    List<T_Productos> consultarProductosXEstado(Boolean estado);

    void actualizarProducto(T_Productos producto);

    void eliminarProducto(T_Productos producto);

    T_Productos consultarProductoPorId(Long idProducto);

    void eliminarDetalle(Long idProducto);

     List<T_Productos> consultarPorStockMinimo();
    
}
