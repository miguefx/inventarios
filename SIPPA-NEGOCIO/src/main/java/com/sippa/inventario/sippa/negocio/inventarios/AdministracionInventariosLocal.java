/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventario.sippa.negocio.inventarios;

import VO.GraficaStockMinimo;
import VO.T_Bodegas;
import VO.T_Inventarios;
import VO.T_Productos;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author matc_
 */
@Local
public interface AdministracionInventariosLocal {

    T_Inventarios findByProductoXBodega(T_Productos producto, T_Bodegas bodega);

    void crearNuevoInventario(T_Inventarios nuevoInventario);

    List<T_Inventarios> findAll();

    void actualizarInventario(T_Inventarios inventario);

    List<GraficaStockMinimo> consultarByProductoStockMinimo();

    List<T_Inventarios> findByBodega(T_Bodegas bodega);

    void eliminarEntidad(T_Inventarios inventarioAEliminar);


}
