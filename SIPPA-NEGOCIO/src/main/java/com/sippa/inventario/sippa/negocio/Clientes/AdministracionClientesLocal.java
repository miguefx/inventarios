/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventario.sippa.negocio.Clientes;

import VO.T_Clientes;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author matc_
 */
@Local
public interface AdministracionClientesLocal {

    void crearCliente(T_Clientes objvoCliente);

    List<T_Clientes> obtenerClientesXEstado(Boolean estado);

    T_Clientes consultarClienteXId(Long idCliente);

    void eliminarDetallesCliente(Long idCliente);

    void actualizarCliente(T_Clientes clienteElegido);

}
