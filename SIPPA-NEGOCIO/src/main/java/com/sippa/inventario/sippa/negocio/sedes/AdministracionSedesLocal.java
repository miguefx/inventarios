/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventario.sippa.negocio.sedes;

import VO.T_Sedes;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author matc_
 */
@Local
public interface AdministracionSedesLocal {

    void eliminarSede(T_Sedes sede);

    T_Sedes consultarSedePorId(Long idSede);

    void actualizarSede(T_Sedes sedeAModificar);

    void crearSede(T_Sedes nuevaSede);

    List<T_Sedes> obtenerSedesXEstado(Boolean estado);
}
