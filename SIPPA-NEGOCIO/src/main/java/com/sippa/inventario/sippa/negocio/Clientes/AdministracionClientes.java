/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventario.sippa.negocio.Clientes;

import VO.T_Clientes;
import com.sippa.inventarios.sippa.ejb.clientes.T_ClientesFacadeLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class AdministracionClientes implements AdministracionClientesLocal {
    
    @EJB
    T_ClientesFacadeLocal fachadaClientes;
    
    @Override
    public void crearCliente(T_Clientes objvoCliente) {
        fachadaClientes.create(objvoCliente);
    }

    @Override
    public List<T_Clientes> obtenerClientesXEstado(Boolean estado) {
        return fachadaClientes.findByEstado(estado);
    }

    @Override
    public T_Clientes consultarClienteXId(Long idCliente) {
        return fachadaClientes.findByXId(idCliente);
    }

    @Override
    public void eliminarDetallesCliente(Long idCliente) {
            fachadaClientes.eliminarDetallesXId(idCliente);
    }

    @Override
    public void actualizarCliente(T_Clientes clienteElegido) {
        fachadaClientes.edit(clienteElegido);
    }
}
