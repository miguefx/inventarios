/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventario.sippa.negocio.InventarioSeriales;

import VO.T_InventariosSeriales;
import com.sippa.inventarios.sippa.ejb.InventarioSeriales.T_InventariosSerialesFacadeLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author matc_
 */
@Stateless
public class AdministracionInventarioSeriales implements AdministracionInventarioSerialesLocal {

    @EJB
  T_InventariosSerialesFacadeLocal inventarioSerialFacade;
    
    @Override
    public void eliminarEntidad(T_InventariosSeriales serialInventario) {
        inventarioSerialFacade.remove(serialInventario);
    }

    @Override
    public List<T_InventariosSeriales> findByIdInventario(Long idInventario) {
        return inventarioSerialFacade.findByIdInventario(idInventario);
    }

}
