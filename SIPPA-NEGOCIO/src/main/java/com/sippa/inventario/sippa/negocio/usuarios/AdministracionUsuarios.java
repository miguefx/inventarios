/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventario.sippa.negocio.usuarios;

import VO.T_Usuarios;
import com.sippa.inventarios.sippa.ejb.usuarios.T_UsuariosFacadeLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author matc_
 */
@Stateless
public class AdministracionUsuarios implements AdministracionUsuariosLocal {

    @EJB
    T_UsuariosFacadeLocal fachadaUsuarios;

    @Override
    public T_Usuarios login(String usuario, String password) {
        return fachadaUsuarios.findByUsuarioXPassword(usuario, password);
    }

    @Override
    public void crearUsuario(T_Usuarios objVoUsuarios) {
        fachadaUsuarios.create(objVoUsuarios);
    }

    @Override
    public List<T_Usuarios> buscarUsuariosXEstado(Boolean estado) {
        return fachadaUsuarios.findByEstado(estado);
    }

    @Override
    public T_Usuarios buscarUsuarioPorId(Long idUsuario) {
        return fachadaUsuarios.find(idUsuario);
    }

    @Override
    public void actualizarUsuario(T_Usuarios usuarioAModificar) {
        fachadaUsuarios.edit(usuarioAModificar);
    }

}
