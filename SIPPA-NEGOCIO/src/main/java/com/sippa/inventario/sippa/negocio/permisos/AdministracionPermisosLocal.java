/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventario.sippa.negocio.permisos;

import VO.T_Permisos;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author matc_
 */
@Local
public interface AdministracionPermisosLocal {

    List<T_Permisos> buscarPermisosByIdUsuario(Long idUsuario);

    void eliminarPermiso(T_Permisos permiso);

    void crearPermisoUsuario(T_Permisos objVoPermiso);

}
