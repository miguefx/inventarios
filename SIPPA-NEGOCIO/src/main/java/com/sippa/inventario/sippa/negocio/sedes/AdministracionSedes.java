/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventario.sippa.negocio.sedes;

import VO.T_Sedes;
import com.sippa.inventarios.sippa.ejb.sedes.T_SedesFacadeLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author matc_
 */
@Stateless
public class AdministracionSedes implements AdministracionSedesLocal {
    
    @EJB
    T_SedesFacadeLocal fachadaSedes;

    @Override
    public List<T_Sedes> obtenerSedesXEstado(Boolean estado) {
        return fachadaSedes.findByEstado(estado);
    }

    @Override
    public void eliminarSede(T_Sedes sede) {
        fachadaSedes.remove(sede);
    }

    @Override
    public T_Sedes consultarSedePorId(Long idSede) {
        return fachadaSedes.consultarSedePorId(idSede);
    }

    @Override
    public void actualizarSede(T_Sedes sedeAModificar) {
        fachadaSedes.edit(sedeAModificar);
    }

    @Override
    public void crearSede(T_Sedes nuevaSede) {
        fachadaSedes.create(nuevaSede);
    }
    
}
