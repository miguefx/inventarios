/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventario.sippa.negocio.Proveedores;

import VO.T_Proveedores;
import javax.ejb.Stateless;
import com.sippa.inventarios.sippa.ejb.proveedores.T_ProductosProveedoresFacadeLocal;
import com.sippa.inventarios.sippa.ejb.proveedores.T_ProveedoresFacadeLocal;
import java.util.List;
import javax.ejb.EJB;

/**
 *
 * @author matc_
 */
@Stateless
public class AdministracionProveedores implements AdministracionProveedoresLocal {
    
    
    @EJB
    T_ProveedoresFacadeLocal fachadaProveedores;
    
    @EJB
    T_ProductosProveedoresFacadeLocal fachadaProveedoresProductos;

    @Override
    public void crearProveedor(T_Proveedores proveedores) {
        fachadaProveedores.create(proveedores);
    }

    @Override
    public List<T_Proveedores> consultarProveedoresXEstado(Boolean estado) {
        return fachadaProveedores.findByEstadoXEstado(estado);
    }

    @Override
    public void actualizarProveedores(T_Proveedores proveedor) {
        fachadaProveedores.edit(proveedor);
    }

    @Override
    public T_Proveedores buscarProveedorPorId(Long idProveedor) {
        return fachadaProveedores.findById(idProveedor);
    }

    @Override
    public void eliminarDetalle(Long idProveedor) {
        fachadaProveedoresProductos.eliminarRegistrosXId(idProveedor);
    }
    
}
