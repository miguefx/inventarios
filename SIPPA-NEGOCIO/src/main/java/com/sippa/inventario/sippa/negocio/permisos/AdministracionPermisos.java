/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventario.sippa.negocio.permisos;

import VO.T_Permisos;
import com.sippa.inventarios.sippa.ejb.permisos.T_PermisosFacadeLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author matc_
 */
@Stateless
public class AdministracionPermisos implements AdministracionPermisosLocal {
    
    @EJB
    T_PermisosFacadeLocal fachadaPermisos;
    
    @Override
    public List<T_Permisos> buscarPermisosByIdUsuario(Long idUsuario) {
        return fachadaPermisos.findByIdUsuario(idUsuario);
    }
    
    @Override
    public void eliminarPermiso(T_Permisos permiso) {
        fachadaPermisos.remove(permiso);
    }
    
    @Override
    public void crearPermisoUsuario(T_Permisos objVoPermiso) {
        fachadaPermisos.create(objVoPermiso);
    }
    
}
