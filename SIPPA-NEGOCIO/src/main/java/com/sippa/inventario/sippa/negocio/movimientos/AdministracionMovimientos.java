/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventario.sippa.negocio.movimientos;

import VO.T_DetalleMovimiento;
import VO.T_DetalleOrdenDeCompra;
import VO.T_Movimientos;
import VO.TiposMovimientos;
import com.sippa.inventarios.sippa.ejb.movimientos.T_MovimientosFacadeLocal;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author matc_
 */
@Stateless
public class AdministracionMovimientos implements AdministracionMovimientosLocal {

    @EJB
    T_MovimientosFacadeLocal fachadaMovimientos;

    @Override
    public void crearMovimientoDeEntrada(T_Movimientos movimiento) {
        movimiento.setTipoMovimiento(TiposMovimientos.ENTRADA);
        fachadaMovimientos.create(movimiento);
    }

    @Override
    public List<T_DetalleMovimiento> homologarDetalleMovimiento(List<T_DetalleOrdenDeCompra> movimientoDetalle) {
        List<T_DetalleMovimiento> listDetalleNueva = new ArrayList<>();
        for (T_DetalleOrdenDeCompra t_DetalleMovimiento : movimientoDetalle) {
            listDetalleNueva.add(obtenerObjetoHomologado(t_DetalleMovimiento));
        }
        return listDetalleNueva;
    }

    private T_DetalleMovimiento obtenerObjetoHomologado(T_DetalleOrdenDeCompra detalleOrden) {
        T_DetalleMovimiento detalleMovimientoNuevo = new T_DetalleMovimiento();
        detalleMovimientoNuevo.setCantidad(detalleOrden.getCantidad());
        detalleMovimientoNuevo.setItem(detalleOrden.getItem());
        detalleMovimientoNuevo.setValorTotal(detalleOrden.getValorTotal());
        detalleMovimientoNuevo.setValorUnitario(detalleOrden.getValorUnitario());
        detalleMovimientoNuevo.setProducto(detalleOrden.getProducto());
        return detalleMovimientoNuevo;
    }
}
