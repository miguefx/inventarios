/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventario.sippa.negocio.Kardex;

import VO.T_Bodegas;
import VO.T_Kardex;
import VO.T_Productos;
import VO.T_reporteKardex;
import VO.T_reporteKardexAux;
import com.sippa.inventarios.sippa.ejb.Kardex.T_KardexFacadeLocal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Miguel-PC
 */
@Stateless
public class AdministracionKardex implements AdministracionKardexLocal {
    
    @EJB
    private T_KardexFacadeLocal kardexFacade;
    
    @Override
    public T_Kardex findByProductoXBodega(T_Productos producto, T_Bodegas bodega) {
        return kardexFacade.findByProductoBodegaUltimo(producto, bodega);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    @Override
    public Long crearAndReturnId(T_Kardex nuevoKardex) {
        return kardexFacade.createAndReturnId(nuevoKardex);
    }
    
    @Override
    public T_Kardex findById(String numeroRadicado) {
        return kardexFacade.find(Long.valueOf(numeroRadicado));
    }

    @Override
    public List<T_Kardex> findByProductoXBodegaHistorico(T_Productos producto, T_Bodegas bodega) {
        return kardexFacade.findByProductoBodegaUltimoHistorico(producto, bodega);
    }

    @Override
    public void actualizarKardex(T_Kardex kardex) {
        kardexFacade.edit(kardex);
    }

    @Override
    public List<T_reporteKardex> generarReporteGeneral() {
        return kardexFacade.obtenerReporteGeneralKardex();
    }

    @Override
    public List<T_reporteKardexAux> findDistinct() {
        return kardexFacade.findDistinct();
    }
}
