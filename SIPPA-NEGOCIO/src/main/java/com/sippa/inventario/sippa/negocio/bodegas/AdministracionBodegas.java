/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventario.sippa.negocio.bodegas;

import VO.T_Bodegas;
import com.sippa.inventarios.sippa.ejb.bodegas.T_BodegasFacadeLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author matc_
 */
@Stateless
public class AdministracionBodegas implements AdministracionBodegasLocal {
    @EJB
    T_BodegasFacadeLocal fachadaBodegas;

    @Override
    public void crearBodega(T_Bodegas nuevaBodega) {
        fachadaBodegas.create(nuevaBodega);
    }

    @Override
    public List<T_Bodegas> consultarBodegasXEstado(Boolean estado) {
        return fachadaBodegas.consultarBodegasByEstado(estado);
    }

    @Override
    public void actualizarBodegas(T_Bodegas bodega) {
       fachadaBodegas.edit(bodega);
    }

    @Override
    public T_Bodegas consultarBodegaById(Long idBodega) {
        return fachadaBodegas.find(idBodega);
    }

}
