/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventario.sippa.negocio.OrdenCompra;

import VO.T_OrdenDeCompra;
import com.sippa.inventarios.sippa.ejb.ordenCompra.T_OrdenDeCompraFacadeLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author matc_
 */
@Stateless
public class AdministacionOrdenCompra implements AdministacionOrdenCompraLocal {

    @EJB
    T_OrdenDeCompraFacadeLocal persistenciaOrdenCompra;

    @Override
    public Long crearOrdenCompra(T_OrdenDeCompra objVoOrdenCompra) {
        return persistenciaOrdenCompra.crearYObtenerId(objVoOrdenCompra);
    }

    @Override
    public T_OrdenDeCompra findById(Long idOrdenCompra) {
        return persistenciaOrdenCompra.find(idOrdenCompra);
    }

    @Override
    public List<T_OrdenDeCompra> obtenerOrdenesXEstado(Boolean estado) {
        return persistenciaOrdenCompra.consultarOrdenesXEstado(estado);
    }

    @Override
    public void actualizarOrden(T_OrdenDeCompra orden) {
        try {
            persistenciaOrdenCompra.edit(orden);
        } catch (Exception e) {
            System.out.println("Ocurrio un error al actualizar Orden");
        }
    }

    @Override
    public T_OrdenDeCompra buscarOrdenById(Long idOrdenCompra) {
        return persistenciaOrdenCompra.consultarOrdenById(idOrdenCompra);
    }

}
