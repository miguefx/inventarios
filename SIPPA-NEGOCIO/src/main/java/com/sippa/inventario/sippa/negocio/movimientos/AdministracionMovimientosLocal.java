/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventario.sippa.negocio.movimientos;

import VO.T_DetalleMovimiento;
import VO.T_DetalleOrdenDeCompra;
import VO.T_Movimientos;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author matc_
 */
@Local
public interface AdministracionMovimientosLocal {
    
     void crearMovimientoDeEntrada(T_Movimientos movimiento);
    
     List<T_DetalleMovimiento> homologarDetalleMovimiento(List<T_DetalleOrdenDeCompra> movimientoDetalle);
}
