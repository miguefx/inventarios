/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventario.sippa.negocio.bodegas;

import VO.T_Bodegas;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author matc_
 */
@Local
public interface AdministracionBodegasLocal {

    void crearBodega(T_Bodegas nuevaBodega);

    void actualizarBodegas(T_Bodegas bodega);

    T_Bodegas consultarBodegaById(Long idBodega);

    List<T_Bodegas> consultarBodegasXEstado(Boolean estado);


}
