/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventario.sippa.negocio.ciudades;

import VO.T_Ciudades;
import com.sippa.inventarios.sippa.ejb.ciudades.T_CiudadesFacadeLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author matc_
 */
@Stateless
public class AdministracionCiudades implements AdministracionCiudadesLocal {
    
    @EJB
    T_CiudadesFacadeLocal ciudadesFachada;

    @Override
    public List<T_Ciudades> consultarCiudades() {
        return ciudadesFachada.findAll();
    }
    
    
}
