/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventario.sippa.negocio.movimientoSalida;

import VO.T_MovimientoSalidas;
import javax.ejb.Local;

/**
 *
 * @author matc_
 */
@Local
public interface AdministracionMovimientoSalidaLocal {

    void crearSalida(T_MovimientoSalidas salida);
}
