/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventario.sippa.negocio.Kardex;

import VO.T_Bodegas;
import VO.T_Inventarios;
import VO.T_Kardex;
import VO.T_Productos;
import VO.T_reporteKardex;
import VO.T_reporteKardexAux;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Miguel-PC
 */
@Local
public interface AdministracionKardexLocal {

    T_Kardex findByProductoXBodega(T_Productos producto, T_Bodegas bodega);

    Long crearAndReturnId(T_Kardex nuevoKardex);

    T_Kardex findById(String numeroRadicado);

    List<T_Kardex> findByProductoXBodegaHistorico(T_Productos producto, T_Bodegas bodega);

    void actualizarKardex(T_Kardex kardex);

    List<T_reporteKardex> generarReporteGeneral();

    List<T_reporteKardexAux> findDistinct();

}
