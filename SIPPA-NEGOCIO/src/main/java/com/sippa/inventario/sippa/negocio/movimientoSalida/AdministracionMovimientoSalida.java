/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventario.sippa.negocio.movimientoSalida;

import VO.T_MovimientoSalidas;
import com.sippa.inventarios.sippa.ejb.movimientoSalida.T_MovimientoSalidasFacadeLocal;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author matc_
 */
@Stateless
public class AdministracionMovimientoSalida implements AdministracionMovimientoSalidaLocal {

    @EJB
    T_MovimientoSalidasFacadeLocal movimientoSalidaFacade;

    @Override
    public void crearSalida(T_MovimientoSalidas salida) {
       movimientoSalidaFacade.create(salida);
    }

}
