/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanParametrizacionProductos;

import VO.T_Productos;
import VO.T_ProductosProveedores;
import VO.T_Proveedores;
import VO.T_Sedes;
import com.sippa.inventario.sippa.negocio.productos.AdministracionProductosLocal;
import com.sippa.inventario.sippa.negocio.sedes.AdministracionSedesLocal;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import lombok.Getter;
import lombok.Setter;
import org.omnifaces.util.Messages;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.DualListModel;
import org.primefaces.model.StreamedContent;
import org.primefaces.ultima.photoCam.PhotoCamView;
import static org.primefaces.ultima.photoCam.PhotoCamView.file;
import org.primefaces.ultima.service.ProductosService;
import org.primefaces.ultima.service.ProveedorService;

/**
 *
 * @author matc_
 */
@ManagedBean(name = "beanParametrizacionProductos")
@SessionScoped
public class beanGestionParametrizacionProductos extends PhotoCamView implements Serializable {

    @EJB
    AdministracionProductosLocal administracionProductos;

    @EJB
    AdministracionSedesLocal administracionSedes;

    @Getter
    @Setter
    private Long idProducto;

    @Getter
    @Setter
    private T_Productos productoAModificar;

    @Getter
    @Setter
    private List<T_Sedes> sedes;

    @Getter
    @Setter
    private Boolean isCargarImagen;

    @Getter
    @Setter
    private Boolean entroACargar;

    @Getter
    @Setter
    private BigDecimal stockMinimoProducto;

    @Getter
    @Setter
    private Boolean isTomarFoto;

    @Getter
    @Setter
    private StreamedContent imagenLocalCargadaProducto;

    @Getter
    @Setter
    private DualListModel<T_Proveedores> proveedoresXProducto;

    @ManagedProperty("#{proveedorService}")
    private ProveedorService service;

    public ProveedorService getService() {
        return service;
    }

    public void setService(ProveedorService service) {
        this.service = service;
    }

    @PostConstruct
    public void init() {
        sedes = new ArrayList<>();
        obtenerSedes();
        isCargarImagen = Boolean.FALSE;
        isTomarFoto = Boolean.FALSE;
        entroACargar = false;
    }

    public void cargarProductoAModificar() {
        if (Objects.nonNull(idProducto)) {
            productoAModificar = administracionProductos.consultarProductoPorId(idProducto);
            cargarInformacionDelPickList();
            cargarImagenProducto();
        }
    }

    public void cargarManualMente() {
        entroACargar = true;
        isCargarImagen = Boolean.TRUE;
        isTomarFoto = Boolean.FALSE;
        super.inicializarPhotoCam("gestionProductos");
    }

    public void tomarFoto() {
        entroACargar = true;
        isCargarImagen = Boolean.FALSE;
        isTomarFoto = Boolean.TRUE;
        super.inicializarPhotoCam("gestionProductos");
    }

    public void visualizarImagen() {
        try {
            if (Objects.nonNull(productoAModificar.getFoto())) {
                byte[] arreglo = productoAModificar.getFoto();
                HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
                response.getOutputStream().write(arreglo);
                response.getOutputStream().close();
                FacesContext.getCurrentInstance().responseComplete();
            }
            notificarMensajeError("El producto no tiene una imagen precargada");
        } catch (Exception e) {
        }
    }

    public void notificarMensajeError(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", mensaje));
    }

    public void notificarMensajeExitoso(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion!", mensaje));
    }

    public String actualizarProducto() {
        eliminarDatosDetalleProductos();
        setearValoresActualesPickList();
        setearImagenNuevaAlPrducto();
        administracionProductos.actualizarProducto(productoAModificar);
        notificarMensajeExitosoGlobal("El producto se ha actualizado correctamente");
        resetFiles();
        return "GestionProductos";
    }

    private void setearValoresActualesPickList() {
        List<T_ProductosProveedores> listProductosXProveedores = new ArrayList<>();
        proveedoresXProducto.getTarget().stream().forEach(proveedor -> {
            T_ProductosProveedores nuevoT_Producto = new T_ProductosProveedores();
            nuevoT_Producto.setIdProveedor(proveedor);
            nuevoT_Producto.setIdProducto(productoAModificar);
            listProductosXProveedores.add(nuevoT_Producto);
        });
        productoAModificar.setProductosXProvedores(listProductosXProveedores);
    }

    public beanGestionParametrizacionProductos() {
    }

    private void obtenerSedes() {
        sedes = administracionSedes.obtenerSedesXEstado(Boolean.TRUE);
    }

    public void notificarMensajeExitosoGlobal(String mensaje) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion !",
                mensaje);
        Messages.addFlashGlobal(msg);
    }

    public void notificarMensajeErrorGlobal(String mensaje) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error !",
                mensaje);
        Messages.addFlashGlobal(msg);

    }

    private void setearImagenNuevaAlPrducto() {
        if (entroACargar) {
            if (isCargarImagen && Objects.nonNull(file)) {
                productoAModificar.setFoto(establecerImagen());
            }
            if (Objects.nonNull(imagenTomada)) {
                productoAModificar.setFoto(imagenTomada);
            }
        }
    }

    private void cargarInformacionDelPickList() {
        List<T_Proveedores> proveedoreSource = service.consultarProveedoresActuales();
        List<T_Proveedores> proveedoresTarget = productoAModificar.getProductosXProvedores().stream().map(mapper -> mapper.getIdProveedor()).collect(Collectors.toList());
        proveedoreSource = eliminarProveedores(proveedoreSource);
        proveedoresTarget = eliminarProveedores(proveedoresTarget);
        proveedoresXProducto = new DualListModel<>(eliminarInformacionRepetida(proveedoreSource, proveedoresTarget), proveedoresTarget);

    }

    private List<T_Proveedores> eliminarInformacionRepetida(List<T_Proveedores> proveedoreSource, List<T_Proveedores> proveedoresTarget) {
        if (proveedoreSource.size() == proveedoresTarget.size()) {
            return proveedoreSource.stream().allMatch(product -> proveedoresTarget.contains(product)) ? new ArrayList<>() : proveedoreSource.stream().filter(productoSource -> !proveedoresTarget.contains(productoSource)).collect(Collectors.toList());
        }
        return proveedoreSource.stream().filter(productoSource -> !proveedoresTarget.contains(productoSource)).collect(Collectors.toList());
    }

    private void eliminarDatosDetalleProductos() {
        administracionProductos.eliminarDetalle(productoAModificar.getIdProducto());
    }

    private void cargarImagenProducto() {
        if (Objects.nonNull(productoAModificar.getFoto())) {
            imagenPreCargada = new DefaultStreamedContent(new ByteArrayInputStream(productoAModificar.getFoto()));
            imagenLocalCargadaProducto = imagenPreCargada;
        }
    }

    private List<T_Proveedores> eliminarProveedores(List<T_Proveedores> proveedoreSource) {
        List<T_Proveedores> proveedoresActivos = service.consultarProveedoresActuales();
        proveedoreSource = proveedoreSource.stream().filter(prov -> proveedoresActivos.contains(prov)).collect(Collectors.toList());
        return proveedoreSource;
    }

}
