/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanRegistroProductos;

import VO.T_Productos;
import VO.T_ProductosProveedores;
import VO.T_Proveedores;
import com.sippa.inventario.sippa.negocio.productos.AdministracionProductosLocal;
import com.sippa.inventario.sippa.negocio.sedes.AdministracionSedesLocal;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import lombok.Getter;
import lombok.Setter;
import org.omnifaces.util.Messages;
import org.primefaces.model.DualListModel;
import org.primefaces.ultima.photoCam.PhotoCamView;
import org.primefaces.ultima.service.ProductosService;
import org.primefaces.ultima.service.ProveedorService;

/**
 *
 * @author matc_
 */
@ManagedBean(name = "beanRegistroProducto")
@SessionScoped
public class beanRegistroProducto extends PhotoCamView implements Serializable {

    @EJB
    AdministracionProductosLocal administracionProductos;

    @EJB
    AdministracionSedesLocal administracionSedes;

    @Getter
    @Setter
    private DualListModel<T_Proveedores> proveedoresPick;

    @Getter
    @Setter
    private String descripcion;

    @Getter
    @Setter
    private String marca;

    @Getter
    @Setter
    private String modelo;

    @Getter
    @Setter
    private String codigoProducto;

    @Getter
    @Setter
    private String garantiaMes;

    @Getter
    @Setter
    private BigDecimal stockMinimoProducto;

    @Getter
    @Setter
    private Boolean stockMinimo;

    @Getter
    @Setter
    private Boolean isCargarImagen;

    @Getter
    @Setter
    private Boolean isRenderStockMinimo;

    @Getter
    @Setter
    private Boolean isTomarFoto;

    @ManagedProperty("#{proveedorService}")
    private ProveedorService service;

    public ProveedorService getService() {
        return service;
    }

    public void setService(ProveedorService service) {
        this.service = service;
    }

    @PostConstruct
    public void init() {
        isCargarImagen = Boolean.FALSE;
        isTomarFoto = Boolean.FALSE;
        setearDualList();
        isRenderStockMinimo = Boolean.FALSE;
    }

    public void cargarManualMente() {
        isCargarImagen = Boolean.TRUE;
        isTomarFoto = Boolean.FALSE;
        resetFiles();
        inicializarPhotoCam("registroProductos");
    }

    private void setearDualList() {
        List<T_Proveedores> productosSource = service.consultarProveedoresActuales();
        List<T_Proveedores> productosTarget = new ArrayList<>();
        proveedoresPick = new DualListModel<>(productosSource, productosTarget);
    }

    public void tomarFoto() {
        isCargarImagen = Boolean.FALSE;
        isTomarFoto = Boolean.TRUE;
        resetFiles();
        inicializarPhotoCam("registroProductos");
    }

    public void registrarProducto() {

        T_Productos nuevoProducto = new T_Productos();
        nuevoProducto.setDescripcion(descripcion);
        nuevoProducto.setMarca(marca);
        nuevoProducto.setModelo(modelo);
        nuevoProducto.setCodigo(codigoProducto);
        nuevoProducto.setFoto(isCargarImagen ? establecerImagen() : imagenTomada);
        nuevoProducto.setFechaRegistro(new Date());
        nuevoProducto.setStockMinimo(stockMinimoProducto);
        setearNuevosProveedores(nuevoProducto);
        administracionProductos.crearProducto(nuevoProducto);
        notificarMensajeExitoso("Se registro correctamente el producto");
        resetFiles();
        reset();

    }

    public beanRegistroProducto() {
    }

    public void notificarMensajeError(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", mensaje));
    }

    public void notificarMensajeExitoso(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion!", mensaje));
    }

    public void notificarMensajeErrorGlobal(String mensaje) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error !",
                mensaje);
        Messages.addFlashGlobal(msg);

    }

    private void setearNuevosProveedores(T_Productos nuevoProducto) {
        List<T_ProductosProveedores> productosXProveedores = new ArrayList<>();
        List<T_Proveedores> proveedores = proveedoresPick.getTarget();
        for (T_Proveedores proveedor : proveedores) {
            T_ProductosProveedores productoXProveedor = new T_ProductosProveedores();
            productoXProveedor.setIdProducto(nuevoProducto);
            productoXProveedor.setIdProveedor(proveedor);
            productosXProveedores.add(productoXProveedor);
        }
        nuevoProducto.setProductosXProvedores(productosXProveedores);
    }

    private void reset() {
        marca = "";
        modelo = "";
        descripcion = "";
        setearDualList();
    }

    public void mostrarMsjStockMinimo() {
        if (stockMinimo) {
            notificarMensajeExitoso("Agregue el stock minimo del producto a registrar");
            isRenderStockMinimo = Boolean.TRUE;
        } else {
            isRenderStockMinimo = Boolean.FALSE;
        }
    }

}
