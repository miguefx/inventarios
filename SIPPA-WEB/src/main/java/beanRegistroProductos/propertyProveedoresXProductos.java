/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanRegistroProductos;

import VO.T_Proveedores;
import beanProveedores.*;
import VO.T_Sedes;
import com.sippa.inventario.sippa.negocio.Proveedores.AdministracionProveedoresLocal;
import com.sippa.inventario.sippa.negocio.sedes.AdministracionSedesLocal;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author matc_
 */
@ManagedBean(name = "propertyProveedoresXProductos", eager = false)
@ApplicationScoped
public class propertyProveedoresXProductos {

    @EJB
    AdministracionProveedoresLocal administracionProveedores;

    @Getter
    @Setter
    private List<T_Proveedores> listDisponiblesProveedores;

    @Getter
    @Setter
    private List<T_Proveedores> listUsadosProveedores;

    /**
     * Creates a new instance of propertyClientesXSedes
     */
    @PostConstruct
    public void init() {
        listDisponiblesProveedores = administracionProveedores.consultarProveedoresXEstado(Boolean.TRUE);
        listUsadosProveedores = new ArrayList<>();
    }

    public propertyProveedoresXProductos() {
    }

}
