/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanKardex;

import VO.T_Bodegas;
import VO.T_Kardex;
import VO.T_Productos;
import VO.T_reporteKardex;
import VO.T_reporteKardexAux;
import com.sippa.inventario.sippa.negocio.Kardex.AdministracionKardexLocal;
import com.sippa.inventario.sippa.negocio.bodegas.AdministracionBodegasLocal;
import com.sippa.inventario.sippa.negocio.productos.AdministracionProductosLocal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Miguel-PC
 */
@ManagedBean(name = "beanGestionKardex")
@ViewScoped
public class beanGestionKardex {

    @EJB
    AdministracionKardexLocal administracionKardex;

    @EJB
    AdministracionBodegasLocal administracionBodegas;

    @EJB
    AdministracionProductosLocal administracionProductos;

    @Getter
    @Setter
    private List<T_Kardex> kardex;

    @Getter
    @Setter
    private List<T_reporteKardex> kardexReporteGeneral;

    @Getter
    @Setter
    private List<T_Kardex> filterKardex;

    @Getter
    @Setter
    private T_Bodegas bodega;

    @Getter
    @Setter
    private List<T_Bodegas> bodegas;

    @Getter
    @Setter
    private T_Productos producto;

    @Getter
    @Setter
    private List<T_Productos> productos;

    public beanGestionKardex() {
    }

    @PostConstruct
    public void init() {
        productos = administracionProductos.consultarProductosXEstado(Boolean.TRUE);
        bodegas = administracionBodegas.consultarBodegasXEstado(Boolean.TRUE);
    }

    public void consultar() {
        kardex = administracionKardex.findByProductoXBodegaHistorico(producto, bodega);
    }

    public void exportarKardex() {
        List<T_reporteKardexAux> listKardexDistinct = administracionKardex.findDistinct();
        List<T_reporteKardex> report = new ArrayList<>();
        for (T_reporteKardexAux t_Kardex : listKardexDistinct) {
            T_Productos objProducto = new T_Productos();
            objProducto.setIdProducto(t_Kardex.getIdProducto());
            T_Bodegas objBodega = new T_Bodegas();
            objBodega.setIdBodega(t_Kardex.getIdBodega());
            T_reporteKardex ObjReport = new T_reporteKardex();
            T_Kardex objKardex = administracionKardex.findByProductoXBodega(objProducto, objBodega);
            if (Objects.nonNull(objKardex)) {
                ObjReport.setNombreProducto(objKardex.getProducto().getDescripcion());
                ObjReport.setCodigoProducto(objKardex.getProducto().getCodigo());
                ObjReport.setCantidad(objKardex.getCantidadTotal());
                ObjReport.setUbicacion(objKardex.getBodega().getNombre());
                ObjReport.setValorUnitario(objKardex.getValorUnitario());
                ObjReport.setValorTotal(objKardex.getValorTotal());
                report.add(ObjReport);
            }
        }
        kardexReporteGeneral = report;
    }

    public String calcularColorCeldaConcepto(String concepto) {
        String resultado = "";
        try {
            switch (concepto) {
                case "Compra":
                    resultado = "compra";
                    break;
                case "Venta":
                    resultado = "venta";
                    break;
            }
        } catch (Exception e) {
        }
        return resultado;
    }
}
