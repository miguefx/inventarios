/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanGestionSedes;

import VO.T_Sedes;
import com.sippa.inventario.sippa.negocio.sedes.AdministracionSedesLocal;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author matc_
 */
@ManagedBean(name = "beanGestionSedes")
@ViewScoped
public class beanGestionSedes implements Serializable {

    @EJB
    AdministracionSedesLocal administracionSedes;

    @Getter
    @Setter
    private List<T_Sedes> listSedes;

    @Getter
    @Setter
    private List<T_Sedes> listSedesFilter;

    @Getter
    @Setter
    private T_Sedes sedeSelection;

    @PostConstruct
    public void init() {
        listSedes = new ArrayList<>();
        listSedesFilter = new ArrayList<>();
        consultarSedesDisponibles();
    }

    private void consultarSedesDisponibles() {
        listSedes = administracionSedes.obtenerSedesXEstado(Boolean.TRUE);
    }

    public void inactivarSede(T_Sedes sede) {
        sede.setEstado(Boolean.FALSE);
        administracionSedes.actualizarSede(sede);
        consultarSedesDisponibles();
        notificarMensajeExitoso("Se inactivo correctamente la sede seleccionada");
        
    }

    public beanGestionSedes() {
    }
    
    public void notificarMensajeError(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", mensaje));
    }

    public void notificarMensajeExitoso(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion!", mensaje));
    }
}
