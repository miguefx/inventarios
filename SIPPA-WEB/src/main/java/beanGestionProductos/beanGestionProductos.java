/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanGestionProductos;

import VO.T_Productos;
import com.sippa.inventario.sippa.negocio.productos.AdministracionProductosLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author matc_
 */
@ManagedBean(name = "beanGestionProductos")
@ViewScoped
public class beanGestionProductos implements Serializable {

    @EJB
    AdministracionProductosLocal administracionProductos;

    @Getter
    @Setter
    private List<T_Productos> listProductos;

    @Getter
    @Setter
    private List<T_Productos> listProductosFilter;

    @Getter
    @Setter
    private T_Productos selectionProducto;

    @PostConstruct
    public void init() {
        buscarProductosActivos();
    }

    private void buscarProductosActivos() {
        listProductos = administracionProductos.consultarProductosXEstado(Boolean.TRUE);
    }

    public void inactivarProducto(T_Productos producto) {
        producto.setEstado(Boolean.FALSE);
        administracionProductos.actualizarProducto(producto);
        buscarProductosActivos();
        notificarMensajeExitoso("Se inactivo correctamente el producto seleccionado");
    }

    public beanGestionProductos() {
    }

    public void notificarMensajeError(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", mensaje));
    }

    public void notificarMensajeExitoso(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion!", mensaje));
    }

}
