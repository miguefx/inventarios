/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanGestionUsuarios;

import VO.T_Usuarios;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author matc_
 */
import com.sippa.inventario.sippa.negocio.usuarios.AdministracionUsuariosLocal;
import java.io.Serializable;
import java.util.Objects;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.omnifaces.util.Messages;

@ManagedBean(name = "beanGestionUsuarios")
@ViewScoped
public class beanGestionUsuarios implements Serializable {

    @EJB
    private AdministracionUsuariosLocal administracionUsuarios;

    @Getter
    @Setter
    private List<T_Usuarios> usuarios;

    @Getter
    @Setter
    private T_Usuarios usuarioSeleccionado;

    @PostConstruct
    public void init() {
        buscarUsuariosActivos();
    }

    private void buscarUsuariosActivos() {
        usuarios = administracionUsuarios.buscarUsuariosXEstado(Boolean.TRUE);
    }

    public void bloquearUsuario(T_Usuarios usuarioElegido) {
        if (Objects.nonNull(usuarioElegido)) {
            usuarioElegido.setEstado(Boolean.FALSE);
            administracionUsuarios.actualizarUsuario(usuarioElegido);
            buscarUsuariosActivos();
            notificarMensajeExitoso("Fue bloqueado con exito el usuario");
        }
    }

    public void notificarMensajeError(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", mensaje));
    }

    public void notificarMensajeExitoso(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion!", mensaje));
    }

    public void notificarMensajeErrorGlobal(String mensaje) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error !",
                mensaje);
        Messages.addFlashGlobal(msg);

    }

    public beanGestionUsuarios() {
    }

}
