/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanGestionParametrizacionClientes;

import VO.T_Clientes;
import VO.T_ClientesXSedes;
import VO.T_Sedes;
import com.sippa.inventario.sippa.negocio.Clientes.AdministracionClientesLocal;
import com.sippa.inventario.sippa.negocio.sedes.AdministracionSedesLocal;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import lombok.Getter;
import lombok.Setter;
import org.omnifaces.util.Messages;
import org.primefaces.model.DualListModel;
import org.primefaces.ultima.service.SedesService;

/**
 *
 * @author matc_
 */
@ManagedBean(name = "beanParametrizacionClientes")
@ViewScoped
public class beanParametrizacionClientes  implements Serializable{

    @EJB
    AdministracionSedesLocal administracionSede;

    @EJB
    AdministracionClientesLocal administracionClientes;

    @Getter
    @Setter
    private Long idCliente;

    @Getter
    @Setter
    private T_Clientes clienteElegido;

    @Getter
    @Setter
    private List<T_Sedes> sedes;

    @Getter
    @Setter
    private DualListModel<T_Sedes> sedesPick;

     @ManagedProperty("#{sedeService}")
    private SedesService service;

    public void cargarInformacionCliente() {
        if (Objects.nonNull(idCliente)) {
            clienteElegido = administracionClientes.consultarClienteXId(idCliente);
            List<T_Sedes> sedesSource = service.consultarSedesActuales();
            sedesPick = new DualListModel<>(eliminarLosQueyaEstan(sedesSource), clienteElegido.getClientesXSedes().stream().map(mapper -> mapper.getIdSede()).collect(Collectors.toList()));
        }
    }

    public beanParametrizacionClientes() {
    }

    public void actualizarCliente() {
        eliminarDetalle(idCliente);
        setearDetalleCliente();
        administracionClientes.actualizarCliente(clienteElegido);
        notificarMensajeExitosoGlobal("Se ha actualizado correctamente el cliente");
    }

    private void setearDetalleCliente() {
        List<T_ClientesXSedes> listClientesXSedes = new ArrayList<>();
        sedesPick.getTarget().stream().forEach(sede -> {
            T_ClientesXSedes objVoClientesXSedes = new T_ClientesXSedes();
            objVoClientesXSedes.setIdCliente(clienteElegido);
            objVoClientesXSedes.setIdSede(sede);
            listClientesXSedes.add(objVoClientesXSedes);
        });
        clienteElegido.setClientesXSedes(listClientesXSedes);
    }


    private List<T_Sedes> eliminarLosQueyaEstan(List<T_Sedes> sedesSource) {
        System.out.println("Entro a eliminar");
        List<T_Sedes> sedesFinal = clienteElegido.getClientesXSedes().stream().map(sede -> sede.getIdSede()).collect(Collectors.toList());
        return sedesSource.stream().filter(sede -> !sedesFinal.contains(sede)).collect(Collectors.toList());
    }
    
     public SedesService getService() {
        return service;
    }

    public void setService(SedesService service) {
        this.service = service;
    }

    private void eliminarDetalle(Long idCliente) {
        administracionClientes.eliminarDetallesCliente(idCliente);
    }
    public void notificarMensajeExitosoGlobal(String mensaje) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion !",
                mensaje);
        Messages.addFlashGlobal(msg);
    }

    public void notificarMensajeErrorGlobal(String mensaje) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error !",
                mensaje);
        Messages.addFlashGlobal(msg);

    }
}
