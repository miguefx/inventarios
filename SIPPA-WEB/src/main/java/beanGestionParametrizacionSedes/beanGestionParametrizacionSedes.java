/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanGestionParametrizacionSedes;

import VO.T_Ciudades;
import VO.T_Sedes;
import VO.T_Usuarios;
import com.sippa.inventario.sippa.negocio.ciudades.AdministracionCiudadesLocal;
import com.sippa.inventario.sippa.negocio.sedes.AdministracionSedesLocal;
import com.sippa.inventario.sippa.negocio.usuarios.AdministracionUsuariosLocal;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import lombok.Getter;
import lombok.Setter;
import org.omnifaces.util.Messages;

/**
 *
 * @author matc_
 */
@ManagedBean(name = "beanGestionParametrizacionSedes")
@ViewScoped
public class beanGestionParametrizacionSedes implements Serializable {

    @EJB
    AdministracionUsuariosLocal administracionUsuarios;

    @EJB
    AdministracionCiudadesLocal administracionCiudades;

    @EJB
    AdministracionSedesLocal administracionSede;

    @Getter
    @Setter
    private Long idSede;

    @Getter
    @Setter
    private T_Sedes sedeAModificar;

    @Getter
    @Setter
    private List<T_Usuarios> usuarios;

    @Getter
    @Setter
    private List<T_Ciudades> ciudades;

    @PostConstruct
    public void init() {
        ciudades = new ArrayList<>();
        usuarios = new ArrayList<>();
        obtenerValoresCiudades();
        obtenerValoresUsuarios();
    }

    public void cargarSedeAModificar() {
        if (Objects.nonNull(idSede)) {
            sedeAModificar = administracionSede.consultarSedePorId(idSede);
        }
    }

    public beanGestionParametrizacionSedes() {
    }

    private void obtenerValoresUsuarios() {
        usuarios = administracionUsuarios.buscarUsuariosXEstado(Boolean.TRUE);
    }

    private void obtenerValoresCiudades() {
        ciudades = administracionCiudades.consultarCiudades();
    }

    public void actualizarSede() {
        try {
            administracionSede.actualizarSede(sedeAModificar);
            notificarMensajeExitosoGlobal("Se Actualizó la sede correctamente");
        } catch (Exception e) {
            System.out.println("Error Al Actualizar la sede" + e);
            notificarMensajeErrorGlobal("Hubo un error actualizando la sede");
        }
    }

    public void notificarMensajeExitosoGlobal(String mensaje) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion !",
                mensaje);
        Messages.addFlashGlobal(msg);
    }

    public void notificarMensajeErrorGlobal(String mensaje) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error !",
                mensaje);
        Messages.addFlashGlobal(msg);

    }

}
