package beanRegistroRecetaEnsamble;

import VO.T_DetalleRecetaDeEnsamble;
import VO.T_Productos;
import VO.T_RecetaDeEnsamble;
import com.sippa.inventario.sippa.negocio.productos.AdministracionProductosLocal;
import com.sippa.inventarios.sippa.ejb.recetaEnsamble.T_RecetaDeEnsambleFacadeLocal;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import lombok.Getter;
import lombok.Setter;

@ManagedBean(name = "beanRegistroRecetaEnsamble")
@ViewScoped
public class beanRegistroRecetaEnsamble {

    @EJB
    T_RecetaDeEnsambleFacadeLocal administracionRecetaEnsamble;

    @EJB
    AdministracionProductosLocal administracionProductos;

    private Connection conexion = null;

    @Getter
    @Setter
    private String nombreEnsamble;

    @Getter
    @Setter
    private BigDecimal totalOrden;

    @Getter
    @Setter
    private List<T_Productos> productos;

    @Getter
    @Setter
    private T_RecetaDeEnsamble recetaNueva;

    private static BigDecimal contador;


    @PostConstruct
    public void init() {
        contador = new BigDecimal(BigInteger.ZERO);
        totalOrden = BigDecimal.ZERO;
        productos = new ArrayList<>();
        productos = administracionProductos.consultarProductosXEstado(Boolean.TRUE);
        recetaNueva = new T_RecetaDeEnsamble();
        recetaNueva.setDetalleReceta(new ArrayList());
    }

    public void eliminarProducto(T_DetalleRecetaDeEnsamble detalle) {
        recetaNueva.getDetalleReceta().remove(detalle);
        actualizarValorTotal();
    }

    public void adicionarProductoDetalle() {
        contador = contador.add(BigDecimal.ONE);
        recetaNueva.getDetalleReceta().add(new T_DetalleRecetaDeEnsamble(contador, productos.stream().findFirst().get()));
    }

    public void obtenerTotal(T_DetalleRecetaDeEnsamble detalleSeleccionado) {
        if (Objects.nonNull(detalleSeleccionado.getCantidad()) && Objects.nonNull(detalleSeleccionado.getValorUnitario())) {
            detalleSeleccionado.setValorTotal(detalleSeleccionado.getCantidad().multiply(detalleSeleccionado.getValorUnitario()));
            actualizarValorTotal();
        }
    }

    public void actualizarValorTotal() {
        recalcularValorTotal();
    }

    private void recalcularValorTotal() {
        totalOrden = ((List<BigDecimal>) recetaNueva.getDetalleReceta().stream().map(mapper -> mapper.getValorTotal()).collect(Collectors.toList())).stream().reduce(BigDecimal::add).get();
    }

    public void registrarRecetaEnsamble() {
        try {
            setearObjetoPadre();
            Long idReceta = administracionRecetaEnsamble.registrarYObtenerId(recetaNueva);
            T_RecetaDeEnsamble receta = administracionRecetaEnsamble.find(idReceta);
            crearProductoPadre(receta);
            notificarMensajeExitoso("La receta fue creada exitosamente");
        } catch (Exception e) {
            notificarMensajeError("Hubo un error al crear la receta");
        }
    }

    public void notificarMensajeError(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", mensaje));
    }

    public void notificarMensajeExitoso(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion!", mensaje));
    }

    private void setearObjetoPadre() {
        recetaNueva.getDetalleReceta().forEach(receta -> receta.setIdReceta(recetaNueva));
        recetaNueva.setEstado(Boolean.TRUE);
        recetaNueva.setFechaCreacion(new Date());
    }

    private void crearProductoPadre(T_RecetaDeEnsamble receta) {
        T_Productos productoNuevo = new T_Productos();
        productoNuevo.setFechaRegistro(new Date());
        productoNuevo.setEstado(Boolean.TRUE);
        productoNuevo.setDescripcion(recetaNueva.getNombreReceta());
        productoNuevo.setCodigo(recetaNueva.getNombreReceta());
        productoNuevo.setRecetaReferenciada(receta);
        administracionProductos.crearProducto(productoNuevo);
    }

    private List<T_RecetaDeEnsamble> obtenerListaReceta(T_RecetaDeEnsamble recetaNueva) {
        List<T_RecetaDeEnsamble> receta = new ArrayList<>();
        receta.add(recetaNueva);
        return receta;
    }
}
