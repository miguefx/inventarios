/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanGestionClientes;

import VO.T_Clientes;
import com.sippa.inventario.sippa.negocio.Clientes.AdministracionClientesLocal;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author matc_
 */
@ManagedBean(name = "beanGestionClientes")
@ViewScoped
public class beanGestionClientes implements Serializable {

    @EJB
    AdministracionClientesLocal administracionClientes;

    @Getter
    @Setter
    private List<T_Clientes> listClientes;

    @Getter
    @Setter
    private List<T_Clientes> listClientesFilter;

    @Getter
    @Setter
    private T_Clientes clieteSeleccion;

    @PostConstruct
    public void init() {
        listClientes = new ArrayList<>();
        listClientesFilter = new ArrayList<>();
        consultarClientesDisponibles();
    }

    private void consultarClientesDisponibles() {
        listClientes = administracionClientes.obtenerClientesXEstado(Boolean.TRUE);
    }

    public void inactivarCliente(T_Clientes cliente) {
        cliente.setEstado(Boolean.FALSE);
        administracionClientes.actualizarCliente(cliente);
        consultarClientesDisponibles();
        notificarMensajeExitoso("Se inactivo correctamente el cliente seleccionado");
    }

    public beanGestionClientes() {
    }

    public void notificarMensajeError(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", mensaje));
    }

    public void notificarMensajeExitoso(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion!", mensaje));
    }
}
