/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanGestionBodegas;

import VO.T_Bodegas;
import com.sippa.inventario.sippa.negocio.bodegas.AdministracionBodegasLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author matc_
 */
@ManagedBean(name = "beanGestionBodegas")
@ViewScoped
public class beanGestionBodegas implements Serializable {

    @EJB
    AdministracionBodegasLocal administracionBodegas;

    @Getter
    @Setter
    private List<T_Bodegas> listBodegas;

    @Getter
    @Setter
    private List<T_Bodegas> listBodegasFilter;

    @Getter
    @Setter
    private T_Bodegas selectionBodega;

    @PostConstruct
    public void init() {
        consultarBodegasActivas();
    }

    private void consultarBodegasActivas() {
        listBodegas = administracionBodegas.consultarBodegasXEstado(Boolean.TRUE);
    }

    public void bloquearBodega(T_Bodegas bodega) {
        bodega.setEstado(Boolean.FALSE);
        administracionBodegas.actualizarBodegas(bodega);
        consultarBodegasActivas();
        notificarMensajeExitoso("Se inactivo correctamente la bodega seleccionada");
    }

    public beanGestionBodegas() {
    }

    public void notificarMensajeError(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", mensaje));
    }

    public void notificarMensajeExitoso(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion!", mensaje));
    }

}
