package beanGestionParametrizacionOrdenCompra;

import VO.Conexion;
import VO.T_DetalleOrdenDeCompra;
import VO.T_DetalleOrdenXSerialProductos;
import VO.T_DetalleRecetaDeEnsamble;
import VO.T_Inventarios;
import VO.T_InventariosSeriales;
import VO.T_OrdenDeCompra;
import VO.T_Productos;
import beanRegistroOrdenCompra.beanRegistroOrdenCompra;
import com.sippa.inventario.sippa.negocio.OrdenCompra.AdministacionOrdenCompraLocal;
import com.sippa.inventario.sippa.negocio.productos.AdministracionProductosLocal;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import lombok.Getter;
import lombok.Setter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.eclipse.jdt.internal.compiler.ast.Block;
import org.omnifaces.util.Messages;

@ManagedBean(name = "beanParametrizacionOrdenCompra")
@ViewScoped
public class beanParametrizacionOrdenCompra implements Serializable {
    
    @EJB
    AdministacionOrdenCompraLocal administracionOrdenDeCompra;
    
    @EJB
    AdministracionProductosLocal administracionProductos;
    
    @Getter
    @Setter
    private Long idOrdenCompra;
    
    @Getter
    @Setter
    private Boolean renderBotonAceptar;
    
    @Getter
    @Setter
    private BigDecimal totalOrden;
    
    @Getter
    @Setter
    private T_OrdenDeCompra ordenAModificar;
    
    @Getter
    @Setter
    private T_DetalleOrdenDeCompra detalleOrdenCompraSeleccionado;
    
    @Getter
    @Setter
    private T_DetalleOrdenDeCompra detalleSeleccionado;
    
    @Getter
    @Setter
    private List<T_Productos> productos;
    
    @Getter
    @Setter
    private T_DetalleOrdenXSerialProductos detalleOrdenSerialProductoSeleccionado;
    
    @Getter
    @Setter
    private List<T_DetalleOrdenXSerialProductos> detalleSeriales;
    
    private static BigDecimal contador;
    
    List<T_InventariosSeriales> detalleSerialesActuales;
    
    private Connection conexion = null;
    
    public void cargarInformacionOrdenCompra() {
        if (Objects.nonNull(idOrdenCompra)) {
            ordenAModificar = administracionOrdenDeCompra.buscarOrdenById(idOrdenCompra);
            detalleSeriales = new ArrayList<>();
            renderBotonAceptar = Boolean.TRUE;
            calcularTotalOrden();
            consultarProductosActuales();
            calcularContador();
        }
    }
    
    public void validarProductoReceta(T_DetalleOrdenDeCompra movimientoSeleccionado) {
        if (Objects.nonNull(movimientoSeleccionado.getProducto().getRecetaReferenciada())) {
            List<T_DetalleRecetaDeEnsamble> detalleReceta = movimientoSeleccionado.getProducto().getRecetaReferenciada().getDetalleReceta();
            movimientoSeleccionado.setValorUnitario(((List<BigDecimal>) detalleReceta.stream().map(detalle -> detalle.getValorTotal())
                    .collect(Collectors.toList())).stream().reduce(BigDecimal::add).get());
        }
    }
    
    private Long crearOrdenCompra(T_OrdenDeCompra objVoOrdenCompra) {
        return administracionOrdenDeCompra.crearOrdenCompra(objVoOrdenCompra);
    }
    
    public void actualizarOrdenCompra() {
        try {
            if (verificarNegocio()) {
                if (verificarCantidadQueNoSeaCero()) {
                    setearOrdenCompra();
                    ordenAModificar.setIdOrdenDeCompra(null);
                    Long id = crearOrdenCompra(ordenAModificar);
                    ordenAModificar.setIdOrdenDeCompra(id);
                    notificarMensajeExitosoGlobal("Se actualizo correctamente la orden de compra");
                    exportarPdfMensual();
                }
            } else {
                notificarMensajeError("El producto esta repetido.");
            }
        } catch (Exception e) {
            notificarMensajeErrorGlobal("Error actualizar orden de compra");
            System.out.println("Error actualizar orden compra");
        }
    }
    
    private String formatearFecha(Date fecha) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(fecha);
    }
    
    private void exportarPdfMensual() throws JRException, IOException {
        try {
            conexion = Conexion.getConexion();
        } catch (SQLException ex) {
            Logger.getLogger(beanRegistroOrdenCompra.class.getName()).log(Level.SEVERE, (String) null, ex);
        }
        Map<String, Object> parametros = new HashMap<>();
        parametros.put("numeroOrden", ordenAModificar.getIdOrdenDeCompra().toString());
        parametros.put("fechaOrden", formatearFecha(ordenAModificar.getFecha()));
        parametros.put("nombreContacto", ordenAModificar.getNombreContacto());
        parametros.put("apellidoContacto", ordenAModificar.getApellidoContacto());
        parametros.put("empresa", ordenAModificar.getEmpresa());
        parametros.put("nit", ordenAModificar.getNit());
        parametros.put("direccion", ordenAModificar.getDireccion());
        parametros.put("contactos", ordenAModificar.getContactos());
        parametros.put("ciudad", ordenAModificar.getCiudad());
        parametros.put("telefono", ordenAModificar.getTelefono());
        parametros.put("correo", ordenAModificar.getCorreo());
        parametros.put("tipoDeCompra", ordenAModificar.getTipoDeCompra());
        parametros.put("subtotal", totalOrden.equals(BigDecimal.ZERO) ? "0" : formatearBigDecimal(totalOrden));
        parametros.put("impuestos", ordenAModificar.getImpuesto().equals(BigDecimal.ZERO) ? "0" : formatearBigDecimal(calcularImpuesto()));
        parametros.put("totalOrden", totalOrden.equals(BigDecimal.ZERO) ? "0" : formatearBigDecimal(totalOrden.add(calcularImpuesto())));
        parametros.put("formaDePago", ordenAModificar.getFormaPago());
        parametros.put("direccionEntrega", ordenAModificar.getDireccionEntrega());
        parametros.put("terminoNegociacion", ordenAModificar.getTerminoNegociacion());
        parametros.put("procesados", ordenAModificar.getProcesado());
        parametros.put("observaciones", ordenAModificar.getObservaciones());
        parametros.put("aprobado", ordenAModificar.getAprobado());
        parametros.put("idOrdenCompra", ordenAModificar.getIdOrdenDeCompra());
        File jasper = new File("C:\\Oracle\\Middleware\\Oracle_Home\\OrdenCompra.jasper");
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasper.getPath(), parametros, conexion);
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        response.addHeader("Content-disposition", "attachment; filename=ReporteOrdenCompra.pdf");
        ServletOutputStream stream = response.getOutputStream();
        JasperExportManager.exportReportToPdfStream(jasperPrint, stream);
        stream.flush();
        stream.close();
        FacesContext.getCurrentInstance().responseComplete();
    }
    
    private BigDecimal calcularImpuesto() {
        return totalOrden.multiply(ordenAModificar.getImpuesto()).divide(new BigDecimal("100"), RoundingMode.HALF_UP);
    }
    
    private String formatearBigDecimal(BigDecimal numero) {
        DecimalFormat formatea = new DecimalFormat("###,###");
        return formatea.format(numero);
    }
    
    public void notificarMensajeExitosoGlobal(String mensaje) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion !", mensaje);
        Messages.addFlashGlobal(msg);
    }
    
    public void notificarMensajeErrorGlobal(String mensaje) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error !", mensaje);
        Messages.addFlashGlobal(msg);
    }
    
    public void obtenerTotal(T_DetalleOrdenDeCompra detalleSeleccionado) {
        System.out.println("Entro a calcular total");
        if (Objects.nonNull(detalleSeleccionado.getCantidad()) && Objects.nonNull(detalleSeleccionado.getValorUnitario())) {
            detalleSeleccionado.setValorTotal(detalleSeleccionado.getCantidad().multiply(detalleSeleccionado.getValorUnitario()));
            actualizarValorTotal();
        }
    }
    
    public void actualizarValorTotal() {
        calcularTotalOrden();
    }
    
    public void eliminarProducto(T_DetalleOrdenDeCompra detalle) {
        ordenAModificar.getDetalleOrdenDeCompra().remove(detalle);
        actualizarValorTotal();
    }
    
    private void calcularTotalOrden() {
        totalOrden = ((List<BigDecimal>) ordenAModificar.getDetalleOrdenDeCompra().stream().map(mapper -> mapper.getValorTotal()).collect(Collectors.toList())).stream().reduce(BigDecimal::add).get();
    }
    
    public void adicionarProductoDetalle() {
        contador = contador.add(BigDecimal.ONE);
        ordenAModificar.getDetalleOrdenDeCompra().add(new T_DetalleOrdenDeCompra(contador, productos.stream().findFirst().get()));
    }
    
    private void consultarProductosActuales() {
        productos = administracionProductos.consultarProductosXEstado(Boolean.TRUE);
    }
    
    private void calcularContador() {
        contador = BigDecimal.valueOf(ordenAModificar.getDetalleOrdenDeCompra().stream().mapToInt(mapper -> mapper.getItem().intValue()).max().getAsInt()).movePointLeft(0);
    }
    
    public void validarListaLlena(T_DetalleOrdenXSerialProductos seleccion) {
        if (detalleSeriales.stream().filter(detalle -> detalle.getIdProducto().equals(seleccion.getIdProducto())).allMatch(detalle -> !detalle.getSerial().equals("Ingrese el serial del producto"))) {
            renderBotonAceptar = Boolean.FALSE;
            List<T_DetalleOrdenXSerialProductos> listFiltrado = (List<T_DetalleOrdenXSerialProductos>) detalleSeriales.stream().filter(detalleSerial -> detalleSerial.getIdProducto().equals(detalleSeleccionado.getProducto())).collect(Collectors.toList());
            detalleSeleccionado.setDetalleSerialesProductos(listFiltrado);
            
        }
    }
    
    public void notificarMensajeCargadoCompleto() {
        renderBotonAceptar = Boolean.TRUE;
        notificarMensajeExitoso("Se ha cargado en su totalidad los seriales del producto " + detalleSeleccionado.getProducto().getDescripcion());
    }
    
    public void notificarMensajeError(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", mensaje));
    }
    
    public void notificarMensajeExitoso(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion!", mensaje));
    }
    
    public void añadirCamposParaSeriales(T_DetalleOrdenDeCompra detalle) {
        detalleSeleccionado = detalle;
        if (!detalleSeriales.isEmpty()) {
            detalleSeriales.clear();
        }
        if (Objects.nonNull(detalle.getDetalleSerialesProductos())) {
            detalleSeriales = detalle.getDetalleSerialesProductos();
        } else {
            while (detalleSeriales.size() < detalle.getCantidad().intValue()) {
                T_DetalleOrdenXSerialProductos detalleSerial = new T_DetalleOrdenXSerialProductos(detalle.getProducto());
                detalleSerial.setSerial("Ingrese el serial del producto");
                detalleSerial.setIdDetalleOrdenDeCompra(detalle);
                detalleSeriales.add(detalleSerial);
            }
        }
    }
    
    private boolean verificarNegocio() {
        return ordenAModificar.getDetalleOrdenDeCompra().stream().map(T_DetalleOrdenDeCompra::getProducto).distinct().count() == ordenAModificar.getDetalleOrdenDeCompra().size();
    }
    
    private boolean verificarCantidadQueNoSeaCero() {
        boolean bandera = true;
        for (T_DetalleOrdenDeCompra detalle : ordenAModificar.getDetalleOrdenDeCompra()) {
            if (detalle.getCantidad() == BigDecimal.ZERO) {
                bandera = false;
                notificarMensajeError("La cantidad del producto " + detalle.getProducto().getDescripcion() + " No puede ser ");
            }
        }
        return bandera;
    }
    
    private void setearOrdenCompra() {
        for (T_DetalleOrdenDeCompra producto : ordenAModificar.getDetalleOrdenDeCompra()) {
            producto.setIdOrdenDeCompra(ordenAModificar);
        }
    }
}
