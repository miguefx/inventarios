/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanGestionOrdenDeCompra;

import VO.Conexion;
import beanGestionClientes.*;
import VO.T_Clientes;
import VO.T_OrdenDeCompra;
import beanRegistroOrdenCompra.beanRegistroOrdenCompra;
import com.sippa.inventario.sippa.negocio.Clientes.AdministracionClientesLocal;
import com.sippa.inventario.sippa.negocio.OrdenCompra.AdministacionOrdenCompraLocal;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import lombok.Getter;
import lombok.Setter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

/**
 *
 * @author matc_
 */
@ManagedBean(name = "beanGestionOrdenCompra")
@ViewScoped
public class beanGestionOrdenCompra implements Serializable {

    @EJB
    AdministacionOrdenCompraLocal administracionOrdenDeCompra;

    @Getter
    @Setter
    private List<T_OrdenDeCompra> listOrdenCompra;

    @Getter
    @Setter
    private List<T_OrdenDeCompra> listOrdenCompraFilter;

    @Getter
    @Setter
    private T_OrdenDeCompra ordenCompra;

    private Connection conexion = null;

    @PostConstruct
    public void init() {
        listOrdenCompra = new ArrayList<>();
        listOrdenCompraFilter = new ArrayList<>();
        consultarOrdenesDeCompra();
    }

    private void consultarOrdenesDeCompra() {
        listOrdenCompra = administracionOrdenDeCompra.obtenerOrdenesXEstado(Boolean.TRUE);
    }

    private String formatearFecha(Date fecha) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(fecha);
    }

    public void imprimir(T_OrdenDeCompra orden) throws JRException, IOException {
        try {
            conexion = Conexion.getConexion();
        } catch (SQLException ex) {
            Logger.getLogger(beanRegistroOrdenCompra.class.getName()).log(Level.SEVERE, (String) null, ex);
        }
        BigDecimal totalOrden = ((List<BigDecimal>) orden.getDetalleOrdenDeCompra().stream().map(mapper -> mapper.getValorTotal()).collect(Collectors.toList())).stream().reduce(BigDecimal::add).get();
        Map<String, Object> parametros = new HashMap<>();
        parametros.put("numeroOrden", orden.getIdOrdenDeCompra().toString());
        parametros.put("fechaOrden", formatearFecha(orden.getFecha()));
        parametros.put("nombreContacto", orden.getNombreContacto());
        parametros.put("apellidoContacto", orden.getApellidoContacto());
        parametros.put("empresa", orden.getEmpresa());
        parametros.put("nit", orden.getNit());
        parametros.put("direccion", orden.getDireccion());
        parametros.put("contactos", orden.getContactos());
        parametros.put("ciudad", orden.getCiudad());
        parametros.put("telefono", orden.getTelefono());
        parametros.put("correo", orden.getCorreo());
        parametros.put("tipoDeCompra", orden.getTipoDeCompra());
        parametros.put("subtotal", totalOrden.equals(BigDecimal.ZERO) ? "0" : formatearBigDecimal(totalOrden));
        parametros.put("impuestos", orden.getImpuesto().equals(BigDecimal.ZERO) ? "0" : formatearBigDecimal(calcularImpuesto(totalOrden,orden)));
        parametros.put("totalOrden", totalOrden.equals(BigDecimal.ZERO) ? "0" : formatearBigDecimal(totalOrden.add(calcularImpuesto(totalOrden,orden))));
        parametros.put("formaDePago", orden.getFormaPago());
        parametros.put("direccionEntrega", orden.getDireccionEntrega());
        parametros.put("terminoNegociacion", orden.getTerminoNegociacion());
        parametros.put("procesados", orden.getProcesado());
        parametros.put("observaciones", orden.getObservaciones());
        parametros.put("aprobado", orden.getAprobado());
        parametros.put("idOrdenCompra", orden.getIdOrdenDeCompra());
        File jasper = new File("C:\\Oracle\\Middleware\\Oracle_Home\\OrdenCompra.jasper");
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasper.getPath(), parametros, conexion);
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        response.addHeader("Content-disposition", "attachment; filename=ReporteOrdenCompra.pdf");
        ServletOutputStream stream = response.getOutputStream();
        JasperExportManager.exportReportToPdfStream(jasperPrint, stream);
        stream.flush();
        stream.close();
        FacesContext.getCurrentInstance().responseComplete();
    }

    private BigDecimal calcularImpuesto(BigDecimal totalOrden,T_OrdenDeCompra orden) {
        return totalOrden.multiply(orden.getImpuesto()).divide(new BigDecimal("100"), RoundingMode.HALF_UP);
    }

    private String formatearBigDecimal(BigDecimal numero) {
        DecimalFormat formatea = new DecimalFormat("###,###");
        return formatea.format(numero);
    }

    public void inactivarOrdenCompra(T_OrdenDeCompra orden) {
        orden.setEstado(Boolean.FALSE);
        administracionOrdenDeCompra.actualizarOrden(orden);
        consultarOrdenesDeCompra();
        notificarMensajeExitoso("Se inactivo correctamente la orden seleccionada");
    }

    public beanGestionOrdenCompra() {
    }

    public void notificarMensajeError(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", mensaje));
    }

    public void notificarMensajeExitoso(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion!", mensaje));
    }
}
