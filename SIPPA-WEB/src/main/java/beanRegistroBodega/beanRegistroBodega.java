/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanRegistroBodega;

import VO.T_Bodegas;
import VO.T_Sedes;
import VO.T_Usuarios;
import com.sippa.inventario.sippa.negocio.bodegas.AdministracionBodegasLocal;
import com.sippa.inventario.sippa.negocio.sedes.AdministracionSedesLocal;
import com.sippa.inventario.sippa.negocio.usuarios.AdministracionUsuariosLocal;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import lombok.Getter;
import lombok.Setter;
import org.omnifaces.util.Messages;

/**
 *
 * @author matc_
 */
@ManagedBean(name = "beanRegistroBodega")
@ViewScoped
public class beanRegistroBodega implements Serializable {

    @EJB
    AdministracionSedesLocal administracionSede;

    @EJB
    AdministracionUsuariosLocal administracionUsuarios;

    @EJB
    AdministracionBodegasLocal administracionBodegas;

    @Getter
    @Setter
    private String nombre;

    @Getter
    @Setter
    private String direccion;

    @Getter
    @Setter
    private String telefono;

    @Getter
    @Setter
    private List<T_Usuarios> usuarios;

    @Getter
    @Setter
    private T_Usuarios usuarioResponsable;

    @Getter
    @Setter
    private T_Sedes sede;

    @Getter
    @Setter
    private List<T_Sedes> sedes;

    @PostConstruct
    public void init() {
        sedes = new ArrayList<>();
        usuarios = new ArrayList<>();
        obtenerValoresDeSede();
        obtenerValoresUsuariosResponsables();

    }

    public beanRegistroBodega() {
    }

    private void obtenerValoresDeSede() {
        sedes = administracionSede.obtenerSedesXEstado(Boolean.TRUE);
        if (Objects.nonNull(sedes)) {
            if (!sedes.isEmpty()) {
                sede = sedes.stream().findFirst().get();
            }
        }
    }

    private void obtenerValoresUsuariosResponsables() {
        usuarios = administracionUsuarios.buscarUsuariosXEstado(Boolean.TRUE);
        if (Objects.nonNull(usuarios)) {
            if (!usuarios.isEmpty()) {
                usuarioResponsable = usuarios.stream().findFirst().get();
            }
        }
    }

    public void guardarBodega() {
        T_Bodegas nuevaBodega = new T_Bodegas();
        nuevaBodega.setSede(sede);
        nuevaBodega.setDireccion(direccion);
        nuevaBodega.setNombre(nombre);
        nuevaBodega.setTelefono(telefono);
        nuevaBodega.setUsuario(usuarioResponsable);
        nuevaBodega.setEstado(Boolean.TRUE);
        administracionBodegas.crearBodega(nuevaBodega);
        notificarMensajeExitoso("La bodega se registro exitosamente");
    }

    public void notificarMensajeError(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", mensaje));
    }

    public void notificarMensajeExitoso(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion!", mensaje));
    }

    public void notificarMensajeErrorGlobal(String mensaje) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error !",
                mensaje);
        Messages.addFlashGlobal(msg);

    }

}
