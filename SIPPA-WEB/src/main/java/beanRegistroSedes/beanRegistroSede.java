/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanRegistroSedes;

import VO.T_Ciudades;
import VO.T_Sedes;
import VO.T_Usuarios;
import com.sippa.inventario.sippa.negocio.ciudades.AdministracionCiudadesLocal;
import com.sippa.inventario.sippa.negocio.sedes.AdministracionSedesLocal;
import com.sippa.inventario.sippa.negocio.usuarios.AdministracionUsuariosLocal;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import lombok.Getter;
import lombok.Setter;
import org.omnifaces.util.Messages;

/**
 *
 * @author matc_
 */
@ManagedBean(name = "beanRegistroSede")
@ViewScoped
public class beanRegistroSede implements Serializable{

    @EJB
    AdministracionUsuariosLocal administracionUsuarios;

    @EJB
    AdministracionSedesLocal administracionSedes;

    @EJB
    AdministracionCiudadesLocal administracionCiudades;

    @Getter
    @Setter
    private String nombre;

    @Getter
    @Setter
    private List<T_Ciudades> ciudades;

    @Getter
    @Setter
    private T_Ciudades ciudad;

    @Getter
    @Setter
    private String direccion;

    @Getter
    @Setter
    private List<T_Usuarios> usuarios;

    @Getter
    @Setter
    private T_Usuarios usuario;

    @Getter
    @Setter
    private String telefono;

    @PostConstruct
    public void init() {
        ciudades = new ArrayList<>();
        usuarios = new ArrayList<>();
        obtenerValoresCiudades();
        obtenerValoresUsuarios();
    }

    public void registrarSede() {
        T_Sedes nuevaSede = new T_Sedes();
        nuevaSede.setCiudad(ciudad);
        nuevaSede.setDireccion(direccion);
        nuevaSede.setNombre(nombre);
        nuevaSede.setTelefono(telefono);
        nuevaSede.setUsuario(usuario);
        nuevaSede.setEstado(Boolean.TRUE);
        administracionSedes.crearSede(nuevaSede);
        notificarMensajeExitoso("La sede fue registrada exitosamente");
        resetFiles();
    }

    public beanRegistroSede() {
    }

    private void obtenerValoresCiudades() {
        ciudades = administracionCiudades.consultarCiudades();
        ciudad=ciudades.stream().findFirst().get();
    }

    private void obtenerValoresUsuarios() {
        usuarios = administracionUsuarios.buscarUsuariosXEstado(Boolean.TRUE);
        usuario=usuarios.stream().findFirst().get();
    }
    
     public void notificarMensajeError(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", mensaje));
    }

    public void notificarMensajeExitoso(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion!", mensaje));
    }

    public void notificarMensajeErrorGlobal(String mensaje) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error !",
                mensaje);
        Messages.addFlashGlobal(msg);

    }

    private void resetFiles() {
        nombre="";
        telefono="";
        direccion="";
    }

}
