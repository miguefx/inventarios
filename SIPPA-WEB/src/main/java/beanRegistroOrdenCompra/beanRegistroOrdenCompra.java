package beanRegistroOrdenCompra;

import VO.Conexion;
import VO.T_DetalleOrdenDeCompra;
import VO.T_DetalleOrdenXSerialProductos;
import VO.T_DetalleRecetaDeEnsamble;
import VO.T_OrdenDeCompra;
import VO.T_Productos;
import com.sippa.inventario.sippa.negocio.OrdenCompra.AdministacionOrdenCompraLocal;
import com.sippa.inventario.sippa.negocio.detalleOrdenCompra.AdministracionDetalleOrdenCompraLocal;
import com.sippa.inventario.sippa.negocio.productos.AdministracionProductosLocal;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import lombok.Getter;
import lombok.Setter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

@ManagedBean(name = "beanRegistroOrdenCompra")
@ViewScoped
public class beanRegistroOrdenCompra {

    @EJB
    AdministacionOrdenCompraLocal administracionOrdenCompra;

    @EJB
    AdministracionDetalleOrdenCompraLocal administracionDetalleOrdenCompra;

    @EJB
    AdministracionProductosLocal administracionProductos;

    private Connection conexion = null;

    @Getter
    @Setter
    private String nombreContacto;

    @Getter
    @Setter
    private Date fecha;

    @Getter
    @Setter
    private String apellidoContacto;

    @Getter
    @Setter
    private String empresa;

    @Getter
    @Setter
    private String nit;

    @Getter
    @Setter
    private Boolean renderBotonAceptar;

    @Getter
    @Setter
    private String direccion;

    @Getter
    @Setter
    private String contactos;

    @Getter
    @Setter
    private String ciudad;

    @Getter
    @Setter
    private String telefono;

    @Getter
    @Setter
    private String correo;

    @Getter
    @Setter
    private String terminoNegociacion;

    @Getter
    @Setter
    private String formaDePago;

    @Getter
    @Setter
    private String tipoCompra;

    @Getter
    @Setter
    private String direccionEntrega;

    @Getter
    @Setter
    private String procesado;

    @Getter
    @Setter
    private String aprobado;

    @Getter
    @Setter
    private String observaciones;

    @Getter
    @Setter
    private BigDecimal subtotal;

    @Getter
    @Setter
    private BigDecimal impuesto;

    @Getter
    @Setter
    private BigDecimal totalOrden;

    @Getter
    @Setter
    private List<T_Productos> productos;

    @Getter
    @Setter
    private List<T_DetalleOrdenDeCompra> listDetalleOrdenDeCompra;

    @Getter
    @Setter
    private T_DetalleOrdenDeCompra detalleSeleccion;

    @Getter
    @Setter
    private T_DetalleOrdenXSerialProductos detalleSeleccionSerialProducto;

    @Getter
    @Setter
    private List<T_DetalleOrdenXSerialProductos> detalleSeriales;

    private static BigDecimal contador;

    @PostConstruct
    public void init() {
        detalleSeriales = new ArrayList<>();
        listDetalleOrdenDeCompra = new ArrayList<>();
        contador = new BigDecimal(BigInteger.ZERO);
        subtotal = BigDecimal.ZERO;
        totalOrden = BigDecimal.ZERO;
        renderBotonAceptar = Boolean.TRUE;
        productos = new ArrayList<>();
        productos = administracionProductos.consultarProductosXEstado(Boolean.TRUE);
    }

    public void notificarMensajeCargadoCompleto() {
        renderBotonAceptar = Boolean.TRUE;
        notificarMensajeExitoso("Se ha cargado en su totalidad los seriales del producto " + detalleSeleccion.getProducto().getDescripcion());
    }

    public void validarListaLlena(T_DetalleOrdenXSerialProductos seleccion) {
        if (detalleSeriales.stream().filter(detalle -> detalle.getIdProducto().equals(seleccion.getIdProducto())).allMatch(detalle -> !detalle.getSerial().equals("Ingrese el serial del producto"))) {
            renderBotonAceptar = Boolean.FALSE;
            List<T_DetalleOrdenXSerialProductos> listFiltrado = (List<T_DetalleOrdenXSerialProductos>) detalleSeriales.stream().filter(detalleSerial -> detalleSerial.getIdProducto().equals(detalleSeleccion.getProducto())).collect(Collectors.toList());
            detalleSeleccion.setDetalleSerialesProductos(listFiltrado);
        }
    }

    public void eliminarProducto(T_DetalleOrdenDeCompra detalle) {
        listDetalleOrdenDeCompra.remove(detalle);
        actualizarValorTotal();
    }

    public void validarProductoReceta(T_DetalleOrdenDeCompra movimientoSeleccionado) {
        if (Objects.nonNull(movimientoSeleccionado.getProducto().getRecetaReferenciada())) {
            List<T_DetalleRecetaDeEnsamble> detalleReceta = movimientoSeleccionado.getProducto().getRecetaReferenciada().getDetalleReceta();
            movimientoSeleccionado.setValorUnitario(((List<BigDecimal>) detalleReceta.stream().map(detalle -> detalle.getValorTotal())
                    .collect(Collectors.toList())).stream().reduce(BigDecimal::add).get());
        }
    }

    public void añadirCamposParaSeriales(T_DetalleOrdenDeCompra detalle) {
        detalleSeleccion = detalle;
        if (!detalleSeriales.isEmpty()) {
            detalleSeriales.clear();
        }
        if (Objects.nonNull(detalle.getDetalleSerialesProductos())) {
            detalleSeriales = detalle.getDetalleSerialesProductos();
        } else {
            while (detalleSeriales.size() < detalle.getCantidad().intValue()) {
                T_DetalleOrdenXSerialProductos detalleSerial = new T_DetalleOrdenXSerialProductos(detalle.getProducto());
                detalleSerial.setSerial("Ingrese el serial del producto");
                detalleSerial.setIdDetalleOrdenDeCompra(detalle);
                detalleSeriales.add(detalleSerial);
            }
        }
    }

    public void adicionarProductoDetalle() {
        contador = contador.add(BigDecimal.ONE);
        listDetalleOrdenDeCompra.add(new T_DetalleOrdenDeCompra(contador, productos.stream().findFirst().get()));
    }

    public void obtenerTotal(T_DetalleOrdenDeCompra detalleSeleccionado) {
        System.out.println("Entro a calcular total");
        if (Objects.nonNull(detalleSeleccionado.getCantidad()) && Objects.nonNull(detalleSeleccionado.getValorUnitario())) {
            detalleSeleccionado.setValorTotal(detalleSeleccionado.getCantidad().multiply(detalleSeleccionado.getValorUnitario()));
            actualizarValorTotal();
        }
    }

    public void actualizarValorTotal() {
        recalcularValorTotal();
    }

    private void recalcularValorTotal() {
        totalOrden = ((List<BigDecimal>) listDetalleOrdenDeCompra.stream().map(mapper -> mapper.getValorTotal()).collect(Collectors.toList())).stream().reduce(BigDecimal::add).get();
    }

    public void registrarOrdenCompra() {
        try {
            if (!verificarRepetidosDetalles()) {
                notificarMensajeError("El detalle no puede tener el mismo producto mas de una vez");
            } else {
                Long id = generarRegistroOrdenDeCompra();
                exportarPdfMensual(id);
            }
        } catch (Exception e) {
            System.out.println("Ocurrio un error generando reporte " + e);
        }
    }

    private void exportarPdfMensual(Long id) throws JRException, IOException {
        try {
            conexion = Conexion.getConexion();
        } catch (SQLException ex) {
            Logger.getLogger(beanRegistroOrdenCompra.class.getName()).log(Level.SEVERE, (String) null, ex);
        }
        Map<String, Object> parametros = new HashMap<>();
        parametros.put("numeroOrden", id.toString());
        parametros.put("fechaOrden", formatearFecha(fecha));
        parametros.put("nombreContacto", nombreContacto);
        parametros.put("apellidoContacto", apellidoContacto);
        parametros.put("empresa", empresa);
        parametros.put("nit", nit);
        parametros.put("direccion", direccion);
        parametros.put("contactos", contactos);
        parametros.put("ciudad", ciudad);
        parametros.put("telefono", telefono);
        parametros.put("correo", correo);
        parametros.put("tipoDeCompra", tipoCompra);
        parametros.put("subtotal", totalOrden.equals(BigDecimal.ZERO) ? "0" : formatearBigDecimal(totalOrden));
        parametros.put("impuestos", impuesto.equals(BigDecimal.ZERO) ? "0" : formatearBigDecimal(calcularImpuesto()));
        parametros.put("totalOrden", totalOrden.equals(BigDecimal.ZERO) ? "0" : formatearBigDecimal(totalOrden.add(calcularImpuesto())));
        parametros.put("formaDePago", formaDePago);
        parametros.put("direccionEntrega", direccionEntrega);
        parametros.put("terminoNegociacion", terminoNegociacion);
        parametros.put("procesados", procesado);
        parametros.put("observaciones", observaciones);
        parametros.put("aprobado", aprobado);
        parametros.put("idOrdenCompra", id);
        File jasper = new File("C:\\Oracle\\Middleware\\Oracle_Home\\OrdenCompra.jasper");
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasper.getPath(), parametros, conexion);
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        response.addHeader("Content-disposition", "attachment; filename=ReporteOrdenCompra.pdf");
        ServletOutputStream stream = response.getOutputStream();

        JasperExportManager.exportReportToPdfStream(jasperPrint, stream);

        stream.flush();
        stream.close();
        FacesContext.getCurrentInstance().responseComplete();
    }

    private Long generarRegistroOrdenDeCompra() {
        T_OrdenDeCompra objVoOrdenCompra = new T_OrdenDeCompra();
        setearValoresDeOrdenCompra(objVoOrdenCompra);
        setearDetalle(objVoOrdenCompra);
        objVoOrdenCompra.setDetalleOrdenDeCompra(listDetalleOrdenDeCompra);
        return crearOrdenCompra(objVoOrdenCompra);
    }

    private String formatearBigDecimal(BigDecimal numero) {
        DecimalFormat formatea = new DecimalFormat("###,###");
        return formatea.format(numero);
    }

    private void setearValoresDeOrdenCompra(T_OrdenDeCompra objVoOrdenCompra) {
        objVoOrdenCompra.setNombreContacto(nombreContacto);
        objVoOrdenCompra.setApellidoContacto(apellidoContacto);
        objVoOrdenCompra.setCiudad(ciudad);
        objVoOrdenCompra.setDireccion(direccionEntrega);
        objVoOrdenCompra.setContactos(contactos);
        objVoOrdenCompra.setCorreo(correo);
        objVoOrdenCompra.setDireccion(direccion);
        objVoOrdenCompra.setEmpresa(empresa);
        objVoOrdenCompra.setFecha(fecha);
        objVoOrdenCompra.setNit(nit);
        objVoOrdenCompra.setTelefono(telefono);
        objVoOrdenCompra.setObservaciones(observaciones);
        objVoOrdenCompra.setTipoDeCompra(tipoCompra);
        objVoOrdenCompra.setTerminoNegociacion(terminoNegociacion);
        objVoOrdenCompra.setFormaPago(formaDePago);
        objVoOrdenCompra.setDireccionEntrega(direccionEntrega);
        objVoOrdenCompra.setProcesado(procesado);
        objVoOrdenCompra.setAprobado(aprobado);
        objVoOrdenCompra.setEstado(Boolean.TRUE);
        objVoOrdenCompra.setImpuesto(impuesto);
    }

    private void setearDetalle(T_OrdenDeCompra objVoOrdenCompra) {
        listDetalleOrdenDeCompra.stream().forEach(detalle -> detalle.setIdOrdenDeCompra(objVoOrdenCompra));
    }

    private Long crearOrdenCompra(T_OrdenDeCompra objVoOrdenCompra) {
        return administracionOrdenCompra.crearOrdenCompra(objVoOrdenCompra);
    }

    private String formatearFecha(Date fecha) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(fecha);
    }

    private boolean verificarRepetidosDetalles() {
        List<T_Productos> listaInicial = (List<T_Productos>) listDetalleOrdenDeCompra.stream().map(detalle -> detalle.getProducto()).collect(Collectors.toList());
        List<T_Productos> listaSinRepetidos = (List<T_Productos>) listaInicial.stream().distinct().collect(Collectors.toList());
        return Objects.equals(listaInicial.size(), listaSinRepetidos.size());
    }

    public void notificarMensajeError(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", mensaje));
    }

    public void notificarMensajeExitoso(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion!", mensaje));
    }

    private BigDecimal calcularImpuesto() {
        return totalOrden.multiply(impuesto).divide(new BigDecimal("100"), RoundingMode.HALF_UP);
    }
}
