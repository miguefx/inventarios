package beanGraficas;

import VO.GraficaStockMinimo;
import VO.T_Bodegas;
import VO.T_Inventarios;
import VO.T_Kardex;
import VO.T_Productos;
import VO.T_reporteKardex;
import VO.T_reporteKardexAux;
import com.sippa.inventario.sippa.negocio.Kardex.AdministracionKardexLocal;
import com.sippa.inventario.sippa.negocio.bodegas.AdministracionBodegas;
import com.sippa.inventario.sippa.negocio.bodegas.AdministracionBodegasLocal;
import com.sippa.inventario.sippa.negocio.inventarios.AdministracionInventariosLocal;
import com.sippa.inventario.sippa.negocio.parametros.AdministracionParametrosLocal;
import com.sippa.inventario.sippa.negocio.productos.AdministracionProductosLocal;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LegendPlacement;

@ManagedBean(name = "beanGraficas")
@ViewScoped
public class beanGraficas implements Serializable {

    @EJB
    AdministracionInventariosLocal administracionInventarios;


    @EJB
    AdministracionParametrosLocal administracionParametros;

    @EJB
    AdministracionProductosLocal administracionProductos;

    @EJB
    AdministracionBodegasLocal administracionBodegas;

    @EJB
    AdministracionKardexLocal administracionKardex;

    @Getter
    @Setter
    private BarChartModel barModelStockMinimo;

    @Getter
    @Setter
    private BarChartModel barModeTotalCosto;

    @Getter
    @Setter
    private List<GraficaStockMinimo> stockMinimo;


    @Getter
    @Setter
    private Integer numeroMaximo;

    @PostConstruct
    public void init() {
        stockMinimo = consultarInventarioExistente();
        stockMinimo = obtenerValoresGraficaStock();
        createBarModels();
    }

    private void createBarModels() {
        createBarModel();
    }

    private void createBarModel() {
        barModelStockMinimo = initBarModel();
        barModelStockMinimo.setTitle("Stock Minimo");
        barModelStockMinimo.setLegendPosition("e");
        barModelStockMinimo.setShowDatatip(true);
        barModelStockMinimo.setShowPointLabels(true);
        barModelStockMinimo.setAnimate(true);
        barModelStockMinimo.setLegendPlacement(LegendPlacement.OUTSIDEGRID);
        barModelStockMinimo.setLegendCols(barModelStockMinimo.getLegendCols());
        Axis xAxis = barModelStockMinimo.getAxis(AxisType.X);
        Axis yAxis = barModelStockMinimo.getAxis(AxisType.Y);
        yAxis.setLabel("Cantidad");
        yAxis.setMin(0);
        yAxis.setMax(20);
        barModeTotalCosto = initBarModelCostos();
        barModeTotalCosto.setTitle("Total Costo");
        barModeTotalCosto.setLegendPosition("e");
        barModeTotalCosto.setShowDatatip(true);
        barModeTotalCosto.setShowPointLabels(true);
        barModeTotalCosto.setAnimate(true);
        barModeTotalCosto.setDatatipFormat("%d - $ %i");
        barModeTotalCosto.setLegendPlacement(LegendPlacement.OUTSIDEGRID);
        Axis xAxisCostos = barModeTotalCosto.getAxis(AxisType.X);
        Axis yAxisCostos = barModeTotalCosto.getAxis(AxisType.Y);
        yAxisCostos.setLabel("Dinero");
        yAxisCostos.setMin(0);
        yAxisCostos.setMax(10000000);
    }

    private BarChartModel initBarModel() {
        BarChartModel model = new BarChartModel();
        List<ChartSeries> productos = new ArrayList<>();
        List<String> listaAComparar = (List<String>) stockMinimo.stream().map(producto -> producto.getProducto()).collect(Collectors.toList());
        productos = crearListaDeSeries(listaAComparar);
        for (GraficaStockMinimo graficaStockMinimo : stockMinimo) {
            for (ChartSeries producto : productos) {
                if (Objects.equals(graficaStockMinimo.getProducto(), producto.getLabel())) {
                    producto.set("Productos", graficaStockMinimo.getCantidad());
                }
            }
        }
        setearValoresAlModelo(model, productos);
        return model;
    }

    private BarChartModel initBarModelCostos() {
        BarChartModel model = new BarChartModel();
        List<GraficaStockMinimo> camposGrafica = new ArrayList<>();
        List<ChartSeries> productos = new ArrayList<>();
        camposGrafica = obtenerValoresGraficaCostos();
        List<String> listaAComparar = (List<String>) camposGrafica.stream().map(producto -> producto.getProducto()).collect(Collectors.toList());
        productos = crearListaDeSeries(listaAComparar);
        for (GraficaStockMinimo graficaStockMinimo : camposGrafica) {
            for (ChartSeries producto : productos) {
                if (Objects.equals(graficaStockMinimo.getProducto(), producto.getLabel())) {
                    producto.set("Sedes", graficaStockMinimo.getCantidad());
                }
            }
        }
        setearValoresAlModelo(model, productos);
        return model;
    }

    private List<GraficaStockMinimo> consultarInventarioExistente() {
        List<GraficaStockMinimo> listaClasificatoria = new ArrayList<>();
        List<T_Productos> productosWithStockMinimo = administracionProductos.consultarPorStockMinimo();
        List<T_Bodegas> bodegasDisponibles = administracionBodegas.consultarBodegasXEstado(true);
        for (T_Productos producto : productosWithStockMinimo) {
            BigDecimal cantidadXProducto = BigDecimal.ZERO;
            for (T_Bodegas bodega : bodegasDisponibles) {
                T_Kardex kardexXProducto = administracionKardex.findByProductoXBodega(producto, bodega);
                if (Objects.nonNull(kardexXProducto)) {
                    if (Objects.nonNull(kardexXProducto.getCantidadTotal())) {
                        System.out.println("Cantidad Total " + kardexXProducto.getCantidadTotal() + "Con pro" + producto.getDescripcion() + "Y bodega " + bodega.getNombre());
                        cantidadXProducto = cantidadXProducto.add(kardexXProducto.getCantidadTotal());
                    }
                }
            }
            if (cantidadXProducto.compareTo(producto.getStockMinimo()) <= 0) {
                GraficaStockMinimo objGanador = new GraficaStockMinimo(producto.getDescripcion(), cantidadXProducto.intValue());
                listaClasificatoria.add(objGanador);
            }
        }
        return listaClasificatoria;
    }

    private List<GraficaStockMinimo> obtenerValoresGraficaCostos() {
        List<GraficaStockMinimo> graficaStock = new ArrayList<>();
        // graficaStock = crearListaGraficaCostos();
        graficaStock = crearListaGraficaCostos2();
        Map<String, Integer> sum = (Map<String, Integer>) graficaStock.stream().collect(
                Collectors.groupingBy(GraficaStockMinimo::getProducto, Collectors.summingInt(GraficaStockMinimo::getCantidad)));
        return mapToListGrafica(sum);
    }

    private List<GraficaStockMinimo> obtenerValoresGraficaStock() {
        Map<String, Integer> sum = (Map<String, Integer>) stockMinimo.stream().collect(
                Collectors.groupingBy(GraficaStockMinimo::getProducto, Collectors.summingInt(GraficaStockMinimo::getCantidad)));
        return mapToListGrafica(sum);
    }


    private void setearValoresNuevoStock(GraficaStockMinimo grafica, T_Inventarios inventario) {
        grafica.setProducto(inventario.getProducto().getDescripcion());
        grafica.setCantidad(inventario.getCantidad().intValue());
    }


    private List<GraficaStockMinimo> mapToListGrafica(Map<String, Integer> valoresRealesGrafica) {
        return (List<GraficaStockMinimo>) valoresRealesGrafica.entrySet().stream()
                .map(e -> new GraficaStockMinimo((String) e.getKey(), (e.getValue()))).collect(Collectors.toList());
    }

    private List<ChartSeries> crearListaDeSeries(List<String> listaAComparar) {
        List<ChartSeries> listaVaciaSeries = new ArrayList<>();
        for (String producto : listaAComparar) {
            ChartSeries serie = new ChartSeries(producto);
            listaVaciaSeries.add(serie);
        }
        return listaVaciaSeries;
    }

    private void setearValoresAlModelo(BarChartModel model, List<ChartSeries> productos) {
        for (ChartSeries producto : productos) {
            model.addSeries(producto);
        }
    }


    private List<GraficaStockMinimo> crearListaGraficaCostos2() {
        List<T_reporteKardex> kardexReporteGeneral = new ArrayList<>();;
        List<T_reporteKardexAux> listKardexDistinct = administracionKardex.findDistinct();
        List<T_reporteKardex> report = new ArrayList<>();
        for (T_reporteKardexAux t_Kardex : listKardexDistinct) {
            T_Productos objProducto = new T_Productos();
            objProducto.setIdProducto(t_Kardex.getIdProducto());
            T_Bodegas objBodega = new T_Bodegas();
            objBodega.setIdBodega(t_Kardex.getIdBodega());
            T_reporteKardex ObjReport = new T_reporteKardex();
            T_Kardex objKardex = administracionKardex.findByProductoXBodega(objProducto, objBodega);
            if (Objects.nonNull(objKardex)) {
                ObjReport.setNombreProducto(objKardex.getProducto().getDescripcion());
                ObjReport.setCodigoProducto(objKardex.getProducto().getCodigo());
                ObjReport.setCantidad(objKardex.getCantidadTotal());
                ObjReport.setUbicacion(objKardex.getBodega().getSede().getNombre());
                ObjReport.setValorUnitario(objKardex.getValorUnitario());
                ObjReport.setValorTotal(objKardex.getValorTotal());
                report.add(ObjReport);
            }
        }
        kardexReporteGeneral = report;
        List<GraficaStockMinimo> list = new ArrayList<>();
        for (T_reporteKardex reporte : report) {

            GraficaStockMinimo grafica = new GraficaStockMinimo();
            grafica.setCantidad(reporte.getValorTotal().intValue());
            grafica.setProducto(reporte.getUbicacion());
            list.add(grafica);
        }
        return list;
    }
}
