/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanRegistroMovimientos;

import VO.T_Bodegas;
import VO.T_DetalleKardex;
import VO.T_DetalleMovimiento;
import VO.T_DetalleMovimientoXSerialProductos;
import VO.T_Inventarios;
import VO.T_InventariosSeriales;
import VO.T_Kardex;
import VO.T_Productos;
import com.sippa.inventario.sippa.negocio.Kardex.AdministracionKardexLocal;
import com.sippa.inventario.sippa.negocio.inventarios.AdministracionInventariosLocal;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Miguel-PC
 */
@ManagedBean(name = "beanRegistroDevolucionSalida")
@ViewScoped
public class beanRegistroDevolucionSalida {

    @Getter
    @Setter
    private String numeroRadicado;

    @Getter
    @Setter
    private T_Kardex kardex;

    @Getter
    @Setter
    private T_Inventarios seleccionInventario;

    @Getter
    @Setter
    private List<T_Kardex> inventarioActual;

    @Getter
    @Setter
    private List<T_DetalleKardex> kardexSelection;

    @EJB
    AdministracionInventariosLocal administracionInventario;

    @EJB
    AdministracionKardexLocal administracionKardex;

    @Getter
    @Setter
    private List<T_InventariosSeriales> listInventariosSeriales;

    @Getter
    @Setter
    private List<T_InventariosSeriales> serialesRegistrados;

    @Getter
    @Setter
    private List<T_InventariosSeriales> listaSeleccionSeriales;

    @EJB
    AdministracionInventariosLocal administracionInventarios;

    private List<String> listSerialesEncontradosIguales;

    public beanRegistroDevolucionSalida() {
    }

    @PostConstruct
    public void init() {
        kardex = new T_Kardex();
        inventarioActual = new ArrayList<>();
        serialesRegistrados = new ArrayList<>();
    }

    public void almacenarSeleccionados() {
        for (T_InventariosSeriales serial : listaSeleccionSeriales) {
            serialesRegistrados.add(serial);
        }
        if (!listaSeleccionSeriales.isEmpty()) {
            listaSeleccionSeriales.clear();
        }

        notificarMensajeExitoso("Se almacenaron los seriales del producto " + seleccionInventario.getProducto().getDescripcion() + " exitosamente");
    }

    public void notificarMensajeError(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", mensaje));
    }

    public void notificarMensajeExitoso(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion!", mensaje));
    }

    public BigDecimal obtenerValorUnitarioAux() {
        return kardex.getValor().divide(kardex.getCantidad(), 0, RoundingMode.UP);
    }

    public void consultarBodegaProductosEnInventario() {
        kardex = new T_Kardex();
        kardex = administracionKardex.findById(numeroRadicado);
        if (Objects.nonNull(kardex)) {
            if (kardex.getConcepto().trim().equalsIgnoreCase("Venta")) {
                if (kardex.getIsDevuelto()) {
                    notificarMensajeError("La venta ya fue regresada y procesada");
                    kardex = null;
                } else {
                    notificarMensajeExitoso("Se cargo exitosa la venta");
                }
            } else {
                notificarMensajeError("El radicado ingresado no corresponde a una venta");
                kardex = null;
            }
        } else {
            notificarMensajeError("No se encontro informacion con ese numero de radicado");
        }
    }

    public BigDecimal obtenerValorUnitario(BigDecimal cantidadTotal) {
        return kardex.getValor().divide(kardex.getCantidad(), 0, RoundingMode.UP).multiply(cantidadTotal);

    }

    public void obtenerSerialesProducto(T_Inventarios inventarioSeleccionado) {
        List<T_InventariosSeriales> listInventariosSeriales2 = new ArrayList<>();
        seleccionInventario = inventarioSeleccionado;
        inventarioSeleccionado.getSerialesProductos().stream().forEach(kardex2 -> {
            for (T_DetalleKardex serial : kardex.getDetalleKardexSeriales()) {
                if (serial.getSeriales().equals(kardex2.getSerial())) {
                    listInventariosSeriales2.add(kardex2);
                }
            }
        });
        listInventariosSeriales = listInventariosSeriales2;
    }

    public void eliminarSerial(T_InventariosSeriales serialSeleccionado) {
        serialesRegistrados.remove(serialSeleccionado);
    }
    @Getter
    @Setter
    private T_Kardex kardexUltima;

    public void crearDevolucionDeCompra() {
        T_Kardex kardexNuevo = new T_Kardex();
        kardexUltima = administracionKardex.findByProductoXBodega(kardex.getProducto(), kardex.getBodega());
        kardexNuevo.setBodega(kardex.getBodega());
        kardexNuevo.setCantidad(new BigDecimal(kardexSelection.size()));
        kardexNuevo.setConcepto("Devolucion de venta con radicado " + numeroRadicado);
        kardexNuevo.setProducto(kardex.getProducto());
        kardexNuevo.setFechaMovimiento(new Date());
        kardexNuevo.setValor(obtenerValorUnitario(new BigDecimal(kardexSelection.size())));
        kardexNuevo.setValorUnitario(calcularCostoPromedioPonderado(kardexUltima.getValorTotal(), kardexUltima.getCantidadTotal(), new BigDecimal(kardexSelection.size()), kardexNuevo.getValor()));
        kardexNuevo.setCantidadTotal(kardexUltima.getCantidadTotal().add(new BigDecimal(kardexSelection.size())));
        kardexNuevo.setValorTotal(kardexUltima.getValorTotal().add(kardexNuevo.getValor()));
        kardexNuevo.setDetalleKardexSeriales(homologarSerialesKardex(kardexNuevo));
        try {
            Long id = administracionKardex.crearAndReturnId(kardexNuevo);
            kardex.setIsDevuelto(Boolean.TRUE);

            try {
                administracionKardex.actualizarKardex(kardex);
            } catch (Exception e) {
                System.out.println(" Error  " + e);
            }
            alterarInvetarioAFavor();
            if (Objects.nonNull(id)) {
                notificarMensajeExitoso("el numero de radicado es: " + id + " para el producto " + kardex.getProducto().getDescripcion());
            }
        } catch (Exception e) {
            System.out.println("Ocurrio un error al crear el producto en el kardex");
        }
    }

    private BigDecimal calcularCostoPromedioPonderado(BigDecimal valorTotalActual, BigDecimal cantidadTotalActual, BigDecimal cantidad, BigDecimal valorTotalProducto) {
        BigDecimal arriba = valorTotalActual.add(valorTotalProducto);
        BigDecimal abajo = cantidadTotalActual.add(cantidad);
        return arriba.divide(abajo, 0, RoundingMode.UP);
    }

    private void alterarInvetarioAFavor() {
        T_Inventarios inventario = administracionInventarios.findByProductoXBodega(kardex.getProducto(), kardex.getBodega());
        if (Objects.nonNull(inventario)) {
            actualizarValoresConInventarioExistente(kardex, inventario);
        }
    }

    private void actualizarValoresConInventarioExistente(T_Kardex detalleMovimiento, T_Inventarios inventario) {
        inventario.setValorTotal(inventario.getValorTotal().add(detalleMovimiento.getValor()));
        inventario.setCantidad(inventario.getCantidad().add(detalleMovimiento.getCantidad()));
        inventario.setSerialesProductos(setearDetalleSerialesActualizados(inventario, detalleMovimiento));
        actualizarInventario(inventario);
    }

    private List<T_InventariosSeriales> setearDetalleSerialesActualizados(T_Inventarios inventario, T_Kardex detalleMovimiento) {
        inventario.getSerialesProductos().addAll(homologarSerialMovimientoEntrada(inventario, detalleMovimiento));
        return inventario.getSerialesProductos();
    }

    private List<T_InventariosSeriales> homologarPorCompletoSerialMovimientoEntrada(T_Inventarios nuevoInventario, T_Kardex serial) {
        List<T_InventariosSeriales> serialInventario = new ArrayList<>();
        for (T_DetalleKardex s : serial.getDetalleKardexSeriales()) {
            T_InventariosSeriales serialInventario2 = new T_InventariosSeriales();
            serialInventario2.setIdInventario(nuevoInventario);
            serialInventario2.setSerial(s.getSeriales());
            serialInventario.add(serialInventario2);
        }
        return serialInventario;
    }

    private List<T_InventariosSeriales> homologarSerialMovimientoEntrada(T_Inventarios inventario, T_Kardex serial) {
        List<T_InventariosSeriales> list = new ArrayList<>();
        for (T_DetalleKardex s : serial.getDetalleKardexSeriales()) {
            T_InventariosSeriales serialInventario = new T_InventariosSeriales();
            serialInventario.setIdInventario(inventario);
            serialInventario.setSerial(s.getSeriales());
            serialInventario.setGarantiaMes(s.getGarantiaMes());
            list.add(serialInventario);
        }
        return list;
    }

    private List<T_DetalleKardex> homologarSerialesKardex(T_Kardex kardexNuevo) {
        List<T_DetalleKardex> list = new ArrayList<>();
        for (T_DetalleKardex serial : kardexSelection) {
            T_DetalleKardex detalle = new T_DetalleKardex();
            detalle.setKardex(kardexNuevo);
            detalle.setSeriales(serial.getSeriales());
            detalle.setGarantiaMes(serial.getGarantiaMes());
            list.add(detalle);
        }
        return list;
    }

    private void crearNuevoProducto(T_Inventarios nuevoInventario) {
        try {
            administracionInventarios.crearNuevoInventario(nuevoInventario);
        } catch (Exception e) {
            System.out.println("No se pudo crear un nuevo inventario " + e);
        }
    }

    private void actualizarInventario(T_Inventarios inventario) {
        try {
            administracionInventarios.actualizarInventario(inventario);
        } catch (Exception e) {
            System.out.println("Ocurrio un error actualizar inventario" + e);
        }
    }

    private boolean verificarExistenciaDeSeriales(List<T_DetalleKardex> detalleKardexSeriales) {
        listSerialesEncontradosIguales = new ArrayList<>();
        Boolean estaOno = false;
        T_Inventarios inventario = administracionInventarios.findByProductoXBodega(kardex.getProducto(), kardex.getBodega());
        for (T_DetalleKardex detalleKardexSeriale : detalleKardexSeriales) {
            for (T_InventariosSeriales serial : inventario.getSerialesProductos()) {
                if (serial.getSerial().equalsIgnoreCase(detalleKardexSeriale.getSeriales())) {
                    estaOno = true;
                    listSerialesEncontradosIguales.add(detalleKardexSeriale.getSeriales());
                }
            }
        }
        return estaOno;
    }

}
