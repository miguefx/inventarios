/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanRegistroMovimientos;

import VO.T_Bodegas;
import VO.T_DetalleKardex;
import VO.T_Inventarios;
import VO.T_InventariosSeriales;
import VO.T_Kardex;
import VO.T_Productos;
import com.sippa.inventario.sippa.negocio.InventarioSeriales.AdministracionInventarioSerialesLocal;
import com.sippa.inventario.sippa.negocio.Kardex.AdministracionKardexLocal;
import com.sippa.inventario.sippa.negocio.inventarios.AdministracionInventariosLocal;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Miguel-PC
 */
@ManagedBean(name = "beanRegistroDevolucionEntrada")
@ViewScoped
public class beanRegistroDevolucionEntrada {

    @EJB
    AdministracionInventarioSerialesLocal administracionInventarioSeriales;

    @EJB
    AdministracionInventariosLocal administracionInventario;

    @EJB
    AdministracionKardexLocal administracionKardex;


    @Getter
    @Setter
    private String numeroRadicado;

    @Getter
    @Setter
    private T_Kardex kardex;

    @Getter
    @Setter
    private T_Kardex kardexUltima;

    @Getter
    @Setter
    private T_Inventarios seleccionInventario;

    @Getter
    @Setter
    private List<T_Inventarios> inventarioActual;

    @Getter
    @Setter
    private List<T_InventariosSeriales> listInventariosSeriales;

    @Getter
    @Setter
    private List<T_InventariosSeriales> serialesRegistrados;

    @Getter
    @Setter
    private List<T_InventariosSeriales> listaSeleccionSeriales;

    @PostConstruct
    public void init() {
        kardex = new T_Kardex();
        inventarioActual = new ArrayList<>();
        serialesRegistrados = new ArrayList<>();
    }

    public beanRegistroDevolucionEntrada() {
    }

    public void almacenarSeleccionados() {
        for (T_InventariosSeriales serial : listaSeleccionSeriales) {
            serialesRegistrados.add(serial);
        }
        if (!listaSeleccionSeriales.isEmpty()) {
            listaSeleccionSeriales.clear();
        }
        notificarMensajeExitoso("Se almacenaron los seriales del producto " + seleccionInventario.getProducto().getDescripcion() + " exitosamente");
    }

    public void notificarMensajeError(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", mensaje));
    }

    public void notificarMensajeExitoso(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion!", mensaje));
    }

    public void consultarBodegaProductosEnInventario() {
        kardex = administracionKardex.findById(numeroRadicado);
        if (Objects.nonNull(kardex)) {
            if (kardex.getConcepto().trim().equalsIgnoreCase("Compra")) {
                inventarioActual = administracionInventario.findByBodega(kardex.getBodega());
                inventarioActual = inventarioActual.stream().filter(filtro -> filtro.getProducto().getDescripcion().equals(kardex.getProducto().getDescripcion())).collect(Collectors.toList());
                inventarioActual.stream().findFirst().get().setSerialesProductos(inventarioActual.stream().findFirst().get().getSerialesProductos().stream().filter(filter -> kardex.getDetalleKardexSeriales()
                        .stream().map(mapper -> mapper.getSeriales()).collect(Collectors.toList())
                        .contains(filter.getSerial())).collect(Collectors.toList()));

            } else {
                notificarMensajeError("El radicado ingresado no corresponde a una compra");
                kardex = null;
            }
        } else {
            notificarMensajeError("No se encontro informacion con ese numero de radicado");
        }
    }

    public BigDecimal obtenerValorUnitario(BigDecimal cantidadTotal) {
        return kardex.getValor().divide(kardex.getCantidad(), 0, RoundingMode.UP).multiply(cantidadTotal);
    }

    public BigDecimal obtenerValorUnitarioAux() {
        return kardex.getValor().divide(kardex.getCantidad(), 0, RoundingMode.UP);
    }

    public void obtenerSerialesProducto(T_Inventarios inventarioSeleccionado) {
        List<T_InventariosSeriales> listInventariosSeriales2 = new ArrayList<>();
        List<T_InventariosSeriales> inventarioReal = administracionInventarioSeriales.findByIdInventario(inventarioSeleccionado.getIdInventario());
        seleccionInventario = inventarioSeleccionado;
        for (T_DetalleKardex serial : kardex.getDetalleKardexSeriales()) {
            for (T_InventariosSeriales kardex2 : inventarioReal) {
                if (serial.getSeriales().equals(kardex2.getSerial())) {
                    listInventariosSeriales2.add(kardex2);
                }
            }
            listInventariosSeriales = listInventariosSeriales2;
        }
    }

    public void eliminarSerial(T_InventariosSeriales serialSeleccionado) {
        serialesRegistrados.remove(serialSeleccionado);
    }

    public void crearDevolucionDeCompra() {
        T_Kardex kardexNuevo = new T_Kardex();
        kardexUltima = administracionKardex.findByProductoXBodega(kardex.getProducto(), kardex.getBodega());
        kardexNuevo.setBodega(kardex.getBodega());
        kardexNuevo.setCantidad(new BigDecimal(serialesRegistrados.size()));
        kardexNuevo.setConcepto("Devolucion de Compra con radicado " + numeroRadicado);
        kardexNuevo.setProducto(kardex.getProducto());
        kardexNuevo.setFechaMovimiento(new Date());
        kardexNuevo.setValor(obtenerValorUnitario(new BigDecimal(serialesRegistrados.size())));
        kardexNuevo.setValorUnitario(calcularCostoPromedioPonderado(kardexUltima.getValorTotal(), kardexUltima.getCantidadTotal(), new BigDecimal(serialesRegistrados.size()), kardexNuevo.getValor()));
        kardexNuevo.setCantidadTotal(kardexUltima.getCantidadTotal().subtract(new BigDecimal(serialesRegistrados.size())));
        kardexNuevo.setValorTotal(kardexUltima.getValorTotal().subtract(kardexNuevo.getValor()));
        kardexNuevo.setDetalleKardexSeriales(homologarSerialesKardex(kardexNuevo));
        try {
            Long id = administracionKardex.crearAndReturnId(kardexNuevo);
            alterarEnInventario(kardexNuevo);
            kardex.setIsDevuelto(Boolean.TRUE);
            try {
                administracionKardex.actualizarKardex(kardex);
            } catch (Exception e) {
                System.out.println("Error" + e);
            }
            if (Objects.nonNull(id)) {
                notificarMensajeExitoso("el numero de radicado es: " + id + " para el producto " + kardex.getProducto().getDescripcion());
            }
        } catch (Exception e) {
            System.out.println("Ocurrio un error al crear el producto en el kardex");
        }
    }

    private BigDecimal calcularCostoPromedioPonderado(BigDecimal valorTotalActual, BigDecimal cantidadTotalActual, BigDecimal cantidad, BigDecimal valorTotalProducto) {
        BigDecimal arriba = valorTotalActual.subtract(valorTotalProducto);
        BigDecimal abajo = cantidadTotalActual.subtract(cantidad);
        return arriba.divide(abajo, 0, RoundingMode.UP);
    }

    private List<T_DetalleKardex> homologarSerialesKardex(T_Kardex kardexNuevo) {
        List<T_DetalleKardex> list = new ArrayList<>();
        for (T_InventariosSeriales serial : serialesRegistrados) {
            T_DetalleKardex detalle = new T_DetalleKardex();
            detalle.setKardex(kardexNuevo);
            detalle.setSeriales(serial.getSerial());
            list.add(detalle);
        }
        return list;
    }

    private boolean validarSiSeEliminaRaiz(T_InventariosSeriales serialInventario) {
        return (serialInventario.getIdInventario().getSerialesProductos().size() == 1);
    }

    private void alterarEnInventario(T_Kardex kardexNuevo) {
        for (T_DetalleKardex detalle : kardexNuevo.getDetalleKardexSeriales()) {
            T_Inventarios inventario = administracionInventario.findByProductoXBodega(kardexNuevo.getProducto(), kardexNuevo.getBodega());
            inventario.getSerialesProductos().stream().forEach(serial -> {
                if (serial.getSerial().equalsIgnoreCase(detalle.getSeriales())) {
                    if (validarSiSeEliminaRaiz(serial)) {
                        T_Inventarios inventarioAEliminar = serial.getIdInventario();
                        eliminarDeRaiz(inventarioAEliminar);
                    } else {
                        eliminarSerialProducto(serial);
                    }
                }
            });
        }
    }

    private void eliminarSerialProducto(T_InventariosSeriales serialInventario) {
        try {
            T_Inventarios inventarioActualCalculado = serialInventario.getIdInventario();
            serialInventario.getIdInventario().setCantidad(inventarioActualCalculado.getCantidad().subtract(BigDecimal.ONE));
            administracionInventario.actualizarInventario(inventarioActualCalculado);
            serialInventario.getIdInventario().getCantidad().subtract(BigDecimal.ONE);
            serialInventario.getIdInventario().setCantidad(serialInventario.getIdInventario().getCantidad().subtract(BigDecimal.ONE));
            administracionInventarioSeriales.eliminarEntidad(serialInventario);
        } catch (Exception e) {
            System.out.println("Ocurrio un error al eliminar la entidad");
        }
    }

    private void retirarDelInventario() {
        for (T_InventariosSeriales serialInventario : serialesRegistrados) {
            if (validarSiSeEliminaRaiz(serialInventario)) {
                T_Inventarios inventarioAEliminar = serialInventario.getIdInventario();
                eliminarDeRaiz(inventarioAEliminar);
            } else {
                eliminarSerialProducto(serialInventario);
            }
        }
    }

    private void eliminarDeRaiz(T_Inventarios inventarioAEliminar) {
        try {
            administracionInventario.eliminarEntidad(inventarioAEliminar);
        } catch (Exception e) {
            System.out.println("Ocurrio un error al eliminar la entidad");
        }
    }

}
