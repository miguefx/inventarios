package beanRegistroMovimientos;

import VO.T_Bodegas;
import VO.T_DetalleKardex;
import VO.T_DetalleMovimiento;
import VO.T_DetalleMovimientoXSerialProductos;
import VO.T_DetalleRecetaDeEnsamble;
import VO.T_Inventarios;
import VO.T_InventariosSeriales;
import VO.T_Kardex;
import VO.T_Movimientos;
import VO.T_OrdenDeCompra;
import VO.T_Productos;
import VO.T_Proveedores;
import VO.T_RecetaDeEnsamble;
import com.sippa.inventario.sippa.negocio.Kardex.AdministracionKardexLocal;
import com.sippa.inventario.sippa.negocio.OrdenCompra.AdministacionOrdenCompraLocal;
import com.sippa.inventario.sippa.negocio.Proveedores.AdministracionProveedoresLocal;
import com.sippa.inventario.sippa.negocio.bodegas.AdministracionBodegasLocal;
import com.sippa.inventario.sippa.negocio.inventarios.AdministracionInventariosLocal;
import com.sippa.inventario.sippa.negocio.movimientos.AdministracionMovimientosLocal;
import com.sippa.inventario.sippa.negocio.productos.AdministracionProductosLocal;
import com.sippa.inventarios.sippa.ejb.recetaEnsamble.T_RecetaDeEnsambleFacadeLocal;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.ultima.photoCam.PhotoCamView;

@ManagedBean(name = "registroMovimientosEntrada")
@SessionScoped
public class RegistroMovimientosEntrada extends PhotoCamView implements Serializable {

    @EJB
    AdministacionOrdenCompraLocal administracionOrdenCompra;

    @EJB
    AdministracionKardexLocal administracionKardex;

    @EJB
    AdministracionMovimientosLocal administracionMovimientos;

    @EJB
    AdministracionProductosLocal administracionProductos;

    @EJB
    AdministracionBodegasLocal administracionBodegas;

    @EJB
    AdministracionProveedoresLocal administracionProveedores;

    @EJB
    AdministracionInventariosLocal administracionInventarios;

    @EJB
    T_RecetaDeEnsambleFacadeLocal administracionRecetaEnsamble;

    @Getter
    @Setter
    private List<T_Proveedores> proveedores;

    @Getter
    @Setter
    private List<T_Bodegas> bodegas;

    @Getter
    @Setter
    private Boolean renderBotonAceptar;

    @Getter
    @Setter
    private T_OrdenDeCompra ordenCompra;

    @Getter
    @Setter
    private List<T_Productos> productos;

    @Getter
    @Setter
    private T_Movimientos movimiento;

    @Setter
    @Getter
    private List<T_DetalleMovimiento> listDetalleMovimientos;

    @Setter
    @Getter
    private List<T_RecetaDeEnsamble> listRecetasDeEnsamble;

    @Getter
    @Setter
    private T_DetalleMovimiento detalleMovimientoSeleccionadoAux;

    @Getter
    @Setter
    private T_DetalleMovimiento detalleMovimientoSeleccionado;

    @Getter
    @Setter
    private Long idOrdenCompra;

    @Getter
    @Setter
    private String numeroOrden;

    @Getter
    @Setter
    private Boolean isCargarImagen;

    @Getter
    @Setter
    private Boolean isTomarFoto;

    @Getter
    @Setter
    private BigDecimal totalOrden;

    @Getter
    @Setter
    private List<T_DetalleMovimientoXSerialProductos> detalleSeriales;

    @Getter
    @Setter
    private T_DetalleMovimientoXSerialProductos detalleSeleccionSerialProducto;

    List<T_InventariosSeriales> detalleSerialesActuales;

    private static BigDecimal contador;

    @PostConstruct
    public void init() {
        detalleSeriales = new ArrayList<>();
        movimiento = new T_Movimientos();
        listDetalleMovimientos = new ArrayList<>();
        inicializar();
    }

    protected void inicializar() {
        contador = new BigDecimal(BigInteger.ZERO);
        obtenerValoresProducto();
        obtenerValoresBodegas();
        obtenerValoresProveedores();
        obtenerValoresDeRecetas();
    }

    public void cargarManualMente() {
        isCargarImagen = Boolean.TRUE;
        isTomarFoto = Boolean.FALSE;
        resetFiles();
        inicializarPhotoCam("default");
    }

    public void validarProductoReceta(T_DetalleMovimiento movimientoSeleccionado) {
        if (Objects.nonNull(movimientoSeleccionado.getProducto().getRecetaReferenciada())) {
            List<T_DetalleRecetaDeEnsamble> detalleReceta = movimientoSeleccionado.getProducto().getRecetaReferenciada().getDetalleReceta();
            movimientoSeleccionado.setValorUnitario(((List<BigDecimal>) detalleReceta.stream().map(detalle -> detalle.getValorTotal())
                    .collect(Collectors.toList())).stream().reduce(BigDecimal::add).get());
        }
    }

    public void tomarFoto() {
        isCargarImagen = Boolean.FALSE;
        isTomarFoto = Boolean.TRUE;
        resetFiles();
        inicializarPhotoCam("default");
    }

    private void obtenerValoresProducto() {
        productos = new ArrayList<>();
        productos = administracionProductos.consultarProductosXEstado(Boolean.TRUE);
    }

    private void obtenerValoresBodegas() {
        bodegas = new ArrayList<>();
        bodegas = administracionBodegas.consultarBodegasXEstado(Boolean.TRUE);
    }

    private void obtenerValoresProveedores() {
        proveedores = new ArrayList<>();
        proveedores = administracionProveedores.consultarProveedoresXEstado(Boolean.TRUE);
    }

    public void findByOrdenCompraXNumeroOrden() {
        try {
            ordenCompra = administracionOrdenCompra.findById(idOrdenCompra);
            if (Objects.nonNull(ordenCompra)) {
                homologarOrdenAMovimiento();
                notificarMensajeExitoso("Se cargo correctamente la informacion de la orden de compra creada");
            } else {
                notificarMensajeError("No existe ninguna orden de compra con el numero ingresado");
            }
        } catch (Exception e) {
            System.out.println("No se pudo homologar la orden de compra existente" + e);
        }
    }

    private void homologarOrdenAMovimiento() {
        listDetalleMovimientos = administracionMovimientos.homologarDetalleMovimiento(ordenCompra.getDetalleOrdenDeCompra());
        recalcularValorTotal();
    }

    public void crearRegistroMovimientoEntrada() {
        try {
            setearFotoOImagen();
            setearDatosDetalleActualizado();
            setearDatosDeDetalle();
            alterarCardexAFavor();
            crearObjetoMovimiento();
            alterarInvetarioAFavor();
            resetFiles();
            reset();
            notificarMensajeExitoso("El movimiento de entrada fue creado correctamente");
        } catch (Exception e) {
            notificarMensajeError("Ocurrio un error al crear el movimiento");
        } catch (Error e) {
            notificarMensajeError("Debe cargar alguna imagen de factura");
        }
    }

    private void setearDatosDeDetalle() {
        movimiento.setDetalleMovimiento(listDetalleMovimientos);
    }

    private void setearDatosDetalleActualizado() {
        for (T_DetalleMovimiento detalleMovimiento : listDetalleMovimientos) {
            detalleMovimiento.setIdMovimiento(movimiento);
        }
    }

    private void crearObjetoMovimiento() {
        try {
            administracionMovimientos.crearMovimientoDeEntrada(movimiento);
        } catch (Exception e) {
            System.out.println("Ocurrio un error creando movimiento de entrada " + e);
        }
    }

    public void eliminarProducto(T_DetalleMovimiento detalle) {
        listDetalleMovimientos.remove(detalle);
        actualizarValorTotal();
    }

    public void adicionarProductoDetalle() {
        contador = contador.add(BigDecimal.ONE);
        listDetalleMovimientos.add(new T_DetalleMovimiento(contador, productos.stream().findFirst().get()));
    }

    public void obtenerTotal(T_DetalleMovimiento detalleSeleccionado) {
        if (Objects.nonNull(detalleSeleccionado.getCantidad()) && Objects.nonNull(detalleSeleccionado.getValorUnitario())) {
            detalleSeleccionado.setValorTotal(detalleSeleccionado.getCantidad().multiply(detalleSeleccionado.getValorUnitario()));
            actualizarValorTotal();
        }
    }

    public void actualizarValorTotal() {
        recalcularValorTotal();
    }

    private void recalcularValorTotal() {
        totalOrden = ((List<BigDecimal>) listDetalleMovimientos.stream().map(mapper -> mapper.getValorTotal()).collect(Collectors.toList())).stream().reduce(BigDecimal::add).get();
    }

    public void notificarMensajeError(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", mensaje));
    }

    public void notificarMensajeExitoso(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion!", mensaje));
    }

    private void setearFotoOImagen() throws Error {
        try {
            movimiento.setFotoFactura(isCargarImagen ? establecerImagen() : imagenTomada);
            movimiento.setFechaRegistro(new Date());
        } catch (Exception e) {
            throw new Error("Debe cargar alguna imagen");
        }
    }

    private void reset() {
        movimiento = new T_Movimientos();
        if (!listDetalleMovimientos.isEmpty()) {
            listDetalleMovimientos.clear();
        }
        if (!detalleSeriales.isEmpty()) {
            detalleSeriales.clear();
        }
        detalleMovimientoSeleccionadoAux = new T_DetalleMovimiento();
        detalleSeleccionSerialProducto = new T_DetalleMovimientoXSerialProductos();
        contador = new BigDecimal(BigInteger.ZERO);
        totalOrden = BigDecimal.ZERO;
    }

    private void alterarInvetarioAFavor() {
        for (T_DetalleMovimiento detalleMovimiento : listDetalleMovimientos) {
            T_Inventarios inventario = administracionInventarios.findByProductoXBodega(detalleMovimiento.getProducto(), detalleMovimiento.getIdMovimiento().getBodega());
            if (Objects.nonNull(inventario)) {
                actualizarValoresConInventarioExistente(detalleMovimiento, inventario);
                continue;
            }
            registrarNuevoProducto(detalleMovimiento);
        }
    }

    private void actualizarValoresConInventarioExistente(T_DetalleMovimiento detalleMovimiento, T_Inventarios inventario) {
        inventario.setValorTotal(inventario.getValorTotal().add(detalleMovimiento.getValorTotal()));
        inventario.setCantidad(inventario.getCantidad().add(detalleMovimiento.getCantidad()));
        inventario.setSerialesProductos(setearDetalleSerialesActualizados(inventario, detalleMovimiento));
        actualizarInventario(inventario);
    }

    private void registrarNuevoProducto(T_DetalleMovimiento detalleMovimiento) {
        T_Inventarios nuevoInventario = new T_Inventarios();
        nuevoInventario.setBodega(detalleMovimiento.getIdMovimiento().getBodega());
        nuevoInventario.setCantidad(detalleMovimiento.getCantidad());
        nuevoInventario.setProducto(detalleMovimiento.getProducto());
        nuevoInventario.setValorTotal(detalleMovimiento.getValorTotal());
        nuevoInventario.setSerialesProductos(homologarPorCompletoSerialMovimientoEntrada(nuevoInventario, detalleMovimiento));
        crearNuevoProducto(nuevoInventario);
    }

    private void crearNuevoProducto(T_Inventarios nuevoInventario) {
        try {
            administracionInventarios.crearNuevoInventario(nuevoInventario);
        } catch (Exception e) {
            System.out.println("No se pudo crear un nuevo inventario " + e);
        }
    }

    private void actualizarInventario(T_Inventarios inventario) {
        try {
            administracionInventarios.actualizarInventario(inventario);
        } catch (Exception e) {
            System.out.println("Ocurrio un error actualizar inventario" + e);
        }
    }

    public void añadirCamposParaSeriales(T_DetalleMovimiento detalle) {
        detalleMovimientoSeleccionadoAux = detalle;
        detalleMovimientoSeleccionado = detalle;
        if (!detalleSeriales.isEmpty()) {
            detalleSeriales.clear();
        }
        if (Objects.nonNull(detalle.getDetalleSerialesProductos())) {
            detalleSeriales = detalle.getDetalleSerialesProductos();
        } else {
            while (detalleSeriales.size() < detalle.getCantidad().intValue()) {
                T_DetalleMovimientoXSerialProductos detalleSerial = new T_DetalleMovimientoXSerialProductos(detalle.getProducto());
                detalleSerial.setSerial("Ingrese el serial del producto");
                detalleSerial.setIdDetalleMovimientos(detalle);
                detalleSeriales.add(detalleSerial);
            }
        }
        renderBotonAceptar = Boolean.TRUE;
        T_Inventarios inv = administracionInventarios.findByProductoXBodega(detalle.getProducto(), movimiento.getBodega());
        if (Objects.nonNull(inv)) {
            detalleSerialesActuales = inv.getSerialesProductos();
        } else {
            detalleSerialesActuales = new ArrayList<>();
        }
    }

    public void notificarMensajeCargadoCompleto() {
        renderBotonAceptar = Boolean.TRUE;
        notificarMensajeExitoso("Se ha cargado en su totalidad los seriales del producto " + detalleMovimientoSeleccionadoAux.getProducto().getDescripcion());
    }

    public void validarListaLlena(T_DetalleMovimientoXSerialProductos seleccion) {
        boolean respuesta = false;
        if (this.detalleSeriales.stream().filter(detalle -> detalle.getIdProducto().equals(seleccion.getIdProducto())).allMatch(detalle -> !detalle.getSerial().equals("Ingrese el serial del producto"))) {
            List<T_DetalleMovimientoXSerialProductos> listFiltrado = (List<T_DetalleMovimientoXSerialProductos>) this.detalleSeriales.stream().filter(detalleSerial -> detalleSerial.getIdProducto().equals(this.detalleMovimientoSeleccionado.getProducto())).collect(Collectors.toList());
            for (T_DetalleMovimientoXSerialProductos t : listFiltrado) {
                for (T_InventariosSeriales b : this.detalleSerialesActuales) {
                    if (b.getSerial().equalsIgnoreCase(t.getSerial())) {
                        notificarMensajeError("El serial " + t.getSerial() + " Ya se encuentra en el inventario");
                        respuesta = true;
                    }
                }
            }
            if (!respuesta) {
                this.renderBotonAceptar = Boolean.FALSE;
                this.detalleMovimientoSeleccionado.setDetalleSerialesProductos(listFiltrado);
            }
        }
    }

    private void obtenerValoresDeRecetas() {
        listRecetasDeEnsamble = administracionRecetaEnsamble.findAll();
    }

    private List<T_InventariosSeriales> setearDetalleSerialesActualizados(T_Inventarios inventario, T_DetalleMovimiento detalleMovimiento) {
        detalleMovimiento.getDetalleSerialesProductos().stream().forEach(serial -> inventario.getSerialesProductos().add(homologarSerialMovimientoEntrada(inventario, serial)));
        return inventario.getSerialesProductos();
    }

    private T_InventariosSeriales homologarSerialMovimientoEntrada(T_Inventarios inventario, T_DetalleMovimientoXSerialProductos serial) {
        T_InventariosSeriales serialInventario = new T_InventariosSeriales();
        serialInventario.setIdInventario(inventario);
        serialInventario.setSerial(serial.getSerial());
        serialInventario.setGarantiaMes(serial.getGarantiaMes());
        return serialInventario;
    }

    private List<T_InventariosSeriales> homologarPorCompletoSerialMovimientoEntrada(T_Inventarios nuevoInventario, T_DetalleMovimiento detalleMovimiento) {
        List<T_InventariosSeriales> serialInventario = new ArrayList<>();
        detalleMovimiento.getDetalleSerialesProductos().stream().forEach(serial -> serialInventario.add(homologarSerialMovimientoEntrada(nuevoInventario, serial)));
        return serialInventario;
    }

    private void alterarCardexAFavor() {
        for (T_DetalleMovimiento detalleMovimiento : listDetalleMovimientos) {
            T_Kardex kardex = new T_Kardex();
            kardex = administracionKardex.findByProductoXBodega(detalleMovimiento.getProducto(), detalleMovimiento.getIdMovimiento().getBodega());
            if (Objects.nonNull(kardex)) {
                actualizarValoresConInventarioExistenteKardex(detalleMovimiento, kardex);
            } else {
                registrarNuevoProductoKardex(detalleMovimiento);
            }
        }
    }

    private void actualizarValoresConInventarioExistenteKardex(T_DetalleMovimiento detalleMovimiento, T_Kardex kardex) throws Error {
        T_Kardex kardexNuevo = new T_Kardex();
        kardexNuevo.setBodega(movimiento.getBodega());
        kardexNuevo.setCantidad(detalleMovimiento.getCantidad());
        kardexNuevo.setConcepto("Compra");
        kardexNuevo.setProducto(detalleMovimiento.getProducto());
        kardexNuevo.setFechaMovimiento(new Date());
        kardexNuevo.setValorUnitario(calcularCostoPromedioPonderado(kardex.getValorTotal(), kardex.getCantidadTotal(), detalleMovimiento.getCantidad(), detalleMovimiento.getValorTotal()));
        kardexNuevo.setValor(detalleMovimiento.getValorTotal());
        kardexNuevo.setCantidadTotal(kardex.getCantidadTotal().add(detalleMovimiento.getCantidad()));
        kardexNuevo.setValorTotal(kardex.getValorTotal().add(detalleMovimiento.getValorTotal()));
        kardexNuevo.setDetalleKardexSeriales(setearDetalleSerialesActualizadosKardex(kardexNuevo, detalleMovimiento, kardex));
        try {
            Long id = administracionKardex.crearAndReturnId(kardexNuevo);
            if (Objects.nonNull(id)) {
                notificarMensajeExitoso("el numero de radicado es: " + id + " para el producto " + detalleMovimiento.getProducto().getDescripcion() + " Guardelo para futuras devoluciones ");
            }
        } catch (Exception e) {
            System.out.println("Ocurrio un error al crear el producto en el kardex");
        }
    }

    private void registrarNuevoProductoKardex(T_DetalleMovimiento detalleMovimiento) {
        T_Kardex nuevoKardex = new T_Kardex();
        nuevoKardex.setBodega(movimiento.getBodega());
        nuevoKardex.setCantidad(detalleMovimiento.getCantidad());
        nuevoKardex.setCantidadTotal(detalleMovimiento.getCantidad());
        nuevoKardex.setConcepto("Inventario Inicial");
        nuevoKardex.setFechaMovimiento(new Date());
        nuevoKardex.setProducto(detalleMovimiento.getProducto());
        nuevoKardex.setValorUnitario(detalleMovimiento.getValorUnitario());
        nuevoKardex.setValor(detalleMovimiento.getValorTotal());
        nuevoKardex.setValorTotal(detalleMovimiento.getValorTotal());
        nuevoKardex.setDetalleKardexSeriales(setearHistoricoDetalleSerialesKardex(nuevoKardex, detalleMovimiento));
        try {
            Long id = administracionKardex.crearAndReturnId(nuevoKardex);
            if (Objects.nonNull(id)) {
                notificarMensajeExitoso("el numero de radicado es: " + id + " para el producto " + detalleMovimiento.getProducto().getDescripcion() + "  Guardelo para futuras devoluciones ");
            }
        } catch (Exception e) {
            System.out.println("Ocurrio un error al crear el producto en el kardex");
        }
    }

    private List<T_DetalleKardex> setearDetalleSerialesActualizadosKardex(T_Kardex kardex, T_DetalleMovimiento detalleMovimiento, T_Kardex kardexViejo) {
        List<T_DetalleKardex> list = new ArrayList<>();
        for (T_DetalleMovimientoXSerialProductos b : detalleMovimiento.getDetalleSerialesProductos()) {
            T_DetalleKardex detalle = new T_DetalleKardex();
            detalle.setKardex(kardex);
            detalle.setSeriales(b.getSerial());
            detalle.setGarantiaMes(b.getGarantiaMes());
            list.add(detalle);
        }
        return list;
    }
    private List<String> listSerialesEncontradosIguales;

    private boolean verificarExistenciaDeSeriales(T_DetalleMovimiento detalle, List<T_DetalleKardex> detalleKardexSeriales) {
        listSerialesEncontradosIguales = new ArrayList<>();
        Boolean estaOno = false;
        T_Inventarios inventario = administracionInventarios.findByProductoXBodega(detalle.getProducto(), movimiento.getBodega());
        for (T_DetalleKardex detalleKardexSeriale : detalleKardexSeriales) {
            for (T_InventariosSeriales serial : inventario.getSerialesProductos()) {
                if (serial.getSerial().equalsIgnoreCase(detalleKardexSeriale.getSeriales())) {
                    estaOno = true;
                    listSerialesEncontradosIguales.add(detalleKardexSeriale.getSeriales());
                }
            }
        }
        return estaOno;
    }

    public List<T_DetalleKardex> setearHistoricoDetalleSerialesKardex(T_Kardex kardex, T_DetalleMovimiento detalleMovimiento) {
        List<T_DetalleKardex> list = new ArrayList<>();
        detalleMovimiento.getDetalleSerialesProductos().stream().forEach(detalle -> {
            T_DetalleKardex det = new T_DetalleKardex();
            det.setKardex(kardex);
            det.setSeriales(detalle.getSerial());
            det.setGarantiaMes(detalle.getGarantiaMes());
            list.add(det);
        });

        return list;

    }

    private BigDecimal calcularCostoPromedioPonderado(BigDecimal valorTotalActual, BigDecimal cantidadTotalActual, BigDecimal cantidad, BigDecimal valorTotalProducto) {
        BigDecimal arriba = valorTotalActual.add(valorTotalProducto);
        BigDecimal abajo = cantidadTotalActual.add(cantidad);
        return arriba.divide(abajo, 0, RoundingMode.HALF_UP);
    }


}
