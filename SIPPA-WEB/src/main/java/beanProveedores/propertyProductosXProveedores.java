/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanProveedores;

import VO.T_Sedes;
import com.sippa.inventario.sippa.negocio.sedes.AdministracionSedesLocal;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author matc_
 */
@ManagedBean(name = "propertyClientesXSedes", eager = false)
@ApplicationScoped
public class propertyProductosXProveedores {
    
    @EJB
    AdministracionSedesLocal administracionSedes;

    @Getter
    @Setter
    private List<T_Sedes> listDisponiblesSedes;

    @Getter
    @Setter
    private List<T_Sedes> listUsadosSedes;

    /**
     * Creates a new instance of propertyClientesXSedes
     */
    
    @PostConstruct
    public void init() {
        listDisponiblesSedes = administracionSedes.obtenerSedesXEstado(Boolean.TRUE);
        listUsadosSedes = new ArrayList<>();
    }
    
    public propertyProductosXProveedores() {
    }

}
