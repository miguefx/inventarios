package beanProveedores;

import VO.T_Ciudades;
import VO.T_Productos;
import VO.T_ProductosProveedores;
import VO.T_Proveedores;
import com.sippa.inventario.sippa.negocio.Proveedores.AdministracionProveedoresLocal;
import com.sippa.inventario.sippa.negocio.ciudades.AdministracionCiudadesLocal;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import lombok.Getter;
import lombok.Setter;
import org.omnifaces.util.Messages;
import org.primefaces.model.DualListModel;
import org.primefaces.ultima.service.ProductosService;

/**
 *
 * @author matc_
 */
@ManagedBean(name = "beanProveedores")
@ViewScoped
public class beanProveedores implements Serializable {

    @EJB
    AdministracionProveedoresLocal administracionProveedores;

    @EJB
    AdministracionCiudadesLocal administracionCiudad;

    @Getter
    @Setter
    private String nit;

    @Getter
    @Setter
    private String correo;

    @Getter
    @Setter
    private String nombreContacto;

    @Getter
    @Setter
    private String razonSocial;

    @Getter
    @Setter
    private String telefonoContacto;

    @Getter
    @Setter
    private String direccion;

    @Getter
    @Setter
    private T_Ciudades ciudad;

    @Getter
    @Setter
    private List<T_Ciudades> ciudades;

    @Getter
    @Setter
    private DualListModel<T_Productos> productosPick;

    @ManagedProperty("#{productoService}")
    private ProductosService service;

    public ProductosService getService() {
        return service;
    }

    public void setService(ProductosService service) {
        this.service = service;
    }

    @PostConstruct
    public void init() {
        ciudades = administracionCiudad.consultarCiudades();
        ciudad = ciudades.stream().findFirst().get();
        setearDualList();
    }

    public void guardarProveedor() {
        T_Proveedores proveedores = new T_Proveedores();
        proveedores.setCorreo(correo);
        proveedores.setNombreContacto(nombreContacto);
        proveedores.setCiudad(ciudad);
        proveedores.setRazonSocial(razonSocial);
        proveedores.setNit(nit);
        proveedores.setDireccion(direccion);
        proveedores.setTelefonoContacto(telefonoContacto);
        proveedores.setEstado(Boolean.TRUE);
        setearValoresActualesPickList(proveedores);
        administracionProveedores.crearProveedor(proveedores);
        notificarMensajeExitoso("El proveedor ha sido registrado exitosamente");
        resetCampos();
    }

    private void resetCampos() {
        nombreContacto = "";
        correo = "";
        ciudad = null;
        razonSocial="";
        nit="";
        direccion="";
        telefonoContacto="";
        setearDualList();
    }

    public beanProveedores() {
    }

    public void notificarMensajeError(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", mensaje));
    }

    public void notificarMensajeExitoso(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion!", mensaje));
    }

    public void notificarMensajeErrorGlobal(String mensaje) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error !",
                mensaje);
        Messages.addFlashGlobal(msg);

    }

    private void setearDualList() {
        List<T_Productos> productosSource = service.obtenerProductosActualizados();
        List<T_Productos> productosTarget = new ArrayList<>();
        productosPick = new DualListModel<>(productosSource, productosTarget);
    }

    private void setearValoresActualesPickList(T_Proveedores proveedor) {
        List<T_ProductosProveedores> listProductosXProveedores = new ArrayList<>();
        productosPick.getTarget().stream().forEach(producto -> {
            T_ProductosProveedores nuevoT_Producto = new T_ProductosProveedores();
            nuevoT_Producto.setIdProducto(producto);
            nuevoT_Producto.setIdProveedor(proveedor);
            listProductosXProveedores.add(nuevoT_Producto);
        });
        proveedor.setProductos(listProductosXProveedores);
    }
}
