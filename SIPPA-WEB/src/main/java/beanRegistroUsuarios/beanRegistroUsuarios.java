/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanRegistroUsuarios;

import VO.T_Usuarios;
import com.sippa.inventario.sippa.negocio.usuarios.AdministracionUsuariosLocal;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import lombok.Getter;
import lombok.Setter;
import org.omnifaces.util.Messages;

/**
 *
 * @author matc_
 */
@ManagedBean(name = "beanRegistroUsuarios")
@ViewScoped
public class beanRegistroUsuarios implements Serializable {
    
    @EJB
    private AdministracionUsuariosLocal administracionUsuarios;

    @Getter
    @Setter
    private String documento;

    @Getter
    @Setter
    private String nombres;

    @Getter
    @Setter
    private String apellidos;

    @Getter
    @Setter
    private String usuario;

    @Getter
    @Setter
    private String password;

    @Getter
    @Setter
    private String cargo;

    private static T_Usuarios usuarioEnSession;

    @PostConstruct
    public void init() {
        obtenerVariablesDeSession();

    }

    public beanRegistroUsuarios() {
    }

    private void obtenerVariablesDeSession() {
        usuarioEnSession = (T_Usuarios) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
    }

    public void registrarUsuario() {
        T_Usuarios objVoUsuarios = new T_Usuarios();
        objVoUsuarios.setApellidos(apellidos);
        objVoUsuarios.setNombres(nombres);
        objVoUsuarios.setDocumento(documento);
        objVoUsuarios.setUsuario(usuario);
        objVoUsuarios.setPassword(password);
        objVoUsuarios.setCargo(cargo);
        objVoUsuarios.setUsuarioCreador(usuarioEnSession.getUsuarioCreador());
        administracionUsuarios.crearUsuario(objVoUsuarios);
        notificarMensajeExitoso("Se registro correctamente el usuario");
        limpiarDatos();
    }
     public void notificarMensajeError(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", mensaje));
    }

    public void notificarMensajeExitoso(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion!", mensaje));
    }

    public void notificarMensajeErrorGlobal(String mensaje) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error !",
                mensaje);
        Messages.addFlashGlobal(msg);

    }

    private void limpiarDatos() {
        apellidos="";
        nombres="";
        cargo="";
        usuario="";
        password="";
        documento="";
    }

}
