/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanGestionProveedores;

import VO.T_Proveedores;
import com.sippa.inventario.sippa.negocio.Proveedores.AdministracionProveedoresLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import lombok.Getter;
import lombok.Setter;
import org.omnifaces.util.Messages;

/**
 *
 * @author matc_
 */
@ManagedBean(name = "beanGestionProveedores")
@ViewScoped
public class beanGestionProveedores implements Serializable {

    @EJB
    AdministracionProveedoresLocal administracionProveedores;

    @Getter
    @Setter
    private List<T_Proveedores> listProveedores;

    @Getter
    @Setter
    private List<T_Proveedores> listProveedoresFilter;

    @Getter
    @Setter
    private T_Proveedores selectionProveedor;

    @PostConstruct
    public void init() {
        consultarProveedoresActivos();
    }

    private void consultarProveedoresActivos() {
        listProveedores = administracionProveedores.consultarProveedoresXEstado(Boolean.TRUE);
    }

    public void bloquearProveedor(T_Proveedores proveedor) {
        proveedor.setEstado(Boolean.FALSE);
        administracionProveedores.actualizarProveedores(proveedor);
        consultarProveedoresActivos();
        notificarMensajeExitoso("Se inactivo el proveedor correctamente");
    }

    public beanGestionProveedores() {
    }

    
    public void notificarMensajeError(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", mensaje));
    }

    public void notificarMensajeExitoso(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion!", mensaje));
    }
    
      public void notificarMensajeExitosoGlobal(String mensaje) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion !",
                mensaje);
        Messages.addFlashGlobal(msg);
    }

    public void notificarMensajeErrorGlobal(String mensaje) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error !",
                mensaje);
        Messages.addFlashGlobal(msg);

    }
}
