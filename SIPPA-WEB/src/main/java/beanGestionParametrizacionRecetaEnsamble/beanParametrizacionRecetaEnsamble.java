/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanGestionParametrizacionRecetaEnsamble;

import VO.T_DetalleRecetaDeEnsamble;
import VO.T_Productos;
import VO.T_RecetaDeEnsamble;
import com.sippa.inventario.sippa.negocio.productos.AdministracionProductosLocal;
import com.sippa.inventarios.sippa.ejb.recetaEnsamble.T_RecetaDeEnsambleFacadeLocal;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import lombok.Getter;
import lombok.Setter;
import org.omnifaces.util.Messages;

/**
 *
 * @author matc_
 */
@ManagedBean(name = "beanParametrizacioRecetaEnsamble")
@ViewScoped
public class beanParametrizacionRecetaEnsamble implements Serializable {

    @EJB
    T_RecetaDeEnsambleFacadeLocal administracionRecetaEnsamble;

    @EJB
    AdministracionProductosLocal administracionProductos;
    
    @Getter
    @Setter
    private Long idRecetaEnsamble;

    @Getter
    @Setter
    private BigDecimal totalOrden;

    @Getter
    @Setter
    private List<T_Productos> productos;

    @Getter
    @Setter
    private T_RecetaDeEnsamble recetaEnsambleAModificar;

    private static BigDecimal contador;

    public void cargarInformacionOrdenCompra() {
        if (Objects.nonNull(idRecetaEnsamble)) {
            recetaEnsambleAModificar = administracionRecetaEnsamble.buscarOrdenById(idRecetaEnsamble);
            calcularTotalOrden();
            consultarProductosActuales();
            calcularContador();
        }
    }

    public void actualizarOrdenCompra() {
        try {
            administracionRecetaEnsamble.edit(recetaEnsambleAModificar);
            notificarMensajeExitosoGlobal("Se actualizo correctamente la receta ");
        } catch (Exception e) {
            notificarMensajeErrorGlobal("Error actualizar orden de compra");
            System.out.println("Error actualizar orden compra");
        }
    }

    public void notificarMensajeExitosoGlobal(String mensaje) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion !",
                mensaje);
        Messages.addFlashGlobal(msg);
    }

    public void notificarMensajeErrorGlobal(String mensaje) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error !",
                mensaje);
        Messages.addFlashGlobal(msg);

    }

    public void obtenerTotal(T_DetalleRecetaDeEnsamble detalleSeleccionado) {
        System.out.println("Entro a calcular total");
        if (Objects.nonNull(detalleSeleccionado.getCantidad()) && Objects.nonNull(detalleSeleccionado.getValorUnitario())) {
            detalleSeleccionado.setValorTotal(detalleSeleccionado.getCantidad().multiply(detalleSeleccionado.getValorUnitario()));
            actualizarValorTotal();
        }
    }

    public void actualizarValorTotal() {
        calcularTotalOrden();
    }

    public void eliminarProducto(T_DetalleRecetaDeEnsamble detalle) {
        recetaEnsambleAModificar.getDetalleReceta().remove(detalle);
        actualizarValorTotal();
    }

    private void calcularTotalOrden() {
       totalOrden = ((List<BigDecimal>) recetaEnsambleAModificar.getDetalleReceta().stream().map(mapper -> mapper.getValorTotal()).collect(Collectors.toList())).stream().reduce(BigDecimal::add).get();
    }

    public void adicionarProductoDetalle() {
        recetaEnsambleAModificar.getDetalleReceta().add(new T_DetalleRecetaDeEnsamble(contador.add(BigDecimal.ONE), productos.stream().findFirst().get()));
    }

    private void consultarProductosActuales() {
        productos = administracionProductos.consultarProductosXEstado(Boolean.TRUE);
    }

    private void calcularContador() {
        contador = BigDecimal.valueOf(recetaEnsambleAModificar.getDetalleReceta().stream().mapToInt(mapper -> mapper.getItem().intValue()).max().getAsInt()).movePointLeft(0);
    }

    public void notificarMensajeError(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", mensaje));
    }

    public void notificarMensajeExitoso(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion!", mensaje));
    }

}
