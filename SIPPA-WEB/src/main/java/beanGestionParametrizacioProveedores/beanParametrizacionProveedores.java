/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanGestionParametrizacioProveedores;

import VO.T_Ciudades;
import VO.T_Permisos;
import VO.T_Productos;
import VO.T_ProductosProveedores;
import VO.T_Proveedores;
import com.sippa.inventario.sippa.negocio.Proveedores.AdministracionProveedoresLocal;
import com.sippa.inventario.sippa.negocio.ciudades.AdministracionCiudadesLocal;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import lombok.Getter;
import lombok.Setter;
import org.omnifaces.util.Messages;
import org.primefaces.model.DualListModel;
import org.primefaces.ultima.service.ProductosService;

/**
 *
 * @author matc_
 */
@ManagedBean(name = "beanParametrizacionProveedores")
@ViewScoped
public class beanParametrizacionProveedores implements Serializable {

    @EJB
    AdministracionProveedoresLocal administracionProveedores;

    @EJB
    AdministracionCiudadesLocal administracionCiudad;

    @Getter
    @Setter
    private Long idProveedor;

    @Getter
    @Setter
    private T_Proveedores proveedorAModificar;

    @Getter
    @Setter
    private DualListModel<T_Productos> productosXProveedores;

    @Getter
    @Setter
    private List<String> permisosDisponibles;

    @Getter
    @Setter
    private List<String> permisosUsados;

    @Getter
    @Setter
    private List<T_Ciudades> ciudades;

    @Getter
    @Setter
    private List<T_Permisos> permisosAEliminar;

    @Getter
    @Setter
    private T_ProductosProveedores productoElegido;

    @ManagedProperty("#{productoService}")
    private ProductosService service;

    public ProductosService getService() {
        return service;
    }

    public void setService(ProductosService service) {
        this.service = service;
    }

    @PostConstruct
    public void init() {
        permisosDisponibles = new ArrayList<>();
        permisosUsados = new ArrayList<>();
        ciudades = administracionCiudad.consultarCiudades();
    }

    public void cargarInformacionProveedor() {
        if (Objects.nonNull(idProveedor)) {
            proveedorAModificar = administracionProveedores.buscarProveedorPorId(idProveedor);
            cargarInformacionDelPickList();
        }
    }

    public void actualizarProveedor() {
        try {
            eliminarDatosDetalleProductos();
            setearValoresActualesPickList();
            administracionProveedores.actualizarProveedores(proveedorAModificar);
            notificarMensajeExitosoGlobal("Se actualizo correctamente el proveedor");

        } catch (Exception e) {
            System.out.println("Errro actualizando proveedor" + e);
            notificarMensajeErrorGlobal("Hubo un error al actualizar el proveedor");
        }
    }

    public beanParametrizacionProveedores() {
    }

    public void notificarMensajeExitosoGlobal(String mensaje) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion !",
                mensaje);
        Messages.addFlashGlobal(msg);
    }

    public void notificarMensajeErrorGlobal(String mensaje) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error !",
                mensaje);
        Messages.addFlashGlobal(msg);

    }

    private void determinarPickList() {
        List<T_Productos> productosSource = service.obtenerProductosActualizados();
        List<T_Productos> productosTarget = new ArrayList<>();
        productosXProveedores = new DualListModel<>(productosSource, productosTarget);
    }

    private void cargarInformacionDelPickList() {
        List<T_Productos> productosSource = service.obtenerProductosActualizados();
        List<T_Productos> productosTarget = proveedorAModificar.getProductos().stream().map(mapper -> mapper.getIdProducto()).collect(Collectors.toList());
        productosXProveedores = new DualListModel<>(eliminarInformacionRepetida(productosSource, productosTarget), productosTarget);

    }

    private List<T_Productos> eliminarInformacionRepetida(List<T_Productos> productosSource, List<T_Productos> productosTarget) {
        if (productosSource.size() == productosTarget.size()) {
                return productosSource.stream().allMatch(product -> productosTarget.contains(product)) ? new ArrayList<>() : productosSource.stream().filter(productoSource -> !productosTarget.contains(productoSource)).collect(Collectors.toList());
        }
        return productosSource.stream().filter(productoSource -> !productosTarget.contains(productoSource)).collect(Collectors.toList());
    }

    private void eliminarDatosDetalleProductos() {
        administracionProveedores.eliminarDetalle(proveedorAModificar.getIdProveedor());
    }

    private void setearValoresActualesPickList() {
        List<T_ProductosProveedores> listProductosXProveedores = new ArrayList<>();
        productosXProveedores.getTarget().stream().forEach(producto -> {
            T_ProductosProveedores nuevoT_Producto = new T_ProductosProveedores();
            nuevoT_Producto.setIdProducto(producto);
            nuevoT_Producto.setIdProveedor(proveedorAModificar);
            listProductosXProveedores.add(nuevoT_Producto);
        });
        proveedorAModificar.setProductos(listProductosXProveedores);
    }

}
