/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanGestionParametrizacionBodegas;

import VO.T_Bodegas;
import VO.T_Sedes;
import VO.T_Usuarios;
import com.sippa.inventario.sippa.negocio.bodegas.AdministracionBodegasLocal;
import com.sippa.inventario.sippa.negocio.sedes.AdministracionSedesLocal;
import com.sippa.inventario.sippa.negocio.usuarios.AdministracionUsuariosLocal;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import lombok.Getter;
import lombok.Setter;
import org.omnifaces.util.Messages;

/**
 *
 * @author matc_
 */
@ManagedBean(name = "beanParametrizacionBodegas")
@ViewScoped
public class beanParametrizacionBodegas  implements Serializable{

    @EJB
    AdministracionSedesLocal administracionSede;

    @EJB
    AdministracionUsuariosLocal administracionUsuarios;

    @EJB
    AdministracionBodegasLocal administracionBodegas;

    @Getter
    @Setter
    private Long idBodega;

    @Getter
    @Setter
    private T_Bodegas bodegaAModificar;

    @Getter
    @Setter
    private List<T_Sedes> sedes;

    @Getter
    @Setter
    private List<T_Usuarios> usuarios;

    @PostConstruct
    public void init() {
        obtenerValoresDeSede();
        obtenerValoresUsuariosResponsables();

    }

    private void obtenerValoresDeSede() {
        sedes = administracionSede.obtenerSedesXEstado(Boolean.TRUE);
    }

    private void obtenerValoresUsuariosResponsables() {
        usuarios = administracionUsuarios.buscarUsuariosXEstado(Boolean.TRUE);
    }

    public void actualizarBodega() {
            administracionBodegas.actualizarBodegas(bodegaAModificar);
            notificarMensajeExitosoGlobal("Se actualizo correctamente la bodega");
    }

    public void cargarInformacionBodega() {
        if (Objects.nonNull(idBodega)) {
            bodegaAModificar = administracionBodegas.consultarBodegaById(idBodega);
        }
    }

    public void actualizarProveedor() {
        try {
            notificarMensajeExitosoGlobal("Se actualizo correctamente el proveedor");

        } catch (Exception e) {
            System.out.println("Errro actualizando proveedor" + e);
            notificarMensajeErrorGlobal("Hubo un error al actualizar el proveedor");
        }
    }

    public beanParametrizacionBodegas() {
    }

    public void notificarMensajeExitosoGlobal(String mensaje) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion !",
                mensaje);
        Messages.addFlashGlobal(msg);
    }

    public void notificarMensajeErrorGlobal(String mensaje) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error !",
                mensaje);
        Messages.addFlashGlobal(msg);

    }

}
