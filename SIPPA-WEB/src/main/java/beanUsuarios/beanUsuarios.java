/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanUsuarios;

import VO.T_Permisos;
import VO.T_Usuarios;
import com.sippa.inventario.sippa.negocio.permisos.AdministracionPermisosLocal;
import com.sippa.inventario.sippa.negocio.usuarios.AdministracionUsuariosLocal;
import controller.sesion;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import lombok.Getter;
import lombok.Setter;
import org.omnifaces.util.Messages;

@ManagedBean(name = "beanUsuarios")
@SessionScoped
public class beanUsuarios implements Serializable {

    @EJB
    AdministracionUsuariosLocal administracionUsuarios;

    @EJB
    AdministracionPermisosLocal administracionPermisos;

    @Getter
    @Setter
    private String user;

    @Getter
    @Setter
    private String nuevoUser;

    @Getter
    @Setter
    private String nuevaPassword;

    @Getter
    @Setter
    private String nuevoPassword;

    @Getter
    @Setter
    private String password;

    @Getter
    @Setter
    private String nombreCompletoAMostrar;

    @Getter
    @Setter
    private Boolean renderRegistroUsuario;
    @Getter
    @Setter
    private Boolean renderAdmUsuario;
    @Getter
    @Setter
    private Boolean renderRegistroProveedores;
    @Getter
    @Setter
    private Boolean renderAdmProveedores;
    @Getter
    @Setter
    private Boolean renderRegistroBodegas;
    @Getter
    @Setter
    private Boolean renderAdmBodegas;
    @Getter
    @Setter
    private Boolean renderRegistroProductos;
    @Getter
    @Setter
    private Boolean renderAdmProductos;
    @Getter
    @Setter
    private Boolean renderRegistroClientes;
    @Getter
    @Setter
    private Boolean renderAdmClientes;
    @Getter
    @Setter
    private Boolean renderRegistroSedes;
    @Getter
    @Setter
    private Boolean renderAdmSedes;
    @Getter
    @Setter
    private Boolean renderRegistroEntradas;
    @Getter
    @Setter
    private Boolean renderRegistroSalidas;
    @Getter
    @Setter
    private Boolean renderRegistroDevEntrada;
    @Getter
    @Setter
    private Boolean renderRegistroDevSalida;
    @Getter
    @Setter
    private Boolean renderRegistroOrdenCompra;
    @Getter
    @Setter
    private Boolean renderAdmOrdenCompra;
    @Getter
    @Setter
    private Boolean renderRegistroRecetaEnsamble;
    @Getter
    @Setter
    private Boolean renderReporteKardex;
    @Getter
    @Setter
    private Boolean renderAdmRecetaEnsamble;
    @Getter
    @Setter
    private Boolean renderAdmKardex;

    private T_Usuarios usuario;

    public void login() throws IOException {
        establecerValoresInicialesRender();
        usuario = administracionUsuarios.login(user, password);
        if (Objects.nonNull(usuario.getDocumento())) {
            crearValoresDeSession();
            establecerPermisosUser();
            FacesContext.getCurrentInstance().getExternalContext().redirect("/Sippa-Inventarios/Principal.xhtml");
        } else {
            notificarMensajeError("Usuario y/o contraseña incorrectos");
        }
    }

    public void session() {
        try {
            usuario = (T_Usuarios) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
            if (Objects.isNull(usuario)) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("/Sippa-Inventarios/Acceso.xhtml");
                notificarMensajeErrorGlobal("Debe iniciar Session");
            }
        } catch (Exception e) {
        }
    }

    public void eliminarSesion() {
        try {
            user = "";
            password = "";
            HttpSession hs = sesion.getSession();
            hs.invalidate();

        } catch (Exception e) {
        }
    }

    public String obtenerNombreCompleto() {
        return Objects.nonNull(usuario) ? usuario.getNombres().concat(" ")
                .concat(usuario.getApellidos())
                : obtenerVariablesDeSession();
    }

    private String obtenerVariablesDeSession() {
        usuario = (T_Usuarios) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
        return usuario.getNombres().concat(" ").concat(usuario.getApellidos());
    }

    private void crearValoresDeSession() {
        HttpSession hs = sesion.getSession();
        hs.setAttribute("usuario", usuario);
    }

    public beanUsuarios() {
    }

    public void notificarMensajeError(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", mensaje));
    }

    public void notificarMensajeExitoso(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion!", mensaje));
    }

    public void notificarMensajeErrorGlobal(String mensaje) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error !",
                mensaje);
        Messages.addFlashGlobal(msg);

    }

    private void establecerValoresInicialesRender() {
        renderRegistroUsuario = false;
        renderAdmUsuario = false;
        renderRegistroProveedores = false;
        renderReporteKardex = false;
        renderAdmProveedores = false;
        renderRegistroBodegas = false;
        renderAdmBodegas = false;
        renderAdmProductos = false;
        renderRegistroProductos = false;
        renderRegistroClientes = false;
        renderAdmClientes = false;
        renderRegistroSedes = false;
        renderAdmSedes = false;
        renderRegistroEntradas = false;
        renderRegistroSalidas = false;
        renderRegistroDevEntrada = false;
        renderRegistroDevSalida = false;
        renderRegistroOrdenCompra = false;
        renderAdmOrdenCompra = false;
        renderRegistroRecetaEnsamble = false;
        renderAdmRecetaEnsamble = false;
        renderAdmKardex = false;
    }

    private void establecerPermisosUser() {
        List<T_Permisos> permisos = administracionPermisos.buscarPermisosByIdUsuario(usuario.getIdUsuario());
        for (T_Permisos permiso : permisos) {
            switch (permiso.getModulo()) {
                case "Registro de Usuarios":
                    renderRegistroUsuario = true;
                    break;
                case "Administracion de usuarios":
                    renderAdmUsuario = true;
                    break;
                case "Registro de proveedores":
                    renderRegistroProveedores = true;
                    break;
                case "Administracion de proveedores":
                    renderAdmProveedores = true;
                    break;
                case "Registro de bodegas":
                    renderRegistroBodegas = true;
                    break;
                case "Administracion de bodegas":
                    renderAdmBodegas = true;
                    break;
                case "Registro de productos":
                    renderRegistroProductos = true;
                    break;
                case "Administracion de productos":
                    renderAdmProductos = true;
                    break;
                case "Registro de clientes":
                    renderRegistroClientes = true;
                    break;
                case "Administracion de clientes":
                    renderAdmClientes = true;
                    break;
                case "Registro de sedes":
                    renderRegistroSedes = true;
                    break;
                case "Administracion de sedes":
                    renderAdmSedes = true;
                    break;
                case "Registro de entradas":
                    renderRegistroEntradas = true;
                    break;
                case "Registro de salida":
                    renderRegistroSalidas = true;
                    break;
                case "Registro devolucion entrada":
                    renderRegistroDevEntrada = true;
                    break;
                case "Registro devolucion salida":
                    renderRegistroDevSalida = true;
                    break;
                case "Registro de orden compra":
                    renderRegistroOrdenCompra = true;
                    break;
                case "Administracion orden de compra":
                    renderAdmOrdenCompra = true;
                    break;
                case "Registro receta ensamble":
                    renderRegistroRecetaEnsamble = true;
                    break;
                case "Administracion receta ensamble":
                    renderAdmRecetaEnsamble = true;
                    break;
                case "Administracion de kardex":
                    renderAdmKardex = true;
                    break;
                case "Reporte Kardex General":
                    renderReporteKardex = true;
                    break;
            }
        }
    }
}
