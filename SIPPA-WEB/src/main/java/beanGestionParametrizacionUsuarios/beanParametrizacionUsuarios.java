/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanGestionParametrizacionUsuarios;

import VO.T_Permisos;
import VO.T_Usuarios;
import com.sippa.inventario.sippa.negocio.permisos.AdministracionPermisosLocal;
import com.sippa.inventario.sippa.negocio.usuarios.AdministracionUsuariosLocal;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import lombok.Getter;
import lombok.Setter;
import org.omnifaces.util.Messages;
import org.primefaces.model.DualListModel;

/**
 *
 * @author matc_
 */
@ManagedBean(name = "beanParametrizacionUsuarios")
@ViewScoped
public class beanParametrizacionUsuarios implements Serializable {

    @EJB
    AdministracionUsuariosLocal administracionUsuarios;

    @EJB
    AdministracionPermisosLocal administracionPermisos;

    @Getter
    @Setter
    private Long idUsuario;

    @Getter
    @Setter
    private T_Usuarios usuarioAModificar;

    @Getter
    @Setter
    private DualListModel<String> permisosUsuario;

    @Getter
    @Setter
    private List<String> permisosDisponibles;

    @Getter
    @Setter
    private List<String> permisosUsados;

    @Getter
    @Setter
    private List<T_Permisos> permisosAEliminar;

    @PostConstruct
    public void init() {
        permisosDisponibles = new ArrayList<>();
        permisosUsados = new ArrayList<>();
    }

    public void cargarInformcionDeUsuario() {
        if (Objects.nonNull(idUsuario)) {
            usuarioAModificar = administracionUsuarios.buscarUsuarioPorId(idUsuario);
            permisosDisponibles = Arrays.asList("Registro de Usuarios", "Administracion de usuarios", 
                    "Registro de proveedores","Administracion de proveedores","Registro de bodegas",
                    "Administracion de bodegas","Registro de productos","Administracion de productos",
                    "Registro de clientes","Administracion de clientes","Registro de sedes","Administracion de sedes",
                    "Registro de entradas","Registro de salida","Registro devolucion entrada","Registro devolucion salida",
                    "Registro de orden compra","Administracion orden de compra","Registro receta ensamble"
                    ,"Administracion receta ensamble","Administracion de kardex","Reporte Kardex General");
            permisosAEliminar = obtenerPermisosConcedidosPorIdUsuario();
            if (!permisosAEliminar.isEmpty()) {
                permisosUsados = permisosAEliminar.stream().map(mapper -> mapper.getModulo()).collect(Collectors.toList());
            }
            permisosDisponibles = eliminarPermisosUsados();
            setearPickList();
        }
    }

    public beanParametrizacionUsuarios() {
    }

    private List<T_Permisos> obtenerPermisosConcedidosPorIdUsuario() {
        return administracionPermisos.buscarPermisosByIdUsuario(idUsuario);
    }

    private List<String> eliminarPermisosUsados() {
        System.out.println("Actualizo");
        return permisosDisponibles.stream().filter(permi -> !permisosUsados.contains(permi)).collect(Collectors.toList());
    }

    private void setearPickList() {
        permisosUsuario = new DualListModel<>();
        permisosUsuario.setSource(permisosDisponibles);
        permisosUsuario.setTarget(permisosUsados);

    }

    public void guardarUsuario() {
        System.out.println("Entro a actualizar");
        administracionUsuarios.actualizarUsuario(usuarioAModificar);
        eliminarPermisosUsadosPrimero();
        if (!permisosUsados.isEmpty()) {
            permisosUsados.clear();
        }
        permisosUsuario.getTarget().stream().forEach(permi -> {
            T_Permisos objVoPermiso = new T_Permisos();
            objVoPermiso.setIdUsuario(idUsuario);
            objVoPermiso.setModulo(permi);
            administracionPermisos.crearPermisoUsuario(objVoPermiso);

        });
        notificarMensajeExitosoGlobal("El usuario se actualizo correctamente");
    }

    private void eliminarPermisosUsadosPrimero() {
        permisosAEliminar.stream().forEach(permiso -> {
            administracionPermisos.eliminarPermiso(permiso);
        });
    }

    public void notificarMensajeExitosoGlobal(String mensaje) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion !",
                mensaje);
        Messages.addFlashGlobal(msg);
    }

    public void notificarMensajeErrorGlobal(String mensaje) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error !",
                mensaje);
        Messages.addFlashGlobal(msg);

    }
}
