/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanRegistroClientes;

import VO.T_Clientes;
import VO.T_ClientesXSedes;
import VO.T_Sedes;
import com.sippa.inventario.sippa.negocio.Clientes.AdministracionClientesLocal;
import com.sippa.inventario.sippa.negocio.sedes.AdministracionSedesLocal;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import lombok.Getter;
import lombok.Setter;
import org.omnifaces.util.Messages;
import org.primefaces.model.DualListModel;
import org.primefaces.ultima.service.SedesService;

/**
 *
 * @author matc_
 */
@ManagedBean(name = "beanRegistroClientes")
public class beanRegistroClientes {

    @EJB
    AdministracionClientesLocal administracionClientes;

    @EJB
    AdministracionSedesLocal administracionSedes;

    @Getter
    @Setter
    private String razonSocial;

    @Getter
    @Setter
    private String nitCedula;

    @Getter
    @Setter
    private String direccion;

    @Getter
    @Setter
    private String nombreContacto;

    @Getter
    @Setter
    private String telefono;

    @Getter
    @Setter
    private String email;

    @Getter
    @Setter
    private String cargo;

    @Getter
    @Setter
    private DualListModel<T_Sedes> sedesPick;

    @ManagedProperty("#{sedeService}")
    private SedesService service;

    @PostConstruct
    public void init() {
        List<T_Sedes> sedesSource = service.consultarSedesActuales();
        System.out.println("Obtenidas"+sedesSource.size());
        List<T_Sedes> sedesTarget = new ArrayList<T_Sedes>();
        sedesPick = new DualListModel<>(sedesSource, sedesTarget);
    }

    public beanRegistroClientes() {
    }

    public SedesService getService() {
        return service;
    }

    public void setService(SedesService service) {
        this.service = service;
    }

    public void registrarCliente() {
         T_Clientes objvoCliente = new T_Clientes();
        setearDatosCliente(objvoCliente);
        setearDatosDetalleSedes(objvoCliente);
        crearObjetoCliente(objvoCliente);
        notificarMensajeExitoso("Se realizo correctamente la adicion del cliente");
    }

    private void setearDatosCliente(T_Clientes objvoCliente) {
        objvoCliente.setCargo(cargo);
        objvoCliente.setDireccion(direccion);
        objvoCliente.setEmail(email);
        objvoCliente.setNitCedula(nitCedula);
        objvoCliente.setNombreContacto(nombreContacto);
        objvoCliente.setRazonSocial(razonSocial);
        objvoCliente.setTelefono(telefono);

    }

    private void setearDatosDetalleSedes(T_Clientes objvoCliente) {
        List<T_ClientesXSedes> listClientesXSedes = new ArrayList<>();
        System.out.println("Size" + sedesPick.getTarget().size());
        sedesPick.getTarget().stream().forEach(sedeUsada -> {
            T_ClientesXSedes objvoClientesXSedes = new T_ClientesXSedes();
            objvoClientesXSedes.setIdCliente(objvoCliente);
            objvoClientesXSedes.setIdSede(sedeUsada);
            listClientesXSedes.add(objvoClientesXSedes);
        });
        objvoCliente.setClientesXSedes(listClientesXSedes);
    }

    private void crearObjetoCliente(T_Clientes objvoCliente) {
        try {
            administracionClientes.crearCliente(objvoCliente);
        } catch (Exception e) {
            notificarMensajeError("Ocurrio un error al registrar el cliente ");
            System.out.println("Ocurrio un error registrando cliente" + e);
        }
    }

    public void notificarMensajeError(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", mensaje));
    }

    public void notificarMensajeExitoso(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion!", mensaje));
    }

    public void notificarMensajeErrorGlobal(String mensaje) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error !",
                mensaje);
        Messages.addFlashGlobal(msg);

    }

}
