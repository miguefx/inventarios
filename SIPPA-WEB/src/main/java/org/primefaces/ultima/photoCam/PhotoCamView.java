package org.primefaces.ultima.photoCam;

import java.io.ByteArrayInputStream;
import org.omnifaces.util.Messages;
import java.io.InputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.event.CaptureEvent;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;
import org.primefaces.event.FileUploadEvent;
import javax.faces.context.FacesContext;
import javax.faces.application.FacesMessage;
import org.primefaces.model.UploadedFile;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ManagedBean;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

@ManagedBean(name = "photoCamView")
@SessionScoped
public class PhotoCamView {

    protected static String modulo;
    protected static byte[] imagenTomada;
    private String filename;
    public static UploadedFile file;
    protected StreamedContent imagenPreCargada;

    public StreamedContent getImagenPreCargada() {
        return imagenPreCargada;
    }

    public void setImagenPreCargada(StreamedContent imagenPreCargada) {
        this.imagenPreCargada = imagenPreCargada;
    }

    public byte[] getImagenTomada() {
        return imagenTomada;
    }

    public void setImagenTomada(final byte[] imagenTomada) {
        this.imagenTomada = imagenTomada;
    }

    public String getmodulo() {
        return modulo;
    }

    public void setModulo(final String modulo) {
        this.modulo = modulo;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(final UploadedFile file) {
        this.file = file;
    }

    public void upload() {
        if (file != null) {
            final FacesMessage message = new FacesMessage("Succesful", file.getFileName() + " is uploaded.");
            FacesContext.getCurrentInstance().addMessage((String) null, message);
        }
    }

    public void handleFileUpload(final FileUploadEvent event) {
        file = event.getFile();
        imagenPreCargada = new DefaultStreamedContent(new ByteArrayInputStream(file.getContents()));
    }

    private String getRandomImageName() {
        final int i = (int) (Math.random() * 1.0E7);
        return String.valueOf(i);
    }

    public String getFilename() {
        return this.filename;
    }

    public void visualizarImagenFoto() {
        try {
            if (Objects.nonNull(imagenTomada)) {
                final byte[] arreglo = imagenTomada;
                final HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
                response.getOutputStream().write(arreglo);
                response.getOutputStream().close();
                FacesContext.getCurrentInstance().responseComplete();
            }
            this.notificarMensajeErrorGlobal("No tiene una imagen precargada");
        } catch (Exception ex) {
        }
    }

    public void visualizarImagenCargada() {
        try {
            if (Objects.nonNull(file)) {
                System.out.println("file" + file);
                final byte[] arreglo = file.getContents();
                System.out.println("arreglo" + arreglo);
                final HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
                response.getOutputStream().write(arreglo);
                response.getOutputStream().close();
                FacesContext.getCurrentInstance().responseComplete();
            }
            this.notificarMensajeErrorGlobal("No tiene una imagen precargada");
        } catch (Exception ex) {
        }
    }

    public void oncapture(CaptureEvent captureEvent) {
        imagenPreCargada = new DefaultStreamedContent(new ByteArrayInputStream(captureEvent.getData()));
        final byte[] data = imagenTomada = captureEvent.getData();
    }

    public String establecerNavegacion() {
        final String modulo = this.modulo;
        switch (modulo) {
            case "registroProductos": {
                this.notificarMensajeExitosoGlobal("Se cargo exitosamente la imagen, continua con el proceso y finaliza en el boton de registrar");
                return "RegistroProductos";
            }
            case "gestionProductos": {
                
                this.notificarMensajeExitosoGlobal("Se cargo exitosamente la imagen, continua con el proceso y finaliza en el boton de actualizar");
                return "GestionParametrizacionProductos";
            }
            default: {
                this.notificarMensajeExitosoGlobal("Se cargo exitosamente la imagen, continua con el proceso y finaliza en el boton de registrar");
                return "RegistroMovimientosEntrada";
            }
        }
    }

    public byte[] establecerImagen() {
        InputStream stream = null;
        try {
            stream = file.getInputstream();
        } catch (IOException ex) {
            System.out.println("No se logro establcer la img");
        }
        final ByteArrayOutputStream output = new ByteArrayOutputStream();
        final byte[] buffer = new byte[1024];
        byte[] foto = new byte[1024];
        try {
            int length = 0;
            while ((length = stream.read(buffer)) > 0) {
                output.write(buffer, 0, length);
                foto = output.toByteArray();
            }
        } catch (IOException ex2) {
        }
        return foto;
    }

    public void resetFiles() {
        file = null;
        this.filename = null;
        imagenTomada = null;
    }

    public void inicializarPhotoCam(final String moduloLlegada) {
        file = null;
        imagenTomada = null;
        modulo = moduloLlegada;
    }

    public void notificarMensajeExitosoGlobal(final String mensaje) {
        final FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion !", mensaje);
        Messages.addFlashGlobal(msg);
    }

    public void notificarMensajeErrorGlobal(final String mensaje) {
        final FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error !", mensaje);
        Messages.addFlashGlobal(msg);
    }
}
