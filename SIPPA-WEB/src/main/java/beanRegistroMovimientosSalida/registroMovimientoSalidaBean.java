package beanRegistroMovimientosSalida;

import VO.EstadosMovimientoSalida;
import VO.GraficaStockMinimo;
import VO.KardexDTO;
import VO.SerialesGarantiaArray;
import VO.SerialesKardex;
import VO.T_Bodegas;
import VO.T_DetalleKardex;
import VO.T_Inventarios;
import VO.T_InventariosSeriales;
import VO.T_Kardex;
import VO.T_MovimientoSalidas;
import com.sippa.inventario.sippa.negocio.InventarioSeriales.AdministracionInventarioSerialesLocal;
import com.sippa.inventario.sippa.negocio.Kardex.AdministracionKardexLocal;
import com.sippa.inventario.sippa.negocio.bodegas.AdministracionBodegasLocal;
import com.sippa.inventario.sippa.negocio.inventarios.AdministracionInventariosLocal;
import com.sippa.inventario.sippa.negocio.movimientoSalida.AdministracionMovimientoSalidaLocal;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import lombok.Getter;
import lombok.Setter;

@ManagedBean(name = "registroMovimientoSalidaBean")
@ViewScoped
public class registroMovimientoSalidaBean {

    @EJB
    AdministracionMovimientoSalidaLocal administracionMovimientoSalidas;

    @EJB
    AdministracionInventariosLocal administracionInventario;

    @EJB
    AdministracionKardexLocal administracionKardex;

    @EJB
    AdministracionInventarioSerialesLocal administracionInventarioSeriales;

    @EJB
    AdministracionBodegasLocal administracionBodegas;

    @Getter
    @Setter
    private List<T_Inventarios> inventarioActual;

    @Getter
    @Setter
    private T_Inventarios seleccionInventario;

    @Getter
    @Setter
    private List<String> serialesKardexIngresados;

    @Getter
    @Setter
    private List<T_InventariosSeriales> listInventariosSeriales;

    @Getter
    @Setter
    private List<T_InventariosSeriales> listaSeleccionSeriales;

    @Getter
    @Setter
    private T_MovimientoSalidas movimientoSalida;

    @Getter
    @Setter
    private List<T_Bodegas> bodegas;

    @Getter
    @Setter
    private Boolean renderTableInventario;

    @Getter
    @Setter
    private List<KardexDTO> listKardex;

    @Getter
    @Setter
    private List<EstadosMovimientoSalida> estadosSalidas;

    @Getter
    @Setter
    private List<T_InventariosSeriales> serialesRegistrados;

    List<SerialesKardex> auxKardex;

    @PostConstruct
    public void init() {
        inicializarVariables();
        obtenerValoresEstadosSalidas();
        obtenerValoresBodegas();
    }

    private void obtenerValoresEstadosSalidas() {
        estadosSalidas = Arrays.asList(EstadosMovimientoSalida.values());
    }

    private void obtenerValoresBodegas() {
        bodegas = new ArrayList<>();
        bodegas = administracionBodegas.consultarBodegasXEstado(Boolean.TRUE);
    }

    private void inicializarVariables() {
        serialesRegistrados = new ArrayList<>();
        movimientoSalida = new T_MovimientoSalidas();
        renderTableInventario = Boolean.FALSE;

    }

    public void consultarBodegaProductosEnInventario() {
        inventarioActual = administracionInventario.findByBodega(movimientoSalida.getBodega());
        if (inventarioActual.size() <= 0) {
            notificarMensajeError("No se encontraron productos para la bodega ingresada");
        } else {
            renderTableInventario = Boolean.TRUE;
        }
    }

    public void eliminarSerialKardex(String serial) {

    }

    public void almacenarSeleccionados() {
        for (T_InventariosSeriales serial : listaSeleccionSeriales) {
            serialesRegistrados.add(serial);
        }
        if (!listaSeleccionSeriales.isEmpty()) {
            listaSeleccionSeriales.clear();
        }
        notificarMensajeExitoso("Se almacenaron los seriales del producto " + seleccionInventario.getProducto().getDescripcion() + " exitosamente");
    }

    public BigDecimal calcularPrecioUnitario(T_InventariosSeriales serialSeleccionado) {
        T_Kardex kardexExistente = administracionKardex.findByProductoXBodega(serialSeleccionado.getIdInventario().getProducto(), serialSeleccionado.getIdInventario().getBodega());
        return kardexExistente.getValorUnitario();
    }

    public void eliminarSerial(T_InventariosSeriales serialSeleccionado) {
        serialesRegistrados.remove(serialSeleccionado);
    }

    public void notificarMensajeError(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", mensaje));
    }

    public void notificarMensajeExitoso(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion!", mensaje));
    }

    public void obtenerSerialesProducto(T_Inventarios inventarioSeleccionado) {
        seleccionInventario = inventarioSeleccionado;
        listInventariosSeriales = administracionInventarioSeriales.findByIdInventario(inventarioSeleccionado.getIdInventario());
    }

    public void registrarSalida() {
        try {
            auxKardex = new ArrayList<>();
            if (serialesRegistrados.isEmpty()) {
                notificarMensajeError("Debe seleccionar por lo menos 1 producto para la salida");
            } else if (movimientoSalida.getFirma().isEmpty()) {
                notificarMensajeError("Debe firmar antes de registrar la salida ");
            } else {
                for (T_InventariosSeriales serialInventario : serialesRegistrados) {
                    if (validarSiSeEliminaRaiz(serialInventario)) {
                        T_Inventarios inventarioAEliminar = serialInventario.getIdInventario();
                        eliminarDeRaiz(inventarioAEliminar);
                    } else {
                        eliminarSerialProducto(serialInventario);
                    }
                    alterarMovimientoSalida(serialInventario);
                    agrupingproductoKardex(serialInventario);
                }
                realizarSalidaDeCardex();
                notificarMensajeExitoso("La salida fue registrada exitosamente");
                resetFiles();
            }
        } catch (Exception e) {
            notificarMensajeError("Ocurrio un error al registrar la salida" + e.getMessage());
            resetFiles();
        }
    }

    private boolean validarSiSeEliminaRaiz(T_InventariosSeriales serialInventario) {
        return (serialInventario.getIdInventario().getSerialesProductos().size() == 1);
    }

    private void eliminarDeRaiz(T_Inventarios inventarioAEliminar) {
        try {
            administracionInventario.eliminarEntidad(inventarioAEliminar);
        } catch (Exception e) {
            System.out.println("Ocurrio un error al eliminar la entidad");
        }
    }

    private void eliminarSerialProducto(T_InventariosSeriales serialInventario) {
        try {
            T_Inventarios inventarioActualCalculado = serialInventario.getIdInventario();
            serialInventario.getIdInventario().setCantidad(inventarioActualCalculado.getCantidad().subtract(BigDecimal.ONE));
            administracionInventario.actualizarInventario(inventarioActualCalculado);
            serialInventario.getIdInventario().getCantidad().subtract(BigDecimal.ONE);
            serialInventario.getIdInventario().setCantidad(serialInventario.getIdInventario().getCantidad().subtract(BigDecimal.ONE));
            administracionInventarioSeriales.eliminarEntidad(serialInventario);
        } catch (Exception e) {
            System.out.println("Ocurrio un error al eliminar la entidad");
        }
    }

    private void alterarMovimientoSalida(T_InventariosSeriales idInventario) {
        T_MovimientoSalidas salida = homologarInventario(idInventario);
        persistirSalida(salida);
    }

    private T_MovimientoSalidas homologarInventario(T_InventariosSeriales idInventario) {
        T_MovimientoSalidas salidaNueva = new T_MovimientoSalidas();
        salidaNueva.setBodega(idInventario.getIdInventario().getBodega());
        salidaNueva.setEstadoMovimientoSalida(movimientoSalida.getEstadoMovimientoSalida());
        salidaNueva.setFechaRegistro(movimientoSalida.getFechaRegistro());
        salidaNueva.setFirma(movimientoSalida.getFirma());
        salidaNueva.setProducto(idInventario.getIdInventario().getProducto());
        salidaNueva.setSerial(idInventario.getSerial());
        return salidaNueva;
    }

    private void persistirSalida(T_MovimientoSalidas salida) {
        try {
            administracionMovimientoSalidas.crearSalida(salida);
        } catch (Exception exception) {
        }
    }

    private void resetFiles() {
        if (!inventarioActual.isEmpty()) {
            inventarioActual.clear();
        }
        if (!listaSeleccionSeriales.isEmpty()) {
            listaSeleccionSeriales.clear();
        }
        movimientoSalida = new T_MovimientoSalidas();
    }

    private void agrupingproductoKardex(T_InventariosSeriales serialInventario) {
        try {
            if (auxKardex.isEmpty()) {
                //Es Vacio
                SerialesKardex kardex = new SerialesKardex();
                kardex.setCantidad(1);
                kardex.setProducto(serialInventario.getIdInventario().getProducto());
                ArrayList<SerialesGarantiaArray> serial = new ArrayList();
                SerialesGarantiaArray objNewSerialGarantia = new SerialesGarantiaArray();
                objNewSerialGarantia.setGarantiaMes(serialInventario.getGarantiaMes());
                objNewSerialGarantia.setSerial(serialInventario.getSerial());
                serial.add(objNewSerialGarantia);
                kardex.setSerial(serial);
                auxKardex.add(kardex);
            } else if (auxKardex.stream().map(mapper -> mapper.getProducto()).collect(Collectors.toList()).contains(serialInventario.getIdInventario().getProducto())) {
                for (SerialesKardex k : auxKardex) {
                    if (serialInventario.getIdInventario().getProducto().getDescripcion().equals(k.getProducto().getDescripcion())) {
                        k.setCantidad(k.getCantidad() + 1);
                        ArrayList<SerialesGarantiaArray> serial = new ArrayList();
                        SerialesGarantiaArray objNewSerialGarantia = new SerialesGarantiaArray();
                        objNewSerialGarantia.setGarantiaMes(serialInventario.getGarantiaMes());
                        objNewSerialGarantia.setSerial(serialInventario.getSerial());
                        serial = k.getSerial();
                        serial.add(objNewSerialGarantia);
                        k.setSerial(serial);
                        System.out.println("Sirvio");
                    }
                }
            } else {
                // PRODUCTO NUEVO
                SerialesKardex kardex = new SerialesKardex();
                kardex.setCantidad(1);
                kardex.setProducto(serialInventario.getIdInventario().getProducto());
                ArrayList<SerialesGarantiaArray> serial = new ArrayList();
                SerialesGarantiaArray objNewSerialGarantia = new SerialesGarantiaArray();
                objNewSerialGarantia.setGarantiaMes(serialInventario.getGarantiaMes());
                objNewSerialGarantia.setSerial(serialInventario.getSerial());
                serial.add(objNewSerialGarantia);
                kardex.setSerial(serial);
                auxKardex.add(kardex);
            }
        } catch (Exception e) {
        }
    }

    private void realizarSalidaDeCardex() {
        for (SerialesKardex k : auxKardex) {
            T_Kardex nuevoKardex = new T_Kardex();
            T_Kardex kardexExistente = administracionKardex.findByProductoXBodega(k.getProducto(), movimientoSalida.getBodega());
            nuevoKardex.setBodega(movimientoSalida.getBodega());
            nuevoKardex.setCantidad(new BigDecimal(k.getCantidad()));
            nuevoKardex.setCantidadTotal(kardexExistente.getCantidadTotal().subtract(nuevoKardex.getCantidad()));
            nuevoKardex.setConcepto("Venta");
            nuevoKardex.setFechaMovimiento(new Date());
            nuevoKardex.setProducto(k.getProducto());
            nuevoKardex.setValorUnitario(kardexExistente.getValorUnitario());
            nuevoKardex.setValor(kardexExistente.getValorUnitario().multiply(nuevoKardex.getCantidad()));
            nuevoKardex.setValorTotal(kardexExistente.getValorTotal().subtract(nuevoKardex.getValor()));
            nuevoKardex.setDetalleKardexSeriales(homologarSeriales(nuevoKardex, k));
            Long id = administracionKardex.crearAndReturnId(nuevoKardex);

            notificarMensajeExitoso("Se registro correctamente en el kardex el numero de radicado es: " + id + " Guardelo para futuras devoluciones ");
        }
    }

    private List<T_DetalleKardex> homologarSeriales(T_Kardex nuevoKardex, SerialesKardex k) {
        List<T_DetalleKardex> list = new ArrayList();
        for (SerialesGarantiaArray serial : k.getSerial()) {
            T_DetalleKardex detalle = new T_DetalleKardex();
            detalle.setKardex(nuevoKardex);
            detalle.setSeriales((String) serial.getSerial());
            detalle.setGarantiaMes(serial.getGarantiaMes());
            list.add(detalle);
        }
        return list;
    }


}
