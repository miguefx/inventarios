/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beanGestionRecetaEnsamble;

import VO.T_OrdenDeCompra;
import VO.T_RecetaDeEnsamble;
import com.sippa.inventario.sippa.negocio.OrdenCompra.AdministacionOrdenCompraLocal;
import com.sippa.inventarios.sippa.ejb.recetaEnsamble.T_RecetaDeEnsambleFacadeLocal;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author matc_
 */
@ManagedBean(name = "beanGestionRecetaEnsamble")
@ViewScoped
public class beanGestionRecetaEnsamble implements Serializable {

    @EJB
    T_RecetaDeEnsambleFacadeLocal administracionRecetaEnsamble;

    @Getter
    @Setter
    private List<T_RecetaDeEnsamble> listRecetaEnsamble;

    @Getter
    @Setter
    private List<T_RecetaDeEnsamble> listRecetaEnsambleFilter;

    @Getter
    @Setter
    private T_RecetaDeEnsamble recetaDeEnsamble;

    @PostConstruct
    public void init() {
        listRecetaEnsamble = new ArrayList<>();
        listRecetaEnsambleFilter = new ArrayList<>();
        consultarOrdenesDeCompra();
    }

    private void consultarOrdenesDeCompra() {
        listRecetaEnsamble = administracionRecetaEnsamble.obtenerRecetasXEstado(Boolean.TRUE);
    }

    public void inactivarRecetaEnsamble(T_RecetaDeEnsamble orden) {
        orden.setEstado(Boolean.FALSE);
        administracionRecetaEnsamble.edit(orden);
        consultarOrdenesDeCompra();
        notificarMensajeExitoso("Se inactivo correctamente la receta seleccionada");
    }

    public beanGestionRecetaEnsamble() {
    }
    
    public void notificarMensajeError(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", mensaje));
    }

    public void notificarMensajeExitoso(String mensaje) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Informacion!", mensaje));
    }
}
