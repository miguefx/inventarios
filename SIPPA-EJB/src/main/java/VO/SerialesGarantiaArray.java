/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;

/**
 *
 * @author Miguel-PC
 */
@Data
public class SerialesGarantiaArray {
    private String serial;
    private BigDecimal garantiaMes;

}
