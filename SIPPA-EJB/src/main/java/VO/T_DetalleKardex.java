/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author matc_
 */
@EqualsAndHashCode(of = {"idDetalleKardex"})
@ToString(of = {"idDetalleKardex"}, includeFieldNames = true)
@Entity
@Table(name = "T_DetalleKardex")
public class T_DetalleKardex implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    @Column(name = "IdDetalleKardex")
    private Long idDetalleKardex;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "IdHistorialInventario", referencedColumnName = "IdHistorialInventario")
    private T_Kardex kardex;

    @Getter
    @Setter
    @Column(name = "SerialDeSalida")
    private String seriales;

    @Getter
    @Setter
    @Column(name = "GarantiaMeses")
    private BigDecimal garantiaMes;

    public T_DetalleKardex() {
    }

}
