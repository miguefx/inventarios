/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author matc_
 */
@EqualsAndHashCode(of = {"idPermiso"})
@ToString(of = {"idPermiso"}, includeFieldNames = true)
@Entity
@Table(name = "T_Permisos")
@NamedQueries({
    @NamedQuery(name = "T_Permisos.findByIdUsuario", query = "SELECT p from T_Permisos p where p.idUsuario = :idusuario")})

public class T_Permisos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    @Column(name = "IdPermiso")
    private Long idPermiso;

    @Getter
    @Setter
    @Column(name = "IdUsuario")
    private Long idUsuario;

    @Getter
    @Setter
    @Column(name = "Modulo")
    private String modulo;

}
