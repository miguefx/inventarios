package VO;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


@EqualsAndHashCode(of = {"idMovimientoSalida"})
@ToString(of = {"idMovimientoSalida"}, includeFieldNames = true)
@Entity
@Table(name = "T_MovimientosSalidas")
@Data
public class T_MovimientoSalidas implements Serializable {
  private static final long serialVersionUID = 1L;
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "IdMovimientoSalida")
  private Long idMovimientoSalida;
  
  @ManyToOne
  @JoinColumn(name = "IdBodega", referencedColumnName = "IdBodega")
  private T_Bodegas bodega;
  
  @Column(name = "Estado")
  @Enumerated(EnumType.STRING)
  private EstadosMovimientoSalida estadoMovimientoSalida;
  
  @Column(name = "FechaSalida")
  @Temporal(TemporalType.TIMESTAMP)
  private Date fechaRegistro;
  
  @Column(name = "Firma")
  private String firma;
  
  @ManyToOne
  @JoinColumn(name = "IdProducto", referencedColumnName = "IdEquipo")
  private T_Productos producto;
  
  @Column(name = "Serial")
  private String serial;
  
}
