/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author matc_
 */
@EqualsAndHashCode(of = {"idCliente"})
@ToString(of = {"idCliente"}, includeFieldNames = true)
@Entity
@Table(name = "T_Clientes")
@NamedQueries({
    @NamedQuery(name = "T_Clientes.findById", query = "SELECT u from T_Clientes u  WHERE u.idCliente = :idCliente"),
    @NamedQuery(name = "T_Clientes.findByEstado", query = "SELECT u from T_Clientes u  WHERE u.estado = :estado")})
public class T_Clientes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    @Column(name = "IDCLIENTE")
    private Long idCliente;

    @Getter
    @Setter
    @Column(name = "RAZONSOCIAL")
    private String razonSocial;

    @Getter
    @Setter
    @Column(name = "NITCEDULA")
    private String nitCedula;

    @Getter
    @Setter
    @Column(name = "DIRECCION")
    private String direccion;

    @Getter
    @Setter
    @Column(name = "TELEFONO")
    private String telefono;

    @Getter
    @Setter
    @Column(name = "NOMBRECONTACTO")
    private String nombreContacto;

    @Getter
    @Setter
    @Column(name = "CARGO")
    private String cargo;

    @Getter
    @Setter
    @Column(name = "EMAIL")
    private String email;

    @Getter
    @Setter
    @Column(name = "ESTADO")
    private Boolean estado;

    @Setter
    @Getter
    @OneToMany(mappedBy = "idCliente", cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    private List<T_ClientesXSedes> clientesXSedes;

    public T_Clientes() {
        estado = Boolean.TRUE;
    }

}
