/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author matc_
 */
@EqualsAndHashCode(of = {"idParametro"})
@ToString(of = {"idParametro"}, includeFieldNames = true)
@Entity
@Table(name = "T_Parametros")
@NamedQueries({
    @NamedQuery(name = "T_Parametros.findByParametroGenerico", query = "SELECT u from T_Parametros u  WHERE u.codigo = :codigo")})
public class T_Parametros implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    @Column(name = "IdParametro")
    private Long idParametro;

    @Getter
    @Setter
    @Column(name = "Codigo")
    private String codigo;

    @Getter
    @Setter
    @Column(name = "Valor")
    private String valor;

    @Getter
    @Setter
    @Column(name = "Descripcion")
    private String descripcion;

    @Getter
    @Setter
    @Column(name = "Estado")
    private Boolean estado;

    public T_Parametros() {
    }
    

}
