/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author matc_
 */
@EqualsAndHashCode(of = {"idOrdenDeCompra"})
@ToString(of = {"idOrdenDeCompra"}, includeFieldNames = true)
@Entity
@Table(name = "T_OrdenDeCompra")
@NamedQueries({
    @NamedQuery(name = "T_OrdenDeCompra.findByEstado", query = "SELECT u from T_OrdenDeCompra u  WHERE u.estado = :estado"),
    @NamedQuery(name = "T_OrdenDeCompra.findById", query = "SELECT u from T_OrdenDeCompra u  WHERE u.idOrdenDeCompra = :idOrdenCompra")})
public class T_OrdenDeCompra implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Getter
    @Setter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IdOrdenDeCompra")
    private Long idOrdenDeCompra;

    @Getter
    @Setter
    @Column(name = "NombreContacto")
    private String nombreContacto;

    @Getter
    @Setter
    @Column(name = "ApellidoContacto")
    private String apellidoContacto;

    @Getter
    @Setter
    @Column(name = "Empresa")
    private String empresa;

    @Getter
    @Setter
    @Column(name = "Nit")
    private String nit;

    @Getter
    @Setter
    @Column(name = "Direccion")
    private String direccion;

    @Getter
    @Setter
    @Column(name = "Contactos")
    private String contactos;

    @Getter
    @Setter
    @Column(name = "Ciudad")
    private String ciudad;

    @Getter
    @Setter
    @Column(name = "Telefono")
    private String telefono;

    @Getter
    @Setter
    @Column(name = "Correo")
    private String correo;

    @Getter
    @Setter
    @Column(name = "TipoDeCompra")
    private String tipoDeCompra;

    @Getter
    @Setter
    @Column(name = "Estado")
    private Boolean estado;

    @Getter
    @Setter
    @Column(name = "Fecha")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fecha;

    @Getter
    @Setter
    @Column(name = "TerminoNegociacion")
    private String terminoNegociacion;

    @Getter
    @Setter
    @Column(name = "FormaPago")
    private String formaPago;

    @Getter
    @Setter
    @Column(name = "DireccionEntrega")
    private String direccionEntrega;

    @Getter
    @Setter
    @Column(name = "Procesado")
    private String procesado;

    @Getter
    @Setter
    @Column(name = "Aprobado")
    private String aprobado;

    @Getter
    @Setter
    @Column(name = "Observaciones")
    private String observaciones;
    
    @Getter
    @Setter
    @Column(name = "Impuestos")
    private BigDecimal impuesto;

    @Setter
    @Getter
    @OneToMany(mappedBy = "idOrdenDeCompra", cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    private List<T_DetalleOrdenDeCompra> detalleOrdenDeCompra;

}
