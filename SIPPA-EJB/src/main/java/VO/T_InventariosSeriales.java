package VO;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(of = {"idInventarioSerial"})
@ToString(of = {"idInventarioSerial"}, includeFieldNames = true)
@Entity
@Table(name = "T_InventariosSeriales")
@Data
@NamedQueries({
    @NamedQuery(name = "InventariosSeriales_FindByIdInventario", query = "SELECT u from T_InventariosSeriales u  WHERE u.idInventario.idInventario = :id")})
public class T_InventariosSeriales implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IdInventarioSerial")
    private Long idInventarioSerial;

    @Column(name = "Serial")
    private String serial;

    @Column(name = "GarantiaMeses")
    private BigDecimal garantiaMes;

    @ManyToOne
    @JoinColumn(name = "IdInventario", referencedColumnName = "IdInventario")
    private T_Inventarios idInventario;
}
