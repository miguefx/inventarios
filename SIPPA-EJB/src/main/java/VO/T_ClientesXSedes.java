/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author matc_
 */
@EqualsAndHashCode(of = {"idClienteXSede"})
@ToString(of = {"idClienteXSede"}, includeFieldNames = true)
@Entity
@Table(name = "T_ClientesXSedes")
public class T_ClientesXSedes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    @Column(name = "IDCLIENTEXSEDE")
    private Long idClienteXSede;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "IDSEDE", referencedColumnName = "IdSede")
    private T_Sedes idSede;
    
    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "IDCLIENTE", referencedColumnName = "IDCLIENTE")
    private T_Clientes idCliente;


    public T_ClientesXSedes() {
    }

    
    
    
}
