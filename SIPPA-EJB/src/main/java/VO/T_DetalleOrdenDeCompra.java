/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author matc_
 */
@EqualsAndHashCode(of = {"idDetalleOrdenDeCompra"})
@ToString(of = {"idDetalleOrdenDeCompra"}, includeFieldNames = true)
@Entity
@Table(name = "T_DetalleOrdenDeCompra")
public class T_DetalleOrdenDeCompra implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Getter
    @Setter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IdDetalleOrdenDeCompra")
    private Long idDetalleOrdenDeCompra;

    @Getter
    @Setter
    @Column(name = "Item")
    private BigDecimal item;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "IdProducto", referencedColumnName = "IdEquipo")
    private T_Productos producto;

    @Getter
    @Setter
    @Column(name = "Cantidad")
    private BigDecimal cantidad;

    @Getter
    @Setter
    @Column(name = "ValorUnitario")
    private BigDecimal valorUnitario;

    @Getter
    @Setter
    @Column(name = "ValorTotal")
    private BigDecimal valorTotal;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "IdOrdenDeCompra", referencedColumnName = "IdOrdenDeCompra")
    private T_OrdenDeCompra idOrdenDeCompra;

    @Setter
    @Getter
    @OneToMany(mappedBy = "idDetalleOrdenDeCompra", cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    private List<T_DetalleOrdenXSerialProductos> detalleSerialesProductos;

    public T_DetalleOrdenDeCompra(BigDecimal item, T_Productos producto) {
        this.producto = producto;
        this.item = item;
        this.cantidad = BigDecimal.ZERO;
        this.valorUnitario = BigDecimal.ZERO;
        this.valorTotal = BigDecimal.ZERO;
    }

    public T_DetalleOrdenDeCompra() {
    }

}
