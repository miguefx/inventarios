/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

import lombok.Data;

/**
 *
 * @author matc_
 */
@Data
public class GraficaStockMinimo {

    private String producto;

    private int cantidad;


    public GraficaStockMinimo() {
    }

    public GraficaStockMinimo(String producto, int cantidad) {
        this.producto = producto;
        this.cantidad = cantidad;
    }

}
