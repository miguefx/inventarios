/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author matc_
 */
@EqualsAndHashCode(of = {"idProducto"})
@ToString(of = {"idProducto"}, includeFieldNames = true)
@Entity
@Table(name = "T_Productos")
@NamedQueries({
    @NamedQuery(name = "T_Productos.findById", query = "SELECT u from T_Productos u  WHERE u.idProducto = :idProducto"),
    @NamedQuery(name = "T_Productos.findByEstado", query = "SELECT u from T_Productos u  WHERE u.estado = :estado"),
    @NamedQuery(name = "T_Productos.findByStockMinimo", query = "SELECT u from T_Productos u  WHERE u.estado = :estado AND u.stockMinimo IS NOT NULL")})
public class T_Productos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    @Column(name = "IdEquipo")
    private Long idProducto;

    @Getter
    @Setter
    @Column(name = "Descripcion")
    private String descripcion;

    @Getter
    @Setter
    @Column(name = "Marca")
    private String marca;

    @Getter
    @Setter
    @Column(name = "Modelo")
    private String modelo;

    @Getter
    @Setter
    @Column(name = "FechaRegistro")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date fechaRegistro;

    @Getter
    @Setter
    @Lob
    @Column(name = "Foto")
    private byte[] foto;

    @Getter
    @Setter
    @Column(name = "Estado")
    private Boolean estado;

    @Getter
    @Setter
    @Column(name = "Codigo")
    private String codigo;

    @Getter
    @Setter
    @Column(name = "StockMinimo")
    private BigDecimal stockMinimo;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "IdReceta", referencedColumnName = "IdReceta")
    private T_RecetaDeEnsamble recetaReferenciada;

    @Setter
    @Getter
    @OneToMany(mappedBy = "idProducto", cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    private List<T_ProductosProveedores> productosXProvedores;

    public T_Productos() {
        estado = Boolean.TRUE;
    }

}
