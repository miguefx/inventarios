/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author matc_
 */
@EqualsAndHashCode(of = {"idReceta"})
@ToString(of = {"idReceta"}, includeFieldNames = true)
@Entity
@Table(name = "T_RecetasDeEnsamble")
@NamedQueries({
    @NamedQuery(name = "T_RecetaDeEnsamble.findById", query = "SELECT u from T_RecetaDeEnsamble u  WHERE u.idReceta = :idReceta"),
    @NamedQuery(name = "T_RecetaDeEnsamble.findByEstado", query = "SELECT u from T_RecetaDeEnsamble u  WHERE u.estado = :estado")})
public class T_RecetaDeEnsamble implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Getter
    @Setter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IdReceta")
    private Long idReceta;

    @Getter
    @Setter
    @Column(name = "Estado")
    private Boolean estado;

    @Getter
    @Setter
    @Column(name = "NombreReceta")
    private String nombreReceta;

    @Getter
    @Setter
    @Column(name = "FechaCreacion")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fechaCreacion;

    @Setter
    @Getter

    @OneToMany(mappedBy = "idReceta", cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    private List<T_DetalleRecetaDeEnsamble> detalleReceta;

    public T_RecetaDeEnsamble() {
    }

}
