/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

import java.math.BigDecimal;
import java.util.List;
import lombok.Data;

/**
 *
 * @author matc_
 */
@Data
public class T_reporteKardex {

    private String codigoProducto;
    private String nombreProducto;
    private String ubicacion;
    private BigDecimal cantidad;
    private BigDecimal valorUnitario;
    private BigDecimal valorTotal;

    public T_reporteKardex(String codigoProducto, String nombreProducto, String ubicacion, BigDecimal cantidad, BigDecimal valorUnitario, BigDecimal valorTotal) {
        this.codigoProducto = codigoProducto;
        this.nombreProducto = nombreProducto;
        this.ubicacion = ubicacion;
        this.cantidad = cantidad;
        this.valorUnitario = valorUnitario;
        this.valorTotal = valorTotal;
    }

    public T_reporteKardex() {
    }
    
}
