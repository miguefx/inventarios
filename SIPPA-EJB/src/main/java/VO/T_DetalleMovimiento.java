/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author matc_
 */
@EqualsAndHashCode(of = {"idDetalleMovimiento"})
@ToString(of = {"idDetalleMovimiento"}, includeFieldNames = true)
@Entity
@Table(name = "T_DetalleMovimiento")
public class T_DetalleMovimiento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Getter
    @Setter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDDETALLEMOVIMIENTO")
    private Long idDetalleMovimiento;

    @Getter
    @Setter
    @Column(name = "ITEM")
    private BigDecimal item;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "IdProducto", referencedColumnName = "IdEquipo")
    private T_Productos producto;

    @Getter
    @Setter
    @Column(name = "CANTIDAD")
    private BigDecimal cantidad;

    @Getter
    @Setter
    @Column(name = "VALORUNITARIO")
    private BigDecimal valorUnitario;

    @Getter
    @Setter
    @Column(name = "VALORTOTAL")
    private BigDecimal valorTotal;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "IDMOVIMIENTO", referencedColumnName = "IDMOVIMIENTO")
    private T_Movimientos idMovimiento;
    
    @Setter
    @Getter
    @OneToMany(mappedBy = "idDetalleMovimientos", cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    private List<T_DetalleMovimientoXSerialProductos> detalleSerialesProductos;

    public T_DetalleMovimiento(BigDecimal item, T_Productos producto) {
        this.producto = producto;
        this.item = item;
        this.cantidad = BigDecimal.ZERO;
        this.valorUnitario = BigDecimal.ZERO;
        this.valorTotal = BigDecimal.ZERO;
    }

    public T_DetalleMovimiento() {
    }

}
