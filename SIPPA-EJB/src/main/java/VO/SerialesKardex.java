/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;

/**
 *
 * @author Miguel-PC
 */
@Data
public class SerialesKardex {

    private T_Productos producto;
    private int cantidad;
    private ArrayList<SerialesGarantiaArray> serial = new ArrayList();
}
