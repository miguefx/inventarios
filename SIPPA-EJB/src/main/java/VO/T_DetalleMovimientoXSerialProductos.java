/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author matc_
 */
@EqualsAndHashCode(of = {"idSerialProductoDetalleEntrada"})
@ToString(of = {"idSerialProductoDetalleEntrada"}, includeFieldNames = true)
@Entity
@Table(name = "T_DetalleOrdenXSerialProductos")
public class T_DetalleMovimientoXSerialProductos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    @Column(name = "IdSerialProductosDetalleEntrada")
    private Long idSerialProductoDetalleEntrada;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "IdProducto", referencedColumnName = "IdEquipo")
    private T_Productos idProducto;

    @Getter
    @Setter
    @Column(name = "Serial")
    private String serial;
    
    @Getter
    @Setter
    @Column(name = "GarantiaMes")
    private BigDecimal garantiaMes;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "IdDetalleMovimiento", referencedColumnName = "IDDETALLEMOVIMIENTO")
    private T_DetalleMovimiento idDetalleMovimientos;

    public T_DetalleMovimientoXSerialProductos() {
    }

    public T_DetalleMovimientoXSerialProductos(T_Productos producto) {
        this.idProducto = producto;
    }

}
