/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author matc_
 */
@EqualsAndHashCode(of = {"idSede"})
@Entity
@Table(name = "T_Sedes")
@NamedQueries({
    @NamedQuery(name = "T_Sedes.findById", query = "SELECT u from T_Sedes u  WHERE u.idSede = :idSede"),
    @NamedQuery(name = "T_Sedes.findByEstado", query = "SELECT u from T_Sedes u  WHERE u.estado = :estado")})
public class T_Sedes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    @Column(name = "IdSede")
    private Long idSede;

    @Getter
    @Setter
    @Column(name = "Nombre")
    private String nombre;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "IdCiudad", referencedColumnName = "IdCiudad")
    private T_Ciudades ciudad;

    @Getter
    @Setter
    @Column(name = "Direccion")
    private String direccion;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "IdUsuarioAsignado", referencedColumnName = "IdUsuario")
    private T_Usuarios usuario;

    @Getter
    @Setter
    @Column(name = "TelefonoContacto")
    private String telefono;
    
    @Getter
    @Setter
    @Column(name = "Estado")
    private Boolean estado;
    
    @Setter
    @Getter
    @OneToMany(mappedBy = "idSede", cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    private List<T_ClientesXSedes> clientesXSedes;
    
    
    
    @Override
    public String toString() {
        return idSede.toString();
    }

    public T_Sedes() {
    }

    public T_Sedes(Long idSede, String nombre, T_Ciudades ciudad, String direccion, T_Usuarios usuario, String telefono) {
        this.estado=Boolean.TRUE;
        this.idSede = idSede;
        this.nombre = nombre;
        this.ciudad = ciudad;
        this.direccion = direccion;
        this.usuario = usuario;
        this.telefono = telefono;
    }
    
    

}
