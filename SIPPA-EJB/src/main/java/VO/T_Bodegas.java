/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 *
 * @author matc_
 */
@EqualsAndHashCode(of = {"idBodega"})
@ToString(of = {"idBodega"}, includeFieldNames = true)
@Entity
@Table(name = "T_Bodegas")
@NamedQueries({
    @NamedQuery(name = "T_Bodegas.findByEstado", query = "SELECT u from T_Bodegas u  WHERE u.estado = :estado")})
@Data
public class T_Bodegas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "IdBodega")
    private Long idBodega;

    @ManyToOne
    @JoinColumn(name = "IdSede", referencedColumnName = "IdSede")
    private T_Sedes sede;

    @ManyToOne
    @JoinColumn(name = "IdUsuarioResponsable", referencedColumnName = "IdUsuario")
    private T_Usuarios usuario;

    @Column(name = "Direccion")
    private String direccion;

    @Column(name = "Telefono")
    private String telefono;

    @Column(name = "Nombre")
    private String nombre;

    @Column(name = "Estado")
    private Boolean estado;

    public T_Bodegas() {
    }

}
