package VO;

public enum EstadosMovimientoSalida {
  PRESTAMO, PROCESO, INSTALACION, CERRADO;
}
