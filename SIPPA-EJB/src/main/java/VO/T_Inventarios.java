/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author matc_
 */
@EqualsAndHashCode(of = {"idInventario"})
@ToString(of = {"idInventario"}, includeFieldNames = true)
@Entity
@Table(name = "T_Inventarios")
@NamedQueries({
    @NamedQuery(name = "T_Inventarios.findByProductoXBodega", query = "SELECT u from T_Inventarios u  WHERE u.producto = :producto and u.bodega = :bodega"),
    @NamedQuery(name = "T_Inventarios.findAllByStockMinimoSuperado", query = "SELECT u from T_Inventarios u  WHERE u.producto.stockMinimo IS NOT NULL"),
    @NamedQuery(name = "T_Inventarios.findByBodega", query = "SELECT u from T_Inventarios u  WHERE u.bodega = :bodega")})
@SqlResultSetMapping(name = "GraficaStockMinimo", classes = {
    @ConstructorResult(targetClass = GraficaStockMinimo.class, columns = {
        @ColumnResult(name = "producto", type = String.class),
        @ColumnResult(name = "cantidad", type = Integer.class)})})
public class T_Inventarios implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    @Column(name = "IdInventario")
    private Long idInventario;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "IdBodega", referencedColumnName = "IdBodega")
    private T_Bodegas bodega;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "IdProducto", referencedColumnName = "IdEquipo")
    private T_Productos producto;

    @Getter
    @Setter
    @Column(name = "CANTIDAD")
    private BigDecimal cantidad;

    @Getter
    @Setter
    @Column(name = "VALORTOTAL")
    private BigDecimal valorTotal;

    @Getter
    @Setter
    @OneToMany(mappedBy = "idInventario", cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    private List<T_InventariosSeriales> serialesProductos;

    public T_Inventarios() {
    }

}
