/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author matc_
 */
@EqualsAndHashCode(of = {"idUsuario"})
@ToString(of = {"idUsuario"}, includeFieldNames = true)
@Entity
@Table(name = "T_Usuarios")
@NamedQueries({
    @NamedQuery(name = "T_Usarios.findByUserXPassword", query = "SELECT u from T_Usuarios u  WHERE u.usuario = :usuario and u.password = :password "),
    @NamedQuery(name = "T_Usarios.findByUsuariosXEstado", query = "SELECT u from T_Usuarios u  WHERE u.estado = :estado order by u.nombres"),
})

public class T_Usuarios implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    @Column(name = "IdUsuario")
    private Long idUsuario;

    @Getter
    @Setter
    @Column(name = "Documento")
    private String documento;

    @Getter
    @Setter
    @Column(name = "Nombres")
    private String nombres;

    @Getter
    @Setter
    @Column(name = "Apellidos")
    private String apellidos;

    @Getter
    @Setter
    @Column(name = "Usuario")
    private String usuario;

    @Getter
    @Setter
    @Column(name = "Contraseña")
    private String password;

    @Getter
    @Setter
    @Column(name = "Cargo")
    private String cargo;

    @Getter
    @Setter
    @Column(name = "UsuarioCreador")
    private String usuarioCreador;

    @Getter
    @Setter
    @Column(name = "FechaCreacion")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date fechaCreacion;

    @Getter
    @Setter
    @Column(name = "Estado")
    private Boolean estado;

    public T_Usuarios() {
        fechaCreacion = new Date();
        estado = Boolean.TRUE;
    }

}
