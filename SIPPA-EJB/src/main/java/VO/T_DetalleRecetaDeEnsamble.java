/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author matc_
 */
@EqualsAndHashCode(of = {"idDetalleRecetaEnsamble"})
@ToString(of = {"idDetalleRecetaEnsamble"}, includeFieldNames = true)
@Entity
@Table(name = "T_DetalleRecetaEnsamble")
public class T_DetalleRecetaDeEnsamble implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Getter
    @Setter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IdDetalleReceta")
    private Long idDetalleRecetaEnsamble;

    @Getter
    @Setter
    @Column(name = "Item")
    private BigDecimal item;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "IdProducto", referencedColumnName = "IdEquipo")
    private T_Productos producto;

    @Getter
    @Setter
    @Column(name = "Cantidad")
    private BigDecimal cantidad;

    @Getter
    @Setter
    @Column(name = "ValorUnitario")
    private BigDecimal valorUnitario;

    @Getter
    @Setter
    @Column(name = "ValorTotal")
    private BigDecimal valorTotal;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(referencedColumnName = "IdReceta", name = "IdReceta")
    private T_RecetaDeEnsamble idReceta;

    public T_DetalleRecetaDeEnsamble(BigDecimal item, T_Productos producto) {
        this.producto = producto;
        this.item = item;
        this.cantidad = BigDecimal.ZERO;
        this.valorUnitario = BigDecimal.ZERO;
        this.valorTotal = BigDecimal.ZERO;
    }

    public T_DetalleRecetaDeEnsamble() {
    }

}
