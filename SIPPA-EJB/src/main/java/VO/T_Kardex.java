/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author matc_
 */
@EqualsAndHashCode(of = {"idHistorialInventario"})
@ToString(of = {"idHistorialInventario"}, includeFieldNames = true)
@Entity
@Table(name = "T_Kardex")
@NamedQueries({
    @NamedQuery(name = "Kardex_findByProductoBodegaUltimo", query = "SELECT u from T_Kardex u  WHERE u.producto.idProducto = :producto and u.bodega.idBodega = :bodega ORDER BY u.fechaMovimiento DESC"),
    @NamedQuery(name = "Kardex_findByDistinct", query = "SELECT DISTINCT u.producto.idProducto , u.bodega.idBodega from T_Kardex u")})
@SqlResultSetMapping(name = "GraficaGeneralKardex", classes = {
    @ConstructorResult(targetClass = T_reporteKardexAux.class, columns = {
        @ColumnResult(name = "idProducto", type = Long.class),
        @ColumnResult(name = "idBodega", type = Long.class)})})
public class T_Kardex implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    @Column(name = "IdHistorialInventario")
    private Long idHistorialInventario;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "IdBodega", referencedColumnName = "IdBodega")
    private T_Bodegas bodega;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "IdProducto", referencedColumnName = "IdEquipo")
    private T_Productos producto;
    @Getter
    @Setter
    @Column(name = "NumeroFactura")
    private String factura;

    @Getter
    @Setter
    @Column(name = "ValorUnitario")
    private BigDecimal valorUnitario;

    @Getter
    @Setter
    @Column(name = "Cantidad")
    private BigDecimal cantidad;

    @Getter
    @Setter
    @Column(name = "Valor")
    private BigDecimal valor;

    @Getter
    @Setter
    @Column(name = "CantidadTotal")
    private BigDecimal cantidadTotal;

    @Getter
    @Setter
    @Column(name = "ValorTotal")
    private BigDecimal valorTotal;

    @Getter
    @Setter
    @Column(name = "Concepto")
    private String concepto;

    @Getter
    @Setter
    @Column(name = "FechaMovimiento")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date fechaMovimiento;

    @Getter
    @Setter
    @OneToMany(mappedBy = "kardex", cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    private List<T_DetalleKardex> detalleKardexSeriales;

    @Getter
    @Setter
    @Column(name = "IsDevuelto")
    private Boolean isDevuelto;

    public T_Kardex() {
        isDevuelto = false;
    }

}
