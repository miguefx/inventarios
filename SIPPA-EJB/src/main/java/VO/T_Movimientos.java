/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author matc_
 */
@EqualsAndHashCode(of = {"idMovimiento"})
@ToString(of = {"idMovimiento"}, includeFieldNames = true)
@Entity
@Table(name = "T_Movimientos")
public class T_Movimientos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Getter
    @Setter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDMOVIMIENTO")
    private Long idMovimiento;

    @Getter
    @Setter
    @Column(name = "FECHAFACTURA")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date fechaFactura;

    
    @Getter
    @Setter
    @Column(name = "NUMEROFACTURA")
    private String numeroFactura;

    
    @Getter
    @Setter
    @Lob
    @Column(name = "FOTOFACTURA")
    private byte[] fotoFactura;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "IDBODEGA", referencedColumnName = "IdBodega")
    private T_Bodegas bodega;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "IDPROVEEDOR", referencedColumnName = "IdProveedor")
    private T_Proveedores proveedor;

    
    @Getter
    @Setter
    @Column(name = "FECHAREGISTRO")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date fechaRegistro;
    
    
    @Getter
    @Setter
    @Column(name = "TIPOMOVIMIENTO")
    @Enumerated(EnumType.STRING)
    private TiposMovimientos tipoMovimiento;

    
    @Getter
    @Setter
    @OneToMany(mappedBy = "idMovimiento", cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    private List<T_DetalleMovimiento> detalleMovimiento;

    public T_Movimientos() {
    }

}
