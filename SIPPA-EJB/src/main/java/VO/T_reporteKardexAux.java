/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

import java.math.BigDecimal;
import java.util.List;
import lombok.Data;

/**
 *
 * @author matc_
 */
@Data
public class T_reporteKardexAux {

    private Long idProducto;
    private Long idBodega;

    public T_reporteKardexAux(Long idProducto, Long idBodega) {
        this.idProducto = idProducto;
        this.idBodega = idBodega;
    }
    
}
