/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author matc_
 */
@EqualsAndHashCode(of = {"idProveedor"})
@ToString(of = {"idProveedor"}, includeFieldNames = true)
@Entity
@Table(name = "T_Proveedores")
@NamedQueries({
    @NamedQuery(name = "T_Proveedores.findById", query = "SELECT u from T_Proveedores u  WHERE u.idProveedor = :idproveedor"),
    @NamedQuery(name = "T_Proveedores.findByEstado", query = "SELECT u from T_Proveedores u  WHERE u.estado = :estado")})
public class T_Proveedores implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    @Column(name = "IdProveedor")
    private Long idProveedor;

    @Getter
    @Setter
    @Column(name = "Nit")
    private String nit;

    @Getter
    @Setter
    @Column(name = "RazonSocial")
    private String razonSocial;

    @Getter
    @Setter
    @Column(name = "Correo")
    private String correo;

    @Getter
    @Setter
    @Column(name = "TelefonoContacto")
    private String telefonoContacto;

    @Getter
    @Setter
    @Column(name = "NombreContacto")
    private String nombreContacto;

    @Getter
    @Setter
    @Column(name = "Direccion")
    private String direccion;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "Ciudad", referencedColumnName = "IdCiudad")
    private T_Ciudades ciudad;
    
    @Getter
    @Setter
    @Column (name = "Estado")
    private Boolean estado;

    @Setter
    @Getter
    @OneToMany(mappedBy = "idProveedor", cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    private List<T_ProductosProveedores> productos;
}
