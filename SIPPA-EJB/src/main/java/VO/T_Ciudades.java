/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author matc_
 */
@EqualsAndHashCode(of = {"idCiudad"})
@ToString(of = {"idCiudad"}, includeFieldNames = true)
@Entity
@Table(name = "T_Ciudades")
public class T_Ciudades implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    @Column(name = "IdCiudad")
    private Long idCiudad;

    @Getter
    @Setter
    @Column(name = "Ciudad")
    private String ciudad;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "IdPais", referencedColumnName = "IdPais")
    private T_Paises pais;

}
