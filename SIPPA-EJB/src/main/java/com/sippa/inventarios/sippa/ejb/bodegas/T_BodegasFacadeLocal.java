/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.bodegas;

import VO.T_Bodegas;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author matc_
 */
@Local
public interface T_BodegasFacadeLocal {

    void create(T_Bodegas t_Bodegas);

    void edit(T_Bodegas t_Bodegas);

    void remove(T_Bodegas t_Bodegas);

    T_Bodegas find(Object id);

    List<T_Bodegas> findAll();

    List<T_Bodegas> findRange(int[] range);

    int count();

    List<T_Bodegas> consultarBodegasByEstado(Boolean estado);
    
}
