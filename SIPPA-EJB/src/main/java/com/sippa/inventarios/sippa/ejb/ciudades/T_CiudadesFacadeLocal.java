/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.ciudades;

import VO.T_Ciudades;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author matc_
 */
@Local
public interface T_CiudadesFacadeLocal {

    void create(T_Ciudades t_Ciudades);

    void edit(T_Ciudades t_Ciudades);

    void remove(T_Ciudades t_Ciudades);

    T_Ciudades find(Object id);

    List<T_Ciudades> findAll();

    List<T_Ciudades> findRange(int[] range);

    int count();
    
}
