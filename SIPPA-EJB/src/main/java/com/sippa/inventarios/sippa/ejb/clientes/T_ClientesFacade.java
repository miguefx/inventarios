/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.clientes;

import VO.T_Clientes;
import com.sippa.inventarios.sippa.ejb.persistence.AbstractFacade;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author matc_
 */
@Stateless
public class T_ClientesFacade extends AbstractFacade<T_Clientes> implements T_ClientesFacadeLocal {

    @PersistenceContext(unitName = "com.sippa_Inventario-Sippa-Ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public T_ClientesFacade() {
        super(T_Clientes.class);
    }

    @Override
    public T_Clientes findByXId(Long idCliente) {
        return (T_Clientes) em.createNamedQuery("T_Clientes.findById")
                .setParameter("idCliente", idCliente)
                .getResultList().stream().findFirst().get();
    }

    @Override
    public void eliminarDetallesXId(Long idCliente) {
        em.createQuery("DELETE FROM T_ClientesXSedes as a WHERE a.idCliente.idCliente = ?1 ")
                .setParameter("1", idCliente).executeUpdate();
    }

    @Override
    public List<T_Clientes> findByEstado(Boolean estado) {
        return getEntityManager().createNamedQuery("T_Clientes.findByEstado")
                .setParameter("estado", estado)
                .getResultList();
    }
}
