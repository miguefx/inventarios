/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.inventarios;

import VO.GraficaStockMinimo;
import VO.T_Bodegas;
import VO.T_Inventarios;
import VO.T_Productos;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author matc_
 */
@Local
public interface T_InventariosFacadeLocal {

    void create(T_Inventarios t_Inventarios);

    void edit(T_Inventarios t_Inventarios);

    void remove(T_Inventarios t_Inventarios);

    T_Inventarios find(Object id);

    List<T_Inventarios> findAll();

    List<T_Inventarios> findRange(int[] range);

    int count();

   T_Inventarios findByProductoXBodega(T_Productos producto, T_Bodegas bodega);

     List<GraficaStockMinimo> findAllByStockMinimoSuperado();
     
     List<T_Inventarios> findByBodega(T_Bodegas bodega);
}
