/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.Kardex;

import VO.T_DetalleKardex;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Miguel-PC
 */
@Local
public interface T_DetalleKardexFacadeLocal {

    void create(T_DetalleKardex t_DetalleKardex);

    void edit(T_DetalleKardex t_DetalleKardex);

    void remove(T_DetalleKardex t_DetalleKardex);

    T_DetalleKardex find(Object id);

    List<T_DetalleKardex> findAll();

    List<T_DetalleKardex> findRange(int[] range);

    int count();

}
