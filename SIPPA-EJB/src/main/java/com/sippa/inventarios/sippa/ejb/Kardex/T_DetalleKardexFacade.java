/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.Kardex;

import VO.T_DetalleKardex;
import com.sippa.inventarios.sippa.ejb.persistence.AbstractFacade;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Miguel-PC
 */
@Stateless
public class T_DetalleKardexFacade extends AbstractFacade<T_DetalleKardex> implements T_DetalleKardexFacadeLocal {

    @PersistenceContext(unitName = "com.sippa_Inventario-Sippa-Ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public T_DetalleKardexFacade() {
        super(T_DetalleKardex.class);
    }
    
}
