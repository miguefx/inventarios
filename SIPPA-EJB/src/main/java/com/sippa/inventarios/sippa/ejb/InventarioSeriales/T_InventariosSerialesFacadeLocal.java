/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.InventarioSeriales;

import VO.T_InventariosSeriales;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author matc_
 */
@Local
public interface T_InventariosSerialesFacadeLocal {

    void create(T_InventariosSeriales t_InventariosSeriales);

    void edit(T_InventariosSeriales t_InventariosSeriales);

    void remove(T_InventariosSeriales t_InventariosSeriales);

    T_InventariosSeriales find(Object id);

    List<T_InventariosSeriales> findAll();

    List<T_InventariosSeriales> findRange(int[] range);

    int count();

    List<T_InventariosSeriales> findByIdInventario(Long idInventario);

}
