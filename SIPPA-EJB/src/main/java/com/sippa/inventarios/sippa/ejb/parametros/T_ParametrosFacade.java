/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.parametros;

import VO.T_Parametros;
import com.sippa.inventarios.sippa.ejb.persistence.AbstractFacade;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author matc_
 */
@Stateless
public class T_ParametrosFacade extends AbstractFacade<T_Parametros> implements T_ParametrosFacadeLocal {

    @PersistenceContext(unitName = "com.sippa_Inventario-Sippa-Ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public T_ParametrosFacade() {
        super(T_Parametros.class);
    }

    @Override
    public T_Parametros findByParametroGenerico(String stockMinimoProducto) {
            return (T_Parametros) em.createNamedQuery("T_Parametros.findByParametroGenerico")
                    .setParameter("codigo", stockMinimoProducto)
                    .getResultList().stream().findFirst().orElse(null);
    }
    
}
