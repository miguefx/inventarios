/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.proveedores;

import VO.T_Proveedores;
import VO.T_Usuarios;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author matc_
 */
@Local
public interface T_ProveedoresFacadeLocal {

    void create(T_Proveedores t_Proveedores);

    void edit(T_Proveedores t_Proveedores);

    void remove(T_Proveedores t_Proveedores);

    T_Proveedores find(Object id);

    List<T_Proveedores> findAll();

    List<T_Proveedores> findRange(int[] range);

    int count();

    T_Proveedores findById(Long idProveedor);

    List<T_Proveedores> findByEstadoXEstado(Boolean estado);
    
}
