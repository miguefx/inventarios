/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.Kardex;

import VO.T_Bodegas;
import VO.T_Kardex;
import VO.T_Productos;
import VO.T_reporteKardex;
import VO.T_reporteKardexAux;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Miguel-PC
 */
@Local
public interface T_KardexFacadeLocal {

    void create(T_Kardex t_Kardex);

    void edit(T_Kardex t_Kardex);

    void remove(T_Kardex t_Kardex);

    T_Kardex find(Object id);

    List<T_Kardex> findAll();

    List<T_Kardex> findRange(int[] range);

    int count();

    T_Kardex findByProductoBodegaUltimo(T_Productos producto, T_Bodegas bodega);

    Long createAndReturnId(T_Kardex nuevoKardex);

    List<T_Kardex> findByProductoBodegaUltimoHistorico(T_Productos producto, T_Bodegas bodega);

    void actualizarKardex(T_Kardex kardex);

     List<T_reporteKardex> obtenerReporteGeneralKardex();

 
     List<T_reporteKardexAux> findDistinct();


}
