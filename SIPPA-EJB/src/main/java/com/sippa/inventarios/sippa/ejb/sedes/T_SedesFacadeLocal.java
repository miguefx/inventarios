/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.sedes;

import VO.T_Sedes;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author matc_
 */
@Local
public interface T_SedesFacadeLocal {

    void create(T_Sedes t_Sedes);

    void edit(T_Sedes t_Sedes);

    void remove(T_Sedes t_Sedes);

    T_Sedes find(Object id);

    List<T_Sedes> findAll();

    List<T_Sedes> findRange(int[] range);

    int count();

    T_Sedes consultarSedePorId(Long idSede);

    List<T_Sedes> findByEstado(Boolean estado);
    
}
