/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.proveedores;

import VO.T_Proveedores;
import com.sippa.inventarios.sippa.ejb.persistence.AbstractFacade;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author matc_
 */
@Stateless
public class T_ProveedoresFacade extends AbstractFacade<T_Proveedores> implements T_ProveedoresFacadeLocal {

    @PersistenceContext(unitName = "com.sippa_Inventario-Sippa-Ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public T_ProveedoresFacade() {
        super(T_Proveedores.class);
    }

    @Override
    public T_Proveedores findById(Long idProveedor) {
        return (T_Proveedores) getEntityManager().createNamedQuery("T_Proveedores.findById")
                .setParameter("idproveedor", idProveedor)
                .getResultList().stream().findFirst().get();
        
    }

    @Override
    public List<T_Proveedores> findByEstadoXEstado(Boolean estado) {
        return getEntityManager().createNamedQuery("T_Proveedores.findByEstado")
                .setParameter("estado", estado)
                .getResultList();
    }

}
