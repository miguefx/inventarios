/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.permisos;

import VO.T_Permisos;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author matc_
 */
@Local
public interface T_PermisosFacadeLocal {
    
     void create(T_Permisos t_Permisos);

    void edit(T_Permisos t_Permisos);

    void remove(T_Permisos t_Permisos);

    T_Permisos find(Object id);

    List<T_Permisos> findAll();

    List<T_Permisos> findRange(int[] range);

    int count();

     List<T_Permisos> findByIdUsuario(Long idUsuario);
    
}
