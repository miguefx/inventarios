/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.ciudades.detalleOrdenCompra;

import VO.T_DetalleOrdenDeCompra;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author matc_
 */
@Local
public interface T_DetalleOrdenDeCompraFacadeLocal {

    void create(T_DetalleOrdenDeCompra t_DetalleOrdenDeCompra);

    void edit(T_DetalleOrdenDeCompra t_DetalleOrdenDeCompra);

    void remove(T_DetalleOrdenDeCompra t_DetalleOrdenDeCompra);

    T_DetalleOrdenDeCompra find(Object id);

    List<T_DetalleOrdenDeCompra> findAll();

    List<T_DetalleOrdenDeCompra> findRange(int[] range);

    int count();
    
}
