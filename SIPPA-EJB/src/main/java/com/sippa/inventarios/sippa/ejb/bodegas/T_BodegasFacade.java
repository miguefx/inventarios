/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.bodegas;

import VO.T_Bodegas;
import com.sippa.inventarios.sippa.ejb.persistence.AbstractFacade;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author matc_
 */
@Stateless
public class T_BodegasFacade extends AbstractFacade<T_Bodegas> implements T_BodegasFacadeLocal {

    @PersistenceContext(unitName = "com.sippa_Inventario-Sippa-Ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public T_BodegasFacade() {
        super(T_Bodegas.class);
    }

    @Override
    public List<T_Bodegas> consultarBodegasByEstado(Boolean estado) {
        return getEntityManager().createNamedQuery("T_Bodegas.findByEstado")
                .setParameter("estado", estado)
                .getResultList();
    }

}
