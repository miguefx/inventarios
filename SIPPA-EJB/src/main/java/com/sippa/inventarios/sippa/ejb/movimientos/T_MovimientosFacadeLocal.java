/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.movimientos;

import VO.T_Movimientos;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author matc_
 */
@Local
public interface T_MovimientosFacadeLocal {

    void create(T_Movimientos t_Movimientos);

    void edit(T_Movimientos t_Movimientos);

    void remove(T_Movimientos t_Movimientos);

    T_Movimientos find(Object id);

    List<T_Movimientos> findAll();

    List<T_Movimientos> findRange(int[] range);

    int count();
    
}
