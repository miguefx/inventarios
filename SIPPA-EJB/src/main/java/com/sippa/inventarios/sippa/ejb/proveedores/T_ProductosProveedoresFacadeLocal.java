/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.proveedores;

import VO.T_ProductosProveedores;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author matc_
 */
@Local
public interface T_ProductosProveedoresFacadeLocal {

    void create(T_ProductosProveedores t_ProductosProveedores);

    void edit(T_ProductosProveedores t_ProductosProveedores);

    void remove(T_ProductosProveedores t_ProductosProveedores);

    T_ProductosProveedores find(Object id);

    List<T_ProductosProveedores> findAll();

    List<T_ProductosProveedores> findRange(int[] range);

    int count();

    void eliminarRegistrosXId(Long idProveedor);
    
}
