/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.recetaEnsamble;

import VO.T_OrdenDeCompra;
import VO.T_RecetaDeEnsamble;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author ASUS
 */
@Local
public interface T_RecetaDeEnsambleFacadeLocal {

    void create(T_RecetaDeEnsamble t_RecetaDeEnsamble);

    void edit(T_RecetaDeEnsamble t_RecetaDeEnsamble);

    void remove(T_RecetaDeEnsamble t_RecetaDeEnsamble);

    T_RecetaDeEnsamble find(Object id);

    List<T_RecetaDeEnsamble> findAll();

    List<T_RecetaDeEnsamble> findRange(int[] range);

    int count();

    List<T_RecetaDeEnsamble> obtenerRecetasXEstado(Boolean TRUE);

    void actualizarOrden(T_RecetaDeEnsamble orden);

    T_RecetaDeEnsamble buscarOrdenById(Long idRecetaEnsamble);

    Long registrarYObtenerId(T_RecetaDeEnsamble recetaNueva);

}
