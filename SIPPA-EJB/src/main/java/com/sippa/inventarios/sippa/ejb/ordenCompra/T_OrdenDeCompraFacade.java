/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.ordenCompra;

import VO.T_OrdenDeCompra;
import com.sippa.inventarios.sippa.ejb.persistence.AbstractFacade;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author matc_
 */
@Stateless
public class T_OrdenDeCompraFacade extends AbstractFacade<T_OrdenDeCompra> implements T_OrdenDeCompraFacadeLocal {

    @PersistenceContext(unitName = "com.sippa_Inventario-Sippa-Ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public T_OrdenDeCompraFacade() {
        super(T_OrdenDeCompra.class);
    }

    @Override
    public Long crearYObtenerId(T_OrdenDeCompra objVoOrdenCompra) {
        em.persist(objVoOrdenCompra);
        em.flush();
        return objVoOrdenCompra.getIdOrdenDeCompra();
    }

    @Override
    public List<T_OrdenDeCompra> consultarOrdenesXEstado(Boolean estado) {
        return em.createNamedQuery("T_OrdenDeCompra.findByEstado").setParameter("estado", estado)
                .getResultList();
    }

    @Override
    public T_OrdenDeCompra consultarOrdenById(Long idOrdenCompra) {
        return (T_OrdenDeCompra) em.createNamedQuery("T_OrdenDeCompra.findById").setParameter("idOrdenCompra",idOrdenCompra)
                .getResultList().stream().findFirst().orElse(null);
                
    }

}
