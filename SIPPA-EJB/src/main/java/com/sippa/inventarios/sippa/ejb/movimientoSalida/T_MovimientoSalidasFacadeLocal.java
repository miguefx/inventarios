/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.movimientoSalida;

import VO.T_MovimientoSalidas;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author matc_
 */
@Local
public interface T_MovimientoSalidasFacadeLocal {

    void create(T_MovimientoSalidas t_MovimientoSalidas);

    void edit(T_MovimientoSalidas t_MovimientoSalidas);

    void remove(T_MovimientoSalidas t_MovimientoSalidas);

    T_MovimientoSalidas find(Object id);

    List<T_MovimientoSalidas> findAll();

    List<T_MovimientoSalidas> findRange(int[] range);

    int count();
    
}
