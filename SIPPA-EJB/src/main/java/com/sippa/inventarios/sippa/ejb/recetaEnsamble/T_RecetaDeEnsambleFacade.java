/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.recetaEnsamble;

import VO.T_OrdenDeCompra;
import VO.T_RecetaDeEnsamble;
import com.sippa.inventarios.sippa.ejb.persistence.AbstractFacade;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author ASUS
 */
@Stateless
public class T_RecetaDeEnsambleFacade extends AbstractFacade<T_RecetaDeEnsamble> implements T_RecetaDeEnsambleFacadeLocal {

    @PersistenceContext(unitName = "com.sippa_Inventario-Sippa-Ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public T_RecetaDeEnsambleFacade() {
        super(T_RecetaDeEnsamble.class);
    }

    @Override
    public List<T_RecetaDeEnsamble> obtenerRecetasXEstado(Boolean estado) {
        return em.createNamedQuery("T_RecetaDeEnsamble.findByEstado").setParameter("estado", estado)
                .getResultList();
    }

    @Override
    public void actualizarOrden(T_RecetaDeEnsamble orden) {

    }

    @Override
    public T_RecetaDeEnsamble buscarOrdenById(Long idRecetaEnsamble) {
        return (T_RecetaDeEnsamble) em.createNamedQuery("T_RecetaDeEnsamble.findById").setParameter("idReceta", idRecetaEnsamble)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public Long registrarYObtenerId(T_RecetaDeEnsamble recetaNueva) {
        em.persist(recetaNueva);
        em.flush();
        return recetaNueva.getIdReceta();
    }

}
