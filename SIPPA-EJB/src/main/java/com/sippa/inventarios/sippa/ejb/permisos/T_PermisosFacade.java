/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.permisos;

import VO.T_Permisos;
import com.sippa.inventarios.sippa.ejb.persistence.AbstractFacade;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author matc_
 */
@Stateless
public class T_PermisosFacade extends AbstractFacade<T_Permisos>  implements T_PermisosFacadeLocal {

     @PersistenceContext(unitName = "com.sippa_Inventario-Sippa-Ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public T_PermisosFacade() {
        super(T_Permisos.class);
    }

    @Override
    public List<T_Permisos> findByIdUsuario(Long idUsuario) {
        return getEntityManager().createNamedQuery("T_Permisos.findByIdUsuario")
                .setParameter("idusuario", idUsuario)
                .getResultList();
    }
    
    
    
}
