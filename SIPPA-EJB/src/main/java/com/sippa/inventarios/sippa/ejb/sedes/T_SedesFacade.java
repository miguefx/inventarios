/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.sedes;

import VO.T_Sedes;
import com.sippa.inventarios.sippa.ejb.persistence.AbstractFacade;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author matc_
 */
@Stateless
public class T_SedesFacade extends AbstractFacade<T_Sedes> implements T_SedesFacadeLocal {

    @PersistenceContext(unitName = "com.sippa_Inventario-Sippa-Ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public T_SedesFacade() {
        super(T_Sedes.class);
    }

    @Override
    public T_Sedes consultarSedePorId(Long idSede) {
        return (T_Sedes) getEntityManager().createNamedQuery("T_Sedes.findById")
                .setParameter("idSede", idSede)
                .getResultList().stream().findFirst()
                .get();
    }

    @Override
    public List<T_Sedes> findByEstado(Boolean estado) {
        return getEntityManager().createNamedQuery("T_Sedes.findByEstado")
                .setParameter("estado", estado)
                .getResultList();
    }
    
}
