/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.proveedores;

import VO.T_ProductosProveedores;
import com.sippa.inventarios.sippa.ejb.persistence.AbstractFacade;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author matc_
 */
@Stateless
public class T_ProductosProveedoresFacade extends AbstractFacade<T_ProductosProveedores> implements T_ProductosProveedoresFacadeLocal {

    @PersistenceContext(unitName = "com.sippa_Inventario-Sippa-Ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public T_ProductosProveedoresFacade() {
        super(T_ProductosProveedores.class);
    }

    @Override
    public void eliminarRegistrosXId(Long idProveedor) {
        Query query = em.createQuery("DELETE FROM T_ProductosProveedores as a where a.idProveedor.idProveedor = ?1");
        query.setParameter(1, idProveedor);
        query.executeUpdate();
    }

}
