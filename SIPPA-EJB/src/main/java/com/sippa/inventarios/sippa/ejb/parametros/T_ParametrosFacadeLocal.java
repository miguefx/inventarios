/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.parametros;

import VO.T_Parametros;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author matc_
 */
@Local
public interface T_ParametrosFacadeLocal {

    void create(T_Parametros t_Parametros);

    void edit(T_Parametros t_Parametros);

    void remove(T_Parametros t_Parametros);

    T_Parametros find(Object id);

    List<T_Parametros> findAll();

    List<T_Parametros> findRange(int[] range);

    int count();

     T_Parametros findByParametroGenerico(String stockMinimoProducto);
    
}
