/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.usuarios;

import VO.T_Usuarios;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author matc_
 */
@Local
public interface T_UsuariosFacadeLocal {

    void create(T_Usuarios t_Usuarios);

    void edit(T_Usuarios t_Usuarios);

    void remove(T_Usuarios t_Usuarios);

    T_Usuarios find(Object id);

    List<T_Usuarios> findAll();

    List<T_Usuarios> findRange(int[] range);

    int count();

    T_Usuarios findByUsuarioXPassword(String usuario, String password);

    List<T_Usuarios> findByEstado(Boolean estado);
    
}
