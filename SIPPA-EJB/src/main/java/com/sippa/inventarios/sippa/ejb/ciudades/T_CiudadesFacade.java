/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.ciudades;

import VO.T_Ciudades;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import com.sippa.inventarios.sippa.ejb.persistence.AbstractFacade;
import javax.persistence.PersistenceContext;

/**
 *
 * @author matc_
 */
@Stateless
public class T_CiudadesFacade extends AbstractFacade<T_Ciudades> implements T_CiudadesFacadeLocal {

    @PersistenceContext(unitName = "com.sippa_Inventario-Sippa-Ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public T_CiudadesFacade() {
        super(T_Ciudades.class);
    }
    
}
