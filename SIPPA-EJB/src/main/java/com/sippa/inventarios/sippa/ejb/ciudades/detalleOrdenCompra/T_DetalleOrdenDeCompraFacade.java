/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.ciudades.detalleOrdenCompra;

import VO.T_DetalleOrdenDeCompra;
import com.sippa.inventarios.sippa.ejb.persistence.AbstractFacade;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author matc_
 */
@Stateless
public class T_DetalleOrdenDeCompraFacade extends AbstractFacade<T_DetalleOrdenDeCompra> implements T_DetalleOrdenDeCompraFacadeLocal {

    @PersistenceContext(unitName = "com.sippa_Inventario-Sippa-Ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public T_DetalleOrdenDeCompraFacade() {
        super(T_DetalleOrdenDeCompra.class);
    }
    
}
