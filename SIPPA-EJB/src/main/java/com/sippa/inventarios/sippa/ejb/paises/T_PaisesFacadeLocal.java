/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.paises;

import VO.T_Paises;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author matc_
 */
@Local
public interface T_PaisesFacadeLocal {

    void create(T_Paises t_Paises);

    void edit(T_Paises t_Paises);

    void remove(T_Paises t_Paises);

    T_Paises find(Object id);

    List<T_Paises> findAll();

    List<T_Paises> findRange(int[] range);

    int count();
    
}
