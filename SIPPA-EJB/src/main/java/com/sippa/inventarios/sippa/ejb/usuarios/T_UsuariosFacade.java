/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.usuarios;

import com.sippa.inventarios.sippa.ejb.persistence.AbstractFacade;
import VO.T_Usuarios;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author matc_
 */
@Stateless
public class T_UsuariosFacade extends AbstractFacade<T_Usuarios> implements T_UsuariosFacadeLocal {

    @PersistenceContext(unitName = "com.sippa_Inventario-Sippa-Ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public T_UsuariosFacade() {
        super(T_Usuarios.class);
    }

    @Override
    public T_Usuarios findByUsuarioXPassword(String usuario, String password) {
        return (T_Usuarios) getEntityManager().createNamedQuery("T_Usarios.findByUserXPassword")
                .setParameter("usuario", usuario)
                .setParameter("password", password)
                .getResultList().stream().findFirst().orElse(new T_Usuarios());
    }

    @Override
    public List<T_Usuarios> findByEstado(Boolean estado) {
        return getEntityManager().createNamedQuery("T_Usarios.findByUsuariosXEstado")
                .setParameter("estado", estado)
                .getResultList();
    }
    
}
