/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.inventarios;

import VO.GraficaStockMinimo;
import VO.T_Bodegas;
import VO.T_Inventarios;
import VO.T_Productos;
import com.sippa.inventarios.sippa.ejb.persistence.AbstractFacade;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author matc_
 */
@Stateless
public class T_InventariosFacade extends AbstractFacade<T_Inventarios> implements T_InventariosFacadeLocal {

    @PersistenceContext(unitName = "com.sippa_Inventario-Sippa-Ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public T_InventariosFacade() {
        super(T_Inventarios.class);
    }

    @Override
    public T_Inventarios findByProductoXBodega(T_Productos producto, T_Bodegas bodega) {
        return (T_Inventarios) em.createNamedQuery("T_Inventarios.findByProductoXBodega").setParameter("producto", producto).setParameter("bodega", bodega)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public List<GraficaStockMinimo> findAllByStockMinimoSuperado() {
        return em.createNativeQuery("SELECT p.Descripcion as producto , SUM(a.CANTIDAD) as cantidad  FROM T_Inventarios a , T_Productos p where a.IDPRODUCTO = p.IdEquipo AND p.StockMinimo IS NOT NULL group by  a.IDBODEGA , p.StockMinimo , p.Descripcion having SUM(a.CANTIDAD)<= p.StockMinimo ", "GraficaStockMinimo")
                .getResultList();
    }
    @Override
    public List<T_Inventarios> findByBodega(T_Bodegas bodega) {
        return em.createNamedQuery("T_Inventarios.findByBodega").setParameter("bodega", bodega).getResultList();
    }
}
