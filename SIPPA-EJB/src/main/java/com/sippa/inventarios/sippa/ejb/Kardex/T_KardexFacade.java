/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.Kardex;

import VO.T_Bodegas;
import VO.T_Kardex;
import VO.T_Productos;
import VO.T_reporteKardex;
import VO.T_reporteKardexAux;
import com.sippa.inventarios.sippa.ejb.persistence.AbstractFacade;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Miguel-PC
 */
@Stateless
public class T_KardexFacade extends AbstractFacade<T_Kardex> implements T_KardexFacadeLocal {

    @PersistenceContext(unitName = "com.sippa_Inventario-Sippa-Ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public T_KardexFacade() {
        super(T_Kardex.class);
    }

    @Override
    public T_Kardex findByProductoBodegaUltimo(T_Productos producto, T_Bodegas bodega) {
        return (T_Kardex) em.createNamedQuery("Kardex_findByProductoBodegaUltimo")
                .setParameter("producto", producto.getIdProducto())
                .setParameter("bodega", bodega.getIdBodega())
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public Long createAndReturnId(T_Kardex nuevoKardex) {
        em.persist(nuevoKardex);
        em.flush();
        return nuevoKardex.getIdHistorialInventario();
    }

    @Override
    public List<T_Kardex> findByProductoBodegaUltimoHistorico(T_Productos producto, T_Bodegas bodega) {
        return em.createNamedQuery("Kardex_findByProductoBodegaUltimo").setParameter("producto", producto.getIdProducto())
                .setParameter("bodega", bodega.getIdBodega())
                .getResultList();

    }

    @Override
    public void actualizarKardex(T_Kardex kardex) {

    }

    @Override
    public List<T_reporteKardex> obtenerReporteGeneralKardex() {
        return em.createNativeQuery("select d.Nombre as nombre , c.Descripcion as descripcionNombre , b.Serial as serial , b.GarantiaMeses as garantiaMes from T_Inventarios as a , T_InventariosSeriales as b , T_Productos as c , T_Bodegas as d where a.IDBODEGA = d.IdBodega and a.IDPRODUCTO = c.IdEquipo and a.IDINVENTARIO=b.IdInventario order by descripcionNombre; ", "GraficaGeneralKardex")
                .getResultList();
    }

    @Override
    public List<T_reporteKardexAux> findDistinct() {
        return em.createNativeQuery("select distinct IdProducto as idProducto , IdBodega as idBodega from T_Kardex","GraficaGeneralKardex").getResultList();
    }

}
