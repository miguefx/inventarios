/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.InventarioSeriales;

import VO.T_InventariosSeriales;
import com.sippa.inventarios.sippa.ejb.persistence.AbstractFacade;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author matc_
 */
@Stateless
public class T_InventariosSerialesFacade extends AbstractFacade<T_InventariosSeriales> implements T_InventariosSerialesFacadeLocal {

    @PersistenceContext(unitName = "com.sippa_Inventario-Sippa-Ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public T_InventariosSerialesFacade() {
        super(T_InventariosSeriales.class);
    }

    @Override
    public List<T_InventariosSeriales> findByIdInventario(Long idInventario) {
        return em.createNamedQuery("InventariosSeriales_FindByIdInventario")
                .setParameter("id", idInventario)
                .getResultList();
    }

}
