/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.productos;

import VO.T_Productos;
import com.sippa.inventarios.sippa.ejb.persistence.AbstractFacade;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author matc_
 */
@Stateless
public class T_ProductosFacade extends AbstractFacade<T_Productos> implements T_ProductosFacadeLocal {

    @PersistenceContext(unitName = "com.sippa_Inventario-Sippa-Ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public T_ProductosFacade() {
        super(T_Productos.class);
    }

    @Override
    public T_Productos consultarProductoXId(Long idProducto) {
        return (T_Productos) getEntityManager().createNamedQuery("T_Productos.findById")
                .setParameter("idProducto", idProducto)
                .getResultList().stream().findFirst().get();
    }

    @Override
    public void eliminarDetalleProductosXProveedores(Long idProducto) {
        Query query = getEntityManager().createQuery("DELETE FROM T_ProductosProveedores a where a.idProducto.idProducto = ?1")
                .setParameter(1, idProducto);
        query.executeUpdate();
    }

    @Override
    public List<T_Productos> findByEstado(Boolean estado) {
        return getEntityManager().createNamedQuery("T_Productos.findByEstado")
                .setParameter("estado", estado)
                .getResultList();
    }

    @Override
    public List<T_Productos> findAllStockMinimoTrue() {
        return getEntityManager().createNamedQuery("T_Productos.findByStockMinimo")
                .setParameter("estado", true).getResultList();
    }

}
