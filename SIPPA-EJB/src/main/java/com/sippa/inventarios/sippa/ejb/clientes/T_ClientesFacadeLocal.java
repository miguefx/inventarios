/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.clientes;

import VO.T_Clientes;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author matc_
 */
@Local
public interface T_ClientesFacadeLocal {

    void create(T_Clientes t_Clientes);

    void edit(T_Clientes t_Clientes);

    void remove(T_Clientes t_Clientes);

    T_Clientes find(Object id);

    List<T_Clientes> findAll();

    List<T_Clientes> findRange(int[] range);

    int count();

    T_Clientes findByXId(Long idCliente);

    void eliminarDetallesXId(Long idCliente);

    List<T_Clientes> findByEstado(Boolean estado);
    
}
