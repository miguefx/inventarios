/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.ordenCompra;

import VO.T_OrdenDeCompra;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author matc_
 */
@Local
public interface T_OrdenDeCompraFacadeLocal {

    void create(T_OrdenDeCompra t_OrdenDeCompra);

    void edit(T_OrdenDeCompra t_OrdenDeCompra);

    void remove(T_OrdenDeCompra t_OrdenDeCompra);

    T_OrdenDeCompra find(Object id);

    List<T_OrdenDeCompra> findAll();

    List<T_OrdenDeCompra> findRange(int[] range);

    int count();

    Long crearYObtenerId(T_OrdenDeCompra objVoOrdenCompra);

    List<T_OrdenDeCompra> consultarOrdenesXEstado(Boolean estado);

    T_OrdenDeCompra consultarOrdenById(Long idOrdenCompra);

}
