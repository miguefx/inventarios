/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sippa.inventarios.sippa.ejb.productos;

import VO.T_Productos;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author matc_
 */
@Local
public interface T_ProductosFacadeLocal {

    void create(T_Productos t_Productos);

    void edit(T_Productos t_Productos);

    void remove(T_Productos t_Productos);

    T_Productos find(Object id);

    List<T_Productos> findAll();

    List<T_Productos> findRange(int[] range);

    int count();

     T_Productos consultarProductoXId(Long idProducto);

    void eliminarDetalleProductosXProveedores(Long idProducto);

     List<T_Productos> findByEstado(Boolean estado);

     List<T_Productos> findAllStockMinimoTrue();

    }
