/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Enumeraciones;

/**
 *
 * @author matc_
 */
public enum Permisos {
    REGISTROUSUARIOS("Registro de Usuarios"), ADMUSUARIOS("Administracion de usuarios"), 
    REGISTROPROVEEDORES("Registro de proveedores"), ADMPROVEEDORES("Administracion de proveedores"), 
    REGISTROBODEGAS("Registro de bodegas"), ADMPBODEGAS("Administracion de bodegas"), 
    REGISTROPRODUCTOS("Registro de productos"), ADMPRODUCTOS("Administracion de productos"), 
    REGISTROCLIENTES("Registro de clientes"), ADMCLIENTES("Administracion de clientes"), 
    REGISTROSEDES("Registro de sedes"), ADMSEDES("Administracion de sedes"), REGISTROENTRADA("Registro de entradas"),
    REGISTROSALIDA("Registro de salida"), REGISTRODEVENTRADA("Registro devolucion entrada"), 
    REGISTRODEVSALIDA("Registro devolucion salida"),REGISTROORDENCOMPRA("Registro de orden compra"), ADMORDENCOMPRA("Administracion orden de compra"),
    REGISTRARECETA("Registro receta ensamble"), ADMRECETAENSAMBLE("Administracion receta ensamble"), 
    GESTIONKARDEX("Administracion de kardex")
;

    private String ruta;

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    private Permisos(String ruta) {
        this.ruta = ruta;
    }

}
